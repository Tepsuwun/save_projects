-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Feb 27, 2019 at 10:41 AM
-- Server version: 5.5.31
-- PHP Version: 5.6.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `projecth_tara`
--

-- --------------------------------------------------------

--
-- Table structure for table `add_consumable`
--

CREATE TABLE `add_consumable` (
  `atc_id` varchar(11) NOT NULL COMMENT 'รหัสเพิ่มของลงสต๊อควัสดุ',
  `atc_date` date NOT NULL COMMENT 'วันที่ที่เพิ่มลงในสต๊อควัสดุ',
  `bcons_id` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `add_consumable`
--

INSERT INTO `add_consumable` (`atc_id`, `atc_date`, `bcons_id`) VALUES
('atc0001', '2017-12-02', 'bn0001'),
('atc0002', '2018-01-13', 'bn0002'),
('atc0003', '2018-01-13', 'bn0003'),
('atc0004', '2018-01-31', 'bn0003'),
('atc0005', '2018-02-19', 'bn0003'),
('atc0006', '2018-02-19', 'bn0003'),
('atc0007', '2018-02-20', 'bn0004');

-- --------------------------------------------------------

--
-- Table structure for table `add_stock_color`
--

CREATE TABLE `add_stock_color` (
  `addcolor_id` varchar(11) NOT NULL COMMENT 'รหัสเพิ่มสีลงสต๊อค',
  `addcolor_date` date NOT NULL COMMENT 'วันที่เพิ่มสีลงสต๊อค',
  `color_id` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `add_stock_color`
--

INSERT INTO `add_stock_color` (`addcolor_id`, `addcolor_date`, `color_id`) VALUES
('adc0001', '2017-12-02', 'co0001'),
('adc0002', '2018-01-10', 'co0002'),
('adc0003', '2018-01-12', 'co0003'),
('adc0004', '2018-01-13', 'co0004'),
('adc0005', '2018-01-18', 'co0005'),
('adc0006', '2018-02-19', 'co0006'),
('adc0007', '2018-02-20', 'co0006'),
('adc0008', '2018-02-20', 'co0006');

-- --------------------------------------------------------

--
-- Table structure for table `add_to_spare`
--

CREATE TABLE `add_to_spare` (
  `ATS_id` varchar(11) NOT NULL COMMENT 'รหัสเพิ่มสินค้าลงสต๊อกอะไหล่',
  `ATS_date` date NOT NULL COMMENT 'วันที่เพิ่มสินค้าลงสต๊อกอะไหล่',
  `bol_id` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `add_to_spare`
--

INSERT INTO `add_to_spare` (`ATS_id`, `ATS_date`, `bol_id`) VALUES
('ats0001', '2018-01-11', 'bs0001'),
('ats0002', '2018-01-11', 'bs0002'),
('ats0003', '2018-01-11', 'bs0004'),
('ats0004', '2018-01-11', 'bs0005'),
('ats0005', '2018-01-11', 'bs0006'),
('ats0006', '2018-01-11', 'bs0007'),
('ats0007', '2018-01-11', 'bs0008'),
('ats0008', '2018-01-18', 'bs0007'),
('ats0009', '2018-02-19', 'bs0007'),
('ats0010', '2018-02-20', 'bs0005'),
('ats0011', '2018-02-20', 'bs0002'),
('ats0012', '2018-02-20', 'bs0003'),
('ats0013', '2018-02-20', 'bs0002');

-- --------------------------------------------------------

--
-- Table structure for table `bill_claim`
--

CREATE TABLE `bill_claim` (
  `bc_id` varchar(11) NOT NULL COMMENT 'รหัสใบเคลม',
  `bc_name` varchar(25) NOT NULL COMMENT 'ชื่อใบเคลม',
  `ir_claim` varchar(25) NOT NULL COMMENT 'เลขที่ใบเคลม',
  `ir_discount` int(11) NOT NULL COMMENT 'ส่วนลด',
  `ir_type` varchar(25) NOT NULL COMMENT 'ชนิดประกัน',
  `ir_insurance` varchar(25) NOT NULL COMMENT 'กรมธรรม',
  `op_id` varchar(11) NOT NULL COMMENT 'รหัสใบเสนอราคา',
  `ir_id` varchar(11) NOT NULL COMMENT 'รหัสประกัน'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bill_claim`
--

INSERT INTO `bill_claim` (`bc_id`, `bc_name`, `ir_claim`, `ir_discount`, `ir_type`, `ir_insurance`, `op_id`, `ir_id`) VALUES
('bc0001', 'case1', '', 0, '', '', 'op0001', 'ir0001'),
('bc0002', 'case2', '001', 15, 'lv1', 'กรม56678', 'op0002', 'ir0001');

-- --------------------------------------------------------

--
-- Table structure for table `bill_color`
--

CREATE TABLE `bill_color` (
  `color_id` varchar(11) NOT NULL COMMENT 'รหัสใบเบิกสี',
  `color_date` date NOT NULL COMMENT 'วันเวลาเบิกสี',
  `tn_id` varchar(11) NOT NULL COMMENT 'รหัสช่าง',
  `col_dc` int(11) NOT NULL,
  `col_deparment` varchar(25) NOT NULL,
  `rep_id` varchar(11) NOT NULL,
  `oc_id` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bill_color`
--

INSERT INTO `bill_color` (`color_id`, `color_date`, `tn_id`, `col_dc`, `col_deparment`, `rep_id`, `oc_id`) VALUES
('co0001', '2017-12-20', 'tn0001', 0, '', 're0001', 'ci0001'),
('co0002', '2018-02-03', 'tn0001', 15, 'ประกอบ', 're0004', 'ci0002'),
('co0003', '2018-02-19', 'tn0001', 15, 'ประกอบ', 're0001', 'ci0003'),
('co0004', '2018-02-20', 'tn0001', 15, 'ประกอบ', 're0002', 'ci0004'),
('co0005', '2018-02-20', 'tn0002', 15, 'ประกอบ', 're0002', 'ci0005'),
('co0006', '2018-02-27', 'tn0001', 15, 'ประกอบ', 're0001', 'ci0001'),
('co0007', '2018-02-27', 'tn0001', 15, 'ประกอบ', 're0001', 'ci0001');

-- --------------------------------------------------------

--
-- Table structure for table `bill_color_list`
--

CREATE TABLE `bill_color_list` (
  `bcl_id` varchar(11) NOT NULL COMMENT 'รหัสรายการเบิกสี',
  `bcl_use` float NOT NULL COMMENT 'การใช้สี',
  `bcl__codecolor` varchar(25) NOT NULL COMMENT 'เก็บโค้ดสี',
  `formc_id` varchar(11) NOT NULL COMMENT 'รหัสสูตรสี',
  `scolor_id` varchar(11) NOT NULL COMMENT 'รหัสสต๊อกสี'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bill_color_list`
--

INSERT INTO `bill_color_list` (`bcl_id`, `bcl_use`, `bcl__codecolor`, `formc_id`, `scolor_id`) VALUES
('bl0001', 10, '#11FFAA', 'fo0001', 'so0001');

-- --------------------------------------------------------

--
-- Table structure for table `bill_comnsumable`
--

CREATE TABLE `bill_comnsumable` (
  `bcons_id` varchar(11) NOT NULL COMMENT 'รหัสใบเบิกวัสดุสิ้นเปลือง',
  `bcons_date` date NOT NULL COMMENT 'วันเวลาเบิกวัสดุ',
  `tn_id` varchar(11) NOT NULL COMMENT 'รหัสช่าง',
  `bcons_depart` varchar(25) NOT NULL,
  `rep_id` varchar(11) NOT NULL,
  `bcons_dc` int(11) NOT NULL,
  `bcons_stat` tinyint(4) NOT NULL,
  `om_id` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bill_comnsumable`
--

INSERT INTO `bill_comnsumable` (`bcons_id`, `bcons_date`, `tn_id`, `bcons_depart`, `rep_id`, `bcons_dc`, `bcons_stat`, `om_id`) VALUES
('bn0001', '2017-12-14', 'tn0001', '', 're0001', 0, 0, 'om0001'),
('bn0002', '2018-02-05', 'tn0001', 'ประกอบ', 're0001', 15, 0, 'om0002'),
('bn0003', '2018-02-19', 'tn0001', 'ประกอบ', 're0001', 15, 0, 'om0003'),
('bn0004', '2018-02-20', 'tn0001', 'ประกอบ', 're0005', 15, 0, 'om0004'),
('bn0005', '2018-02-20', 'tn0002', 'ประกอบ', 're0005', 15, 0, 'om0005'),
('bn0006', '2018-02-27', 'tn0001', 'ประกอบ', 're0001', 15, 0, 'om0001'),
('bn0007', '2018-02-27', 'tn0001', 'ประกอบ', 're0001', 15, 0, 'om0001');

-- --------------------------------------------------------

--
-- Table structure for table `bill_of_landi`
--

CREATE TABLE `bill_of_landi` (
  `bol_id` varchar(11) NOT NULL COMMENT 'รหัสใบเบิกอะไหล่',
  `bol_date` date NOT NULL COMMENT 'วันที่เบิก',
  `bol_dc` int(11) NOT NULL COMMENT 'ส่วนลดอะไหล่',
  `bol_deparment` varchar(25) NOT NULL,
  `rep_id` varchar(11) NOT NULL COMMENT 'รหัสการซ่อม',
  `tn_id` varchar(11) NOT NULL,
  `os_id` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bill_of_landi`
--

INSERT INTO `bill_of_landi` (`bol_id`, `bol_date`, `bol_dc`, `bol_deparment`, `rep_id`, `tn_id`, `os_id`) VALUES
('bs0001', '2017-12-02', 15, '', 're0001', 'tn0001', 'si0001'),
('bs0002', '2018-01-25', 15, 'ประกอบ', 're0002', 'tn0001', 'si0002'),
('bs0003', '2018-02-19', 15, 'ประกอบ', 're0001', 'tn0001', 'si0003'),
('bs0004', '2018-02-19', 15, 'ประกอบ', 're0001', 'tn0002', 'si0004'),
('bs0005', '2018-02-20', 15, 'ประกอบ', 're0003', 'tn0002', 'si0005'),
('bs0006', '2018-02-20', 15, 'ประกอบ', 're0001', 'tn0001', 'si0006'),
('bs0007', '2018-02-20', 6, 'ประกอบ', 're0001', 'tn0006', 'si0007'),
('bs0008', '2018-02-27', 15, 'ประกอบ', 're0001', 'tn0002', 'si0001'),
('bs0009', '2018-02-27', 15, 'ประกอบ', 're0001', 'tn0001', 'si0001');

-- --------------------------------------------------------

--
-- Table structure for table `bill_repair_list`
--

CREATE TABLE `bill_repair_list` (
  `brl_id` varchar(11) NOT NULL COMMENT 'รหัสใบรายการซ่อม',
  `brl_date` date NOT NULL COMMENT 'เวลาที่เรื่มงาน',
  `brl_time_start` datetime NOT NULL COMMENT 'เวลางานที่กำหนด',
  `brl_timedeadline` datetime NOT NULL COMMENT 'เวลาที่งานเสร็จ',
  `brl_time_finish` datetime NOT NULL COMMENT 'วันที่ออกใบซ่อม',
  `op_id` varchar(11) NOT NULL COMMENT 'รหัสใบเสนอราคา'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bill_repair_list`
--

INSERT INTO `bill_repair_list` (`brl_id`, `brl_date`, `brl_time_start`, `brl_timedeadline`, `brl_time_finish`, `op_id`) VALUES
('brl0001', '2017-12-02', '2017-12-09 00:00:00', '2017-12-15 00:00:00', '2017-12-15 00:00:00', 'op0001');

-- --------------------------------------------------------

--
-- Table structure for table `car`
--

CREATE TABLE `car` (
  `car_id` varchar(11) NOT NULL COMMENT 'รหัสรถ',
  `car_licence` varchar(25) NOT NULL COMMENT 'หมายเลขเครื่อง',
  `car_chassis` varchar(25) NOT NULL COMMENT 'หมายเลขโครงรถ',
  `car_number` varchar(25) NOT NULL COMMENT 'ทะเบียนรถ',
  `car_gen` varchar(25) NOT NULL COMMENT 'รุ่นรถ',
  `car_brand` varchar(25) NOT NULL COMMENT 'ยี่ห้อรถ',
  `car_size` varchar(25) NOT NULL COMMENT 'ขนาดรถ',
  `car_type` varchar(25) NOT NULL COMMENT 'ประเภทรถ',
  `car_color` varchar(25) NOT NULL COMMENT 'สีรถ',
  `car_mile` int(11) NOT NULL COMMENT 'ไมล์รถ',
  `car_province` varchar(25) NOT NULL,
  `cus_id` varchar(11) NOT NULL COMMENT 'รหัสลูกค้า'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `car`
--

INSERT INTO `car` (`car_id`, `car_licence`, `car_chassis`, `car_number`, `car_gen`, `car_brand`, `car_size`, `car_type`, `car_color`, `car_mile`, `car_province`, `cus_id`) VALUES
('ca0001', '001002003', '000a111b222c', '1803', '3', 'mazda', 'กลางเล็ก', '4ล้อ', 'ดำแดงเขียว', 10000, '', 'cu0001');

-- --------------------------------------------------------

--
-- Table structure for table `car_receipt`
--

CREATE TABLE `car_receipt` (
  `cr_id` varchar(11) NOT NULL COMMENT 'รหัสใบรับรถ',
  `cr_date` date NOT NULL COMMENT 'วันเวลาทำใบรับรถ',
  `cr_datepark` date NOT NULL COMMENT 'วันที่รถมาจอด',
  `cr_park_status` tinyint(1) NOT NULL COMMENT 'สถานะการจอดรถ',
  `cr_datemeet` date NOT NULL COMMENT 'วันที่นัดรถมาจอด',
  `cr_recipient_name` varchar(25) NOT NULL COMMENT 'ชื่อคนทีรับรถ'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `car_receipt`
--

INSERT INTO `car_receipt` (`cr_id`, `cr_date`, `cr_datepark`, `cr_park_status`, `cr_datemeet`, `cr_recipient_name`) VALUES
('cr0001', '2017-12-13', '2017-12-20', 1, '2017-12-20', 'อนุมัติบัญชา');

-- --------------------------------------------------------

--
-- Table structure for table `comnsumable`
--

CREATE TABLE `comnsumable` (
  `cons_id` varchar(11) NOT NULL COMMENT 'รหัสวัสดุสิ้นเปลือง',
  `cons_name` varchar(25) NOT NULL COMMENT 'ชื่อรายการวัสดุสิ้นเปลือง',
  `cons_type` varchar(25) NOT NULL COMMENT 'ชนิดวัสดุสิ้นเปลือง',
  `cons_price` float NOT NULL COMMENT 'ราคาวัสดุสิ้นเปลื้อง',
  `cons_amount` varchar(11) NOT NULL COMMENT 'จำนวนวัสดุสิ้นเปลือง'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `comnsumable`
--

INSERT INTO `comnsumable` (`cons_id`, `cons_name`, `cons_type`, `cons_price`, `cons_amount`) VALUES
('cn0001', 'กระดาษทราย', 'ผิวหยาบเบอร์ 5', 110, '91'),
('cn0002', 'ทินเนอร์', 'สารละลาย', 1000, '474'),
('cn0003', 'ทินเนอร์', 'สารละลาย', 1000, '559'),
('cn0004', 'ทินเนอร์', 'สารละลาย', 10000, '110'),
('cn0005', 'ทินเนอร์', 'สารละลาย', 10000, '100'),
('cn0006', 'ทินเนอร์', 'สารละลาย', 1000, '90'),
('cn0007', 'ทินเนอร์', 'สารละลาย', 10000, '110'),
('cn0008', 'ทินเนอร์', 'สารละลาย', 10000, '100'),
('cn0009', 'ทินเนอร์', 'สารละลาย', 1000, '100'),
('cn0010', 'ทินเนอร์', 'สารละลาย', 1000, '0'),
('cn0011', 'ทินเนอร์', 'สารละลาย', 1000, '0'),
('cn0012', 'ทินเนอร์', 'สารละลาย', 1000, '0'),
('cn0013', 'ทินเนอร์', 'สารละลาย', 1000, '0');

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `com_id` varchar(11) NOT NULL COMMENT 'รหัสบริษัท',
  `com_sell_name` varchar(25) NOT NULL COMMENT 'ชื่อบริษัท'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`com_id`, `com_sell_name`) VALUES
('cm0001', 'ชลวัชการอะไหล์ยนต์'),
('cm0002', 'ชลวัชการอะไหล์ยนต์');

-- --------------------------------------------------------

--
-- Table structure for table `consumable_amount`
--

CREATE TABLE `consumable_amount` (
  `cona_id` int(11) NOT NULL COMMENT 'รหัสจำนวนที่สั่งซื้อ',
  `cona_price` float NOT NULL COMMENT 'จำนวนที่สั่งซื้อวัสดุ',
  `cona_amount` int(11) NOT NULL COMMENT 'จำนวนวัสดุสิ้นเปลื้องที่สั่งซื้อ',
  `cona_unit` varchar(25) NOT NULL COMMENT 'หน่วยนับ',
  `cona_discount` int(11) NOT NULL COMMENT 'ส่วนลด',
  `cona_stat` int(11) NOT NULL,
  `cons_id` varchar(11) NOT NULL,
  `om_id` varchar(11) NOT NULL,
  `notShow` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `consumable_amount`
--

INSERT INTO `consumable_amount` (`cona_id`, `cona_price`, `cona_amount`, `cona_unit`, `cona_discount`, `cona_stat`, `cons_id`, `om_id`, `notShow`) VALUES
(1, 1000, 10, 'อัน', 15, 1, 'cn0001', 'om0001', 1),
(2, 14500, 10, 'อัน', 15, 1, 'cn0001', 'om0002', 1),
(3, 14500, 10, 'อัน', 15, 0, 'cn0001', 'om0002', 1),
(4, 14500, 10, 'อัน', 15, 0, 'cn0001', 'om0002', 1),
(5, 14500, 10, 'อัน', 15, 0, 'cn0001', 'om0002', 0),
(6, 14500, 10, 'อัน', 15, 0, 'cn0006', 'om0002', 0),
(7, 14500, 10, 'อัน', 15, 0, 'cn0006', 'om0002', 0),
(8, 14500, 10, 'อัน', 15, 0, 'cn0006', 'om0003', 0),
(9, 14500, 10, 'อัน', 15, 0, 'cn0006', 'om0003', 0),
(10, 14500, 10, 'อัน', 15, 0, 'cn0004', 'om0003', 0),
(11, 14500, 10, 'อัน', 15, 0, 'cn0004', 'om0003', 0),
(12, 14500, 10, 'อัน', 15, 0, 'cn0008', 'om0002', 0),
(15, 14500, 10, 'อัน', 15, 0, 'cn0005', 'om0004', 0),
(16, 14500, 10, 'อัน', 15, 0, 'cn0005', 'om0005', 0);

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `cus_id` varchar(11) NOT NULL COMMENT 'รหัสลูกค้า',
  `cus_name` varchar(25) NOT NULL COMMENT 'ชื่อ',
  `cus_surname` varchar(25) NOT NULL COMMENT 'นามสกุล',
  `cus_address` varchar(25) NOT NULL COMMENT 'ที่อยู่',
  `cus_district` varchar(25) NOT NULL COMMENT 'ตำบล',
  `cus_boundary` varchar(25) NOT NULL COMMENT 'อำเภอ',
  `cus_provice` varchar(25) NOT NULL COMMENT 'จังหวัด',
  `cus_tell` varchar(25) NOT NULL COMMENT 'เบอร์โทรศัพท์/มือถือ',
  `cus_call` varchar(25) NOT NULL COMMENT 'เบอร์โทรศัพท์/บ้าน/สำนักงาน',
  `cus_post` varchar(25) NOT NULL COMMENT 'รหัสไปรษณีย์'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`cus_id`, `cus_name`, `cus_surname`, `cus_address`, `cus_district`, `cus_boundary`, `cus_provice`, `cus_tell`, `cus_call`, `cus_post`) VALUES
('', 'ธนกร', 'เทพสุวรรณ', '191', 'เมืองเชียงใหม่', 'เมืองเชียงใหม่ ', 'เชียงใหม่', '090000000', '050000000', ''),
('0052', 'sss', 'sss', 'cus_address', 'ssss', 'sss', 'sss', 'sss', 'sss', '30130'),
('asdasd', 'asdasd', '', 'cus_address', '', '', '', '', '', '30130'),
('c1234', 'Vattikorn', 'Prajonkbua', '95/16', 'Nhongsarai', 'Pakchong', 'Nakonratchasima', '0999999999', '0994499999', ''),
('C55555', 'Vattikorn', 'Prajonkbua', '95/16', 'Nhongsarai', 'Pakchong', 'Nakonratchasima', '0999999999', '0994499999', '111111'),
('cu0001', 'ธนกร', 'เทพสุวรรณ', '191', 'สุเทพ', 'เมืองเชียงใหม่', 'เชียงใหม่', '090000000', '050000000', '50000'),
('cu0004', 'ธนกร', 'เทพสุวรรณ', 'cus_address', 'เมืองเชียงใหม่', 'เมืองเชียงใหม่', 'เชียงใหม่', '090000000', '050000000', '30130'),
('cus4456', 'qwe', 'qwe', 'cus_address', 'qwe', 'qwe', 'qwe', 'qwe', 'qwe', '30130'),
('cus_008', 'Undermind', 'Najaaaa', 'Moon', 'Min', 'Mooh', 'Kyo', '9999999', '9999999', '152002'),
('cus_1234', 'Mynameis', 'Kala', 'cus_address', 'Comeback', 'Comeback', 'Againnnnn', '15555555', '1555555', '30130'),
('sadasdasd', 'asdasd', 'asd', 'cus_address', 'sd', 'sd', 'sd', 'sd', 'sd', '30130');

-- --------------------------------------------------------

--
-- Table structure for table `customer2`
--

CREATE TABLE `customer2` (
  `CustomerID` int(11) NOT NULL,
  `Name` varchar(11) NOT NULL,
  `Email` varchar(11) NOT NULL,
  `CountryCode` varchar(11) NOT NULL,
  `Budget` varchar(11) NOT NULL,
  `Used` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer2`
--

INSERT INTO `customer2` (`CustomerID`, `Name`, `Email`, `CountryCode`, `Budget`, `Used`) VALUES
(2121, '11qww', '11', '11', '11', '11'),
(15511, '11aa11', '11155', '11', '11', '11');

-- --------------------------------------------------------

--
-- Table structure for table `damage`
--

CREATE TABLE `damage` (
  `dm_id` varchar(11) NOT NULL COMMENT 'รหัสความเสียหาย',
  `dm_name` varchar(25) NOT NULL COMMENT 'ชื่อรายการความเสียหาย',
  `dm_type` varchar(25) NOT NULL COMMENT 'ชนิดความเสียหาย',
  `dm_list` varchar(20) NOT NULL,
  `op_id` varchar(11) NOT NULL COMMENT 'รหัสใบเสนอราคา'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `damage`
--

INSERT INTO `damage` (`dm_id`, `dm_name`, `dm_type`, `dm_list`, `op_id`) VALUES
('dm1', '55454545', 'เปลี่ยน', 'กลาง', 'op0001'),
('dm10', 'ssss', 'เปลี่ยน', 'หนัก', 'op0001'),
('dm11', 'ddddd', 'เปลี่ยน', 'หนัก', 'op0001'),
('dm12', '4444', 'เปลี่ยน', 'หนัก', 'op0001'),
('dm13', '', 'เปลี่ยน', 'หนัก', 'op0001'),
('dm14', '7777777', 'เปลี่ยน', 'หนัก', 'op0001'),
('dm2', 'sssssssss', 'เปลี่ยน', 'หนัก', 'op0001'),
('dm3', 's', 'เปลี่ยน', 'หนัก', 'op0001'),
('dm4', 'aaaa', 'เปลี่ยน', 'หนัก', 'op0001'),
('dm5', 'ffff', 'เปลี่ยน', 'หนัก', 'op0001'),
('dm6', 'ffffwww', 'เปลี่ยน', 'หนัก', 'op0001'),
('dm7', 'oooo', 'เปลี่ยน', 'หนัก', 'op0001'),
('dm8', '5555', 'เปลี่ยน', 'หนัก', 'op0001'),
('dm9', '1111', 'เปลี่ยน', 'หนัก', 'op0001');

-- --------------------------------------------------------

--
-- Table structure for table `date`
--

CREATE TABLE `date` (
  `id` int(10) NOT NULL,
  `date` time NOT NULL,
  `day` date NOT NULL,
  `car_licence` varchar(30) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `date`
--

INSERT INTO `date` (`id`, `date`, `day`, `car_licence`) VALUES
(219, '00:00:00', '0000-00-00', ''),
(220, '14:05:00', '2018-02-20', 'à¸Šà¸¡5546'),
(221, '00:00:00', '0000-00-00', ''),
(222, '15:02:00', '2018-02-21', 'à¸à¸8888'),
(223, '00:00:00', '0000-00-00', ''),
(224, '00:00:00', '0000-00-00', ''),
(225, '00:00:00', '0000-00-00', ''),
(226, '15:02:00', '2018-02-21', 'ssssss'),
(227, '00:00:00', '0000-00-00', ''),
(228, '00:00:00', '0000-00-00', ''),
(229, '00:00:00', '0000-00-00', ''),
(230, '00:00:00', '0000-00-00', ''),
(231, '00:00:00', '0000-00-00', ''),
(232, '09:30:00', '2018-02-20', 'à¸—à¸ž555'),
(233, '09:33:00', '2018-02-20', 'à¸™à¸¢55'),
(234, '09:33:00', '2018-02-20', 'à¸¢à¸¢8895'),
(235, '00:00:00', '0000-00-00', ''),
(236, '00:00:00', '0000-00-00', ''),
(237, '00:00:00', '0000-00-00', ''),
(238, '03:30:00', '2018-02-24', 'Undermind8888'),
(239, '00:00:00', '0000-00-00', ''),
(240, '03:30:00', '2018-02-25', 'iiii789'),
(241, '00:00:00', '0000-00-00', ''),
(242, '00:00:00', '0000-00-00', ''),
(243, '00:00:00', '0000-00-00', ''),
(244, '00:00:00', '0000-00-00', ''),
(245, '00:00:00', '0000-00-00', ''),
(246, '00:00:00', '0000-00-00', ''),
(247, '00:00:00', '0000-00-00', ''),
(248, '00:00:00', '0000-00-00', ''),
(249, '00:00:00', '0000-00-00', ''),
(250, '00:00:00', '0000-00-00', ''),
(251, '00:00:00', '0000-00-00', ''),
(252, '00:00:00', '0000-00-00', ''),
(253, '00:00:00', '0000-00-00', ''),
(254, '00:00:00', '0000-00-00', ''),
(255, '00:00:00', '0000-00-00', ''),
(256, '00:00:00', '0000-00-00', ''),
(257, '00:00:00', '0000-00-00', ''),
(258, '00:00:00', '0000-00-00', ''),
(259, '00:00:00', '0000-00-00', ''),
(260, '00:00:00', '0000-00-00', ''),
(261, '00:00:00', '0000-00-00', ''),
(262, '00:00:00', '0000-00-00', ''),
(263, '00:00:00', '0000-00-00', ''),
(264, '00:00:00', '0000-00-00', ''),
(265, '00:00:00', '0000-00-00', ''),
(266, '00:00:00', '0000-00-00', ''),
(267, '00:00:00', '0000-00-00', ''),
(268, '00:00:00', '0000-00-00', ''),
(269, '00:00:00', '0000-00-00', ''),
(270, '00:00:00', '0000-00-00', ''),
(271, '00:00:00', '0000-00-00', ''),
(272, '00:00:00', '0000-00-00', ''),
(273, '00:00:00', '0000-00-00', ''),
(274, '00:00:00', '0000-00-00', ''),
(275, '00:00:00', '0000-00-00', ''),
(276, '00:00:00', '0000-00-00', ''),
(277, '00:00:00', '0000-00-00', ''),
(278, '00:00:00', '0000-00-00', ''),
(279, '00:00:00', '0000-00-00', ''),
(280, '00:00:00', '0000-00-00', ''),
(281, '00:00:00', '0000-00-00', ''),
(282, '00:00:00', '0000-00-00', ''),
(283, '00:00:00', '0000-00-00', ''),
(284, '00:00:00', '0000-00-00', ''),
(285, '00:00:00', '0000-00-00', ''),
(286, '00:00:00', '0000-00-00', ''),
(287, '00:00:00', '0000-00-00', ''),
(288, '00:00:00', '0000-00-00', ''),
(289, '00:00:00', '0000-00-00', '');

-- --------------------------------------------------------

--
-- Table structure for table `document_check`
--

CREATE TABLE `document_check` (
  `dc_id` varchar(11) NOT NULL COMMENT 'รหัสใบตรวจสอบ',
  `dc_name` varchar(25) NOT NULL COMMENT 'ชื่อผู้ตรวจสอบ',
  `rl_id` varchar(11) NOT NULL COMMENT 'รหัสรายการซ่อม',
  `car_id` varchar(11) NOT NULL COMMENT 'รหัสรายละเอียดรถ'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `document_check`
--

INSERT INTO `document_check` (`dc_id`, `dc_name`, `rl_id`, `car_id`) VALUES
('di0001', 'ชม-1150', 'rl0001', 'ca0001');

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `emp_id` varchar(11) NOT NULL COMMENT 'รหัสพนักงาน',
  `emp_name` varchar(25) NOT NULL COMMENT 'ชื่อพนักงาน',
  `emp_position` varchar(25) NOT NULL COMMENT 'ตำแหน่งพนักงาน'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`emp_id`, `emp_name`, `emp_position`) VALUES
('em0001', 'ธนกร ', 'ล้างรถ');

-- --------------------------------------------------------

--
-- Table structure for table `fixed`
--

CREATE TABLE `fixed` (
  `fix_id` varchar(11) NOT NULL COMMENT 'รหัสเก็บข้อมูลรถและลูกค้า',
  `cus_id` varchar(11) NOT NULL COMMENT 'รหัสลูกค้า',
  `car_id` varchar(11) NOT NULL COMMENT 'รหัสรถ',
  `rep_id` varchar(11) NOT NULL COMMENT 'รหัสการซ่อม'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `fixed`
--

INSERT INTO `fixed` (`fix_id`, `cus_id`, `car_id`, `rep_id`) VALUES
('fi0001', 'cu0001', 'ca0001', 're0001');

-- --------------------------------------------------------

--
-- Table structure for table `formulas_color`
--

CREATE TABLE `formulas_color` (
  `formc_id` varchar(11) NOT NULL COMMENT 'รหัสสูตรสี',
  `formc_formula` text NOT NULL COMMENT 'สูตรสี',
  `formc_amount_use` varchar(11) NOT NULL COMMENT 'จำนวนสีที่ใช้'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `formulas_color`
--

INSERT INTO `formulas_color` (`formc_id`, `formc_formula`, `formc_amount_use`) VALUES
('fo0001', '#111F12', '1000');

-- --------------------------------------------------------

--
-- Table structure for table `image`
--

CREATE TABLE `image` (
  `img_id` varchar(11) NOT NULL COMMENT 'รหัสรูปภาพ',
  `img_name` varchar(200) NOT NULL COMMENT 'ชื่อรูปภาพ',
  `img_repair` varchar(200) NOT NULL COMMENT 'สถานะรูปภาพ',
  `img_status` tinyint(1) NOT NULL COMMENT 'รูปก่อน,ระหว่าง,หลังซ่อม',
  `rl_id` varchar(11) NOT NULL COMMENT 'รหัสรายการซ่อม',
  `rep_id` varchar(11) NOT NULL COMMENT 'รหัสการซ่อม',
  `dateup` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `image`
--

INSERT INTO `image` (`img_id`, `img_name`, `img_repair`, `img_status`, `rl_id`, `rep_id`, `dateup`) VALUES
('img1', '1517387276-before-IMG7099.JPG', 'Longtest', 1, 'rl0001', 're0001', '2018-01-31 08:27:58'),
('img2', '1517387353-IMG7030.JPG', 'Longtest222222', 2, 'rl0001', 're0001', '2018-01-31 08:29:15'),
('img3', '1517387498-IMG7224.JPG', '22222222222222', 2, 'rl0001', 're0001', '2018-01-31 08:31:38'),
('img4', '1517883261-after-IMG0028.JPG', 'ddddd', 3, 'rl0001', 're0001', '2018-02-06 02:14:19'),
('img5', '1517968220-before-IMG0087.JPG', 'ssssssssssss', 1, 'rl0001', 're0001', '2018-02-07 01:50:20');

-- --------------------------------------------------------

--
-- Table structure for table `insurance`
--

CREATE TABLE `insurance` (
  `ir_id` varchar(11) NOT NULL COMMENT 'รหัสประกัน',
  `ir_name` varchar(25) NOT NULL COMMENT 'ชื่อประกัน',
  `ir_company` varchar(25) NOT NULL COMMENT 'ชื่อบริษัทประกัน'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `insurance`
--

INSERT INTO `insurance` (`ir_id`, `ir_name`, `ir_company`) VALUES
('ir0001', 'กะบะวิริยะ', 'วิริยะ');

-- --------------------------------------------------------

--
-- Table structure for table `ist_bill_con`
--

CREATE TABLE `ist_bill_con` (
  `bcn_id` int(11) NOT NULL,
  `bcn_price` int(11) NOT NULL,
  `bcn_amount` int(11) NOT NULL,
  `bcons_id` varchar(11) NOT NULL,
  `cons_id` varchar(11) NOT NULL,
  `notShow` int(1) NOT NULL,
  `bcn_stat` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ist_bill_con`
--

INSERT INTO `ist_bill_con` (`bcn_id`, `bcn_price`, `bcn_amount`, `bcons_id`, `cons_id`, `notShow`, `bcn_stat`) VALUES
(1, 1000, 10, 'bn0001', 'cn0006', 0, 0),
(2, 110, 15, 'bn0001', 'cn0001', 0, 0),
(3, 110, 15, 'bn0001', 'cn0001', 0, 0),
(4, 1000, 11, 'bn0001', 'cn0002', 0, 0),
(5, 1000, 15, 'bn0001', 'cn0003', 0, 0),
(6, 10000, 11, 'bn0003', 'cn0007', 0, 0),
(7, 1000, 11, 'bn0003', 'cn0009', 0, 0),
(8, 10000, 11, 'bn0004', 'cn0005', 0, 0),
(9, 10000, 11, 'bn0004', 'cn0004', 0, 0),
(10, 1000, 11, 'bn0005', 'cn0003', 0, 0),
(11, 1000, 78, 'bn0005', 'cn0003', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `list_bill_color`
--

CREATE TABLE `list_bill_color` (
  `col_id` int(11) NOT NULL,
  `col_amount` int(11) NOT NULL,
  `col_price` int(11) NOT NULL,
  `scolor_id` varchar(11) NOT NULL,
  `color_id` varchar(11) NOT NULL,
  `col_stat` tinyint(1) NOT NULL,
  `notShow` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `list_bill_color`
--

INSERT INTO `list_bill_color` (`col_id`, `col_amount`, `col_price`, `scolor_id`, `color_id`, `col_stat`, `notShow`) VALUES
(1, 5, 10000, 'so0004', 'co0001', 0, 0),
(3, 1, 10000, 'so0004', 'co0002', 0, 0),
(4, 42, 1000, 'so0001', 'co0001', 0, 0),
(5, 15, 10000, 'so0002', 'co0002', 0, 0),
(6, 3, 10000, 'so0004', 'co0003', 0, 0),
(7, 3, 10000, 'so0004', 'co0003', 0, 0),
(8, 11, 10000, 'so0005', 'co0003', 0, 0),
(9, 11, 1000, 'so0003', '', 0, 0),
(10, 11, 10000, 'so0005', 'co0003', 0, 0),
(11, 10, 10000, 'so0005', 'co0003', 0, 0),
(12, 11, 1000, 'so0003', 'co0004', 0, 0),
(13, 4, 10000, 'so0004', 'co0005', 0, 0),
(14, 11, 1000, 'so0003', 'co0005', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `list_bill_spare`
--

CREATE TABLE `list_bill_spare` (
  `lbs_id` int(11) NOT NULL,
  `spare_name` varchar(25) CHARACTER SET utf8 NOT NULL,
  `spare_gen` varchar(25) CHARACTER SET utf8 NOT NULL,
  `spare_brand` varchar(25) CHARACTER SET utf8 NOT NULL,
  `bol_id` varchar(11) CHARACTER SET utf8 NOT NULL,
  `bol_stat` tinyint(1) NOT NULL,
  `spare_id` varchar(11) CHARACTER SET utf8 NOT NULL,
  `sp_amount` int(11) NOT NULL,
  `notShow` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `list_bill_spare`
--

INSERT INTO `list_bill_spare` (`lbs_id`, `spare_name`, `spare_gen`, `spare_brand`, `bol_id`, `bol_stat`, `spare_id`, `sp_amount`, `notShow`) VALUES
(2, 'ไฟท้ายด้านซ้าย', 'มาสด้า3', 'มาสด้า', 'bs0001', 0, 'sp0008', 11, 0),
(3, 'ไฟท้ายด้านซ้าย', 'มาสด้า3', 'มาสด้า', 'bs0001', 0, 'sp0008', 11, 0),
(6, 'กันชน', 'ดีแม็ก', 'อีซูซุ', 'bs0001', 0, 'sp0002', 11, 0),
(9, 'ไฟเลี้ยวด้านซ้าย', 'มาสด้า3', 'มาสด้า', 'bs0002', 0, 'sp0005', 11, 0),
(10, 'ไฟเลี้ยวด้านซ้าย', 'มาสด้า3', 'มาสด้า', 'bs0002', 0, 'sp0005', 11, 0),
(11, 'ไฟเลี้ยวด้านซ้าย', 'มาสด้า3', 'มาสด้า', 'bs0002', 0, 'sp0005', 11, 0),
(12, 'ไฟเลี้ยวด้านซ้าย', 'มาสด้า3', 'มาสด้า', 'bs0002', 0, 'sp0005', 11, 0),
(13, 'ไฟเลี้ยวด้านซ้าย', 'มาสด้า3', 'มาสด้า', 'bs0002', 0, 'sp0005', 11, 0),
(14, 'ไฟท้ายด้านซ้าย', 'ดีแม๊ก', 'อีซูซุ', 'bs0002', 0, 'sp0006', 11, 0),
(15, 'ไฟท้ายด้านซ้าย', 'มาสด้า3', 'มาสด้า', 'bs0003', 0, 'sp0003', 11, 0),
(16, 'ไฟท้ายด้านซ้าย', 'มาสด้า3', 'มาสด้า', 'bs0003', 0, 'sp0003', 11, 0),
(17, 'ไฟท้ายด้านซ้าย', 'มาสด้า3', 'มาสด้า', 'bs0004', 0, 'sp0007', 10, 0),
(18, 'ไฟหน้าด้านซ้าย', 'ดีแม๊ก', 'อีซูซุ', 'bs0004', 0, 'sp0004', 11, 0),
(19, 'ไฟท้ายด้านซ้าย', 'มาสด้า3', 'มาสด้า', 'bs0004', 0, 'sp0003', 11, 0),
(20, 'ไฟหน้าด้านซ้าย', 'ดีแม๊ก', 'อีซูซุ', 'bs0005', 0, 'sp0004', 11, 0),
(22, 'ไฟหน้าด้านซ้าย', 'ดีแม๊ก', 'อีซูซุ', 'bs0006', 0, 'sp0004', 11, 0),
(23, 'ไฟท้ายด้านซ้าย', 'มาสด้า3', 'มาสด้า', 'bs0006', 0, 'sp0003', 11, 0),
(24, 'กันชน', 'มาสด้า3', 'ดำ', 'bs0007', 0, 'sp0013', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `list_color`
--

CREATE TABLE `list_color` (
  `list_color_id` int(11) NOT NULL COMMENT 'รหัสรายการเพิ่มสีลง',
  `list_color_price` float NOT NULL COMMENT 'ราคาสี',
  `list_color_stat` tinyint(1) NOT NULL COMMENT 'สถานะการอนัติ/ไม่อนุมัติ',
  `list_color_amount` int(11) NOT NULL COMMENT 'จำนวนสีที่เพิ่มลงสต๊อค',
  `addcolor_id` varchar(11) NOT NULL COMMENT 'รหัสใบเพิ่มสีลงสต๊อค',
  `scolor_id` varchar(11) NOT NULL COMMENT 'รหัสสต๊อคสีสิ้นเปลื้อง',
  `list_color_dc` int(11) NOT NULL,
  `com_id` varchar(11) NOT NULL,
  `notShow` int(1) NOT NULL COMMENT 'โชว์'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `list_color`
--

INSERT INTO `list_color` (`list_color_id`, `list_color_price`, `list_color_stat`, `list_color_amount`, `addcolor_id`, `scolor_id`, `list_color_dc`, `com_id`, `notShow`) VALUES
(1, 1000, 0, 11, 'adc0001', 'so0001', 0, 'cm0001', 1),
(2, 1000, 0, 10, 'adc0002', 'so0002', 10, 'cm0001', 1),
(3, 1000, 0, 10, 'adc0003', 'so0002', 10, 'cm0001', 1),
(4, 1000, 0, 10, 'adc0003', 'so0002', 10, 'cm0001', 1),
(5, 1000, 0, 10, 'adc0002', 'so0001', 10, 'cm0001', 1),
(6, 1000, 0, 10, 'adc0002', 'so0001', 10, 'cm0001', 1),
(7, 1000, 0, 10, 'adc0002', 'so0001', 10, 'cm0001', 1),
(8, 1000, 0, 13, 'adc0004', 'so0002', 10, 'cm0001', 1),
(9, 1000, 0, 5, 'adc0005', 'so0001', 10, 'cm0001', 1),
(11, 1000, 1, 10, 'adc0006', 'so0003', 10, 'cm0001', 1),
(12, 1000, 0, 10, 'adc0005', 'so0005', 10, 'cm0001', 0),
(13, 1000, 0, 10, 'adc0007', 'so0002', 10, 'cm0001', 0),
(14, 1000, 0, 10, 'adc0008', 'so0005', 10, 'cm0001', 0);

-- --------------------------------------------------------

--
-- Table structure for table `list_consumable`
--

CREATE TABLE `list_consumable` (
  `list_cons_id` int(11) NOT NULL COMMENT 'รหัสรายการเพิ่มวัสดุลงสต๊อค',
  `list_cons_price` float NOT NULL COMMENT 'ราคาวัสดุสิ้นเปลื้อง',
  `list_cons_stat` tinyint(1) NOT NULL COMMENT 'สถานะการอนัติ/ไม่อนุมัติ',
  `list_cons_amount` int(11) NOT NULL COMMENT 'จำนวนวัสดุที่เพิ่มลงสต๊อค',
  `atc_id` varchar(11) NOT NULL COMMENT 'รหัสใบเพิ่มวัสดุลงสต๊อค',
  `cons_id` varchar(11) NOT NULL COMMENT 'รหัสสต๊อควัสดุสิ้นเปลื้อง',
  `com_id` varchar(11) NOT NULL,
  `notShow` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `list_consumable`
--

INSERT INTO `list_consumable` (`list_cons_id`, `list_cons_price`, `list_cons_stat`, `list_cons_amount`, `atc_id`, `cons_id`, `com_id`, `notShow`) VALUES
(1, 1000, 0, 11, 'atc0001', 'cn0001', 'cm0001', 1),
(7, 0, 0, 0, 'atc0002', 'cn0002', '', 1),
(8, 1000, 0, 10, 'atc0003', 'cn0003', '', 1),
(9, 1000, 0, 10, 'atc0003', 'cn0003', '', 1),
(10, 1000, 0, 10, 'atc0003', 'cn0003', '', 1),
(11, 1000, 0, 10, 'atc0003', 'cn0003', '', 1),
(12, 1000, 0, 10, 'atc0002', 'cn0002', '', 1),
(13, 1000, 0, 10, 'atc0002', 'cn0002', 'cm0001', 1),
(14, 1000, 0, 10, 'atc0002', 'cn0003', 'cm0001', 1),
(16, 10000, 0, 10, 'atc0001', 'cn0001', 'cm0001', 1),
(17, 1000, 0, 10, 'atc0003', 'cn0002', 'cm0001', 1),
(18, 1000, 0, 5, 'atc0003', 'cn0002', 'cm0001', 1),
(21, 1000, 0, 10, 'atc0004', 'cn0005', 'cm0001', 1),
(22, 1000, 1, 10, 'atc0005', 'cn0003', 'cm0001', 1),
(23, 1000, 0, 10, 'atc0006', 'cn0004', 'cm0001', 1),
(24, 1000, 1, 10, 'atc0007', 'cn0004', 'cm0001', 1);

-- --------------------------------------------------------

--
-- Table structure for table `list_spare`
--

CREATE TABLE `list_spare` (
  `list_spare_id` int(11) NOT NULL COMMENT 'รหัสรายการเพิ่มอะไหล่ลง',
  `list_spare_price` float NOT NULL COMMENT 'ราคาวัสดุอะไหล่',
  `list_spare_stat` tinyint(1) NOT NULL COMMENT 'สถานะการอนัติ/ไม่อนุมัติ',
  `list_spare_amount` int(11) NOT NULL COMMENT 'จำนวนอะไหล่ที่เพิ่มลงสต๊อค',
  `ATS_id` varchar(11) NOT NULL COMMENT 'รหัสใบเพิ่มอะไหล่ลงสต๊อค',
  `spare_id` varchar(11) NOT NULL COMMENT 'รหัสเพิ่มอะไหล่ลงสต๊อค',
  `com_id` varchar(11) NOT NULL,
  `notShow` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `list_spare`
--

INSERT INTO `list_spare` (`list_spare_id`, `list_spare_price`, `list_spare_stat`, `list_spare_amount`, `ATS_id`, `spare_id`, `com_id`, `notShow`) VALUES
(1, 1000, 0, 10, 'ats0001', 'sp0002', 'cm0001', 1),
(2, 1000, 0, 10, 'ats0002', 'sp0002', 'cm0001', 1),
(4, 1000, 0, 10, 'ats0004', 'sp0002', 'cm0001', 1),
(6, 1000, 0, 10, 'ats0006', 'sp0004', 'cm0001', 1),
(9, 1000, 0, 10, 'ats0002', 'sp0002', 'cm0001', 1),
(10, 1000, 0, 10, 'ats0002', 'sp0003', 'cm0001', 1),
(11, 1000, 0, 10, 'ats0002', 'sp0004', 'cm0001', 1),
(12, 1000, 0, 10, 'ats0004', 'sp0002', 'cm0001', 1),
(13, 1000, 0, 10, 'ats0001', 'sp0001', 'cm0002', 1),
(14, 1000, 0, 10, 'ats0001', 'sp0001', 'cm0002', 1),
(15, 1000, 1, 20, 'ats0001', 'sp0001', 'cm0002', 1),
(16, 1000, 0, 20, 'ats0002', 'sp0001', 'cm0001', 1),
(17, 1000, 0, 20, 'ats0002', 'sp0001', 'cm0001', 1),
(18, 1000, 0, 30, 'ats0002', 'sp0002', 'cm0001', 1),
(19, 1000, 0, 43, 'ats0002', 'sp0002', 'cm0001', 1),
(20, 1000, 0, 283, 'ats0008', 'sp0001', 'cm0001', 1),
(21, 1000, 0, 263, 'ats0008', 'sp0001', 'cm0001', 1),
(22, 1000, 0, 273, 'ats0008', 'sp0001', 'cm0001', 1),
(23, 1000, 0, 283, 'ats0008', 'sp0001', 'cm0001', 1),
(24, 1000, 0, 263, 'ats0008', 'sp0001', 'cm0001', 1),
(25, 1000, 0, 273, 'ats0008', 'sp0001', 'cm0001', 1),
(26, 1000, 0, 10, 'ats0008', 'sp0001', 'cm0001', 1),
(27, 1000, 0, 10, 'ats0008', 'sp0001', 'cm0001', 1),
(28, 1000, 0, 20, 'ats0008', 'sp0001', 'cm0001', 1),
(29, 1000, 0, 30, 'ats0008', 'sp0001', 'cm0001', 1),
(30, 1000, 0, 35, 'ats0008', 'sp0001', 'cm0001', 1),
(34, 1000, 0, 55, 'ats0004', 'sp0001', 'cm0001', 1),
(36, 1000, 0, 24, 'ats0003', 'sp0005', 'cm0001', 1),
(38, 1000, 0, 25, 'ats0007', 'sp0004', 'cm0001', 0),
(39, 1000, 0, 30, 'ats0007', 'sp0005', 'cm0001', 0),
(41, 1000, 0, 25, 'ats0010', 'sp0004', 'cm0001', 0),
(42, 1000, 0, 25, 'ats0010', 'sp0004', 'cm0001', 0),
(43, 1000, 0, 25, 'ats0011', 'sp0004', 'cm0001', 0),
(44, 200, 1, 1, 'ats0012', 'sp0012', 'cm0002', 1),
(46, 23212, 0, 3, 'ats0012', 'sp0013', 'cm0001', 0),
(47, 1000, 1, 15, 'ats0013', 'sp0013', 'cm0001', 1),
(48, 500, 0, 37, 'ats0011', 'sp0005', 'cm0001', 0),
(49, 3000, 0, 15, 'ats0011', 'sp0008', 'cm0001', 0);

-- --------------------------------------------------------

--
-- Table structure for table `offer_price`
--

CREATE TABLE `offer_price` (
  `op_id` varchar(11) NOT NULL COMMENT 'รหัสใบเสนอราคา',
  `op_date` date NOT NULL COMMENT 'วันที่เสนอราคา',
  `op_confirm` tinyint(1) NOT NULL COMMENT 'ยืนยันการเสนอราคา',
  `op_spare_price` float NOT NULL COMMENT 'เก็บราคาอะไหล่',
  `op_color_price` float NOT NULL COMMENT 'เก็บราคาสี',
  `op_other_price` float NOT NULL COMMENT 'เก็บราคาอื่นๆ',
  `op_paystat` tinyint(1) NOT NULL COMMENT 'เก็บสถานะจ่ายเงินสด/ประกัน',
  `rep_id` varchar(11) NOT NULL COMMENT 'รหัสการซ่อม'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `offer_price`
--

INSERT INTO `offer_price` (`op_id`, `op_date`, `op_confirm`, `op_spare_price`, `op_color_price`, `op_other_price`, `op_paystat`, `rep_id`) VALUES
('op0001', '2017-12-02', 1, 10000, 10000, 10000, 1, 're0001'),
('op0002', '2018-02-07', 2, 3000, 4000, 5000, 1, 're0001');

-- --------------------------------------------------------

--
-- Table structure for table `order_color`
--

CREATE TABLE `order_color` (
  `oc_id` varchar(11) NOT NULL COMMENT 'รหัสใบสั่งซื้อสี',
  `oc_order_date` date NOT NULL COMMENT 'วันที่สั่งซื้อสี',
  `oc_send_date` date NOT NULL COMMENT 'จำนวนวันที่คาดว่าจะได้รับของ',
  `oc_department` varchar(25) NOT NULL COMMENT 'แผนกที่สั่งซื้อ',
  `oc_name_contact` varchar(25) NOT NULL COMMENT 'ชื่อผู้ติดต่อ',
  `emp_id` varchar(11) NOT NULL COMMENT 'รหัสพนักงาน',
  `com_id` varchar(11) NOT NULL COMMENT 'รหัสบริษัทที่สั่งซื้อ',
  `rep_id` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `order_color`
--

INSERT INTO `order_color` (`oc_id`, `oc_order_date`, `oc_send_date`, `oc_department`, `oc_name_contact`, `emp_id`, `com_id`, `rep_id`) VALUES
('ci0001', '2017-12-20', '2017-12-27', 'เคาะพ่นสี', 'ธนกร', 'em0001', 'cm0001', 're0001'),
('ci0002', '2018-01-05', '2018-01-10', 'ถอดอะไหล่', 'บะหมี่โหน่ง', 'em0001', 'cm0001', 're0001'),
('ci0003', '2018-01-05', '2018-01-12', 'ถอดอะไหล่', 'บะหมี่โหน่ง', 'em0001', 'cm0001', 're0001'),
('ci0004', '2018-02-20', '2018-02-22', 'ถอดอะไหล่', 'ชลวัช', 'em0001', 'cm0001', 're0001'),
('ci0005', '2018-02-20', '2018-02-22', 'เตรียมพื้น', 'ชลวัช', 'em0001', 'cm0001', 're0001'),
('ci0006', '2018-02-27', '2018-02-08', 'เตรียมพื้น', 'ชลวัช', 'em0001', 'cm0001', 're0001'),
('ci0007', '2018-02-27', '2018-02-14', 'ถอดอะไหล่', 'ชลวัช', 'em0001', 'cm0001', 're0001');

-- --------------------------------------------------------

--
-- Table structure for table `order_color_amount`
--

CREATE TABLE `order_color_amount` (
  `oca_id` int(11) NOT NULL COMMENT 'รหัสจำนวนที่สั่งซื้อสี',
  `oca_price` float NOT NULL COMMENT 'ราคาสีที่สั่งซื้อ',
  `oca_amount` int(11) NOT NULL COMMENT 'จำนวนสีที่สั่งซื้อ',
  `oca_unit` varchar(25) NOT NULL COMMENT 'หน่วยนับ',
  `oca_discount` float NOT NULL COMMENT 'ส่วนลด',
  `oca_stat` int(11) NOT NULL,
  `scolor_id` varchar(11) NOT NULL,
  `oca_quantity` varchar(25) NOT NULL,
  `oc_id` varchar(11) NOT NULL,
  `notShow` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `order_color_amount`
--

INSERT INTO `order_color_amount` (`oca_id`, `oca_price`, `oca_amount`, `oca_unit`, `oca_discount`, `oca_stat`, `scolor_id`, `oca_quantity`, `oc_id`, `notShow`) VALUES
(1, 1000, 1, 'แกลลอน', 15, 1, 'so0001', '10000', 'ci0001', 1),
(2, 14500, 10, 'อัน', 15, 1, 'so0001', '10000', 'ci0002', 1),
(3, 14500, 10, 'อัน', 15, 1, 'so0001', '10000', 'ci0002', 1),
(4, 14500, 10, 'อัน', 15, 0, 'so0001', '10000', 'ci0003', 0),
(5, 14500, 10, 'อัน', 15, 1, 'so0001', '10000', 'ci0003', 1),
(6, 14500, 10, 'อัน', 15, 0, 'so0001', '10000', 'ci0003', 0),
(7, 14500, 10, 'อัน', 15, 0, 'so0001', '10000', 'ci0002', 1),
(8, 14500, 10, 'อัน', 15, 0, 'so0001', '10000', 'ci0002', 0),
(9, 10000, 10, 'อัน', 15, 0, 'so0002', '1000', 'ci0002', 0),
(10, 14500, 12, 'อัน', 15, 0, 'so0003', '1000', 'ci0002', 0),
(11, 14500, 12, 'อัน', 15, 0, 'so0003', '1000', 'ci0002', 0),
(12, 14500, 10, 'อัน', 18, 0, 'so0004', '1000', 'ci0002', 0),
(13, 14500, 10, 'อัน', 18, 0, 'so0004', '1000', 'ci0002', 0),
(14, 14510, 10, 'ถัง', 15, 0, 'so0005', '1000', 'ci0002', 0),
(15, 14500, 10, 'อัน', 15, 0, 'so0003', '10000', 'ci0002', 0),
(16, 14500, 10, 'อัน', 15, 0, 'so0003', '10000', 'ci0002', 0),
(17, 14500, 10, 'อัน', 15, 0, 'so0003', '10000', 'ci0002', 0),
(18, 14500, 10, 'อัน', 15, 0, 'so0003', '10000', 'ci0002', 0),
(19, 14500, 10, 'อัน', 15, 0, 'so0003', '10000', 'ci0002', 0),
(20, 14500, 10, 'อัน', 15, 0, 'so0003', '10000', 'ci0002', 0),
(21, 14500, 10, 'อัน', 15, 0, 'so0003', '10000', 'ci0002', 0),
(22, 14500, 10, 'อัน', 15, 0, 'so0003', '10000', 'ci0002', 0),
(23, 14500, 10, 'อัน', 15, 0, 'so0005', '10000', 'ci0002', 0),
(24, 14500, 10, 'อัน', 15, 0, 'so0005', '10000', 'ci0002', 0),
(25, 14500, 10, 'อัน', 15, 0, 'so0005', '10000', 'ci0002', 0),
(26, 14500, 10, 'อัน', 15, 0, 'so0003', '10000', 'ci0004', 0);

-- --------------------------------------------------------

--
-- Table structure for table `order_consumable`
--

CREATE TABLE `order_consumable` (
  `om_id` varchar(11) NOT NULL COMMENT 'รหัสใบสั่งซื้อวัสดุ',
  `om_order_date` date NOT NULL COMMENT 'วันที่สั่งซื้อวัสดุ',
  `om_send_date` int(11) NOT NULL COMMENT 'จำนวนวันที่คาดว่าจะได้รับของ',
  `om_department` varchar(25) NOT NULL COMMENT 'แผนกที่สั่งซื้อ',
  `om_name_contact` varchar(25) NOT NULL COMMENT 'ชื่อผู้ติดต่อ',
  `emp_id` varchar(11) NOT NULL COMMENT 'รหัสพนักงาน',
  `com_id` varchar(11) NOT NULL COMMENT 'บริษัทที่สั่งซื้อ',
  `rep_id` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `order_consumable`
--

INSERT INTO `order_consumable` (`om_id`, `om_order_date`, `om_send_date`, `om_department`, `om_name_contact`, `emp_id`, `com_id`, `rep_id`) VALUES
('om0001', '2017-12-13', 3, 'เคาะพ่นสี', 'ธนกร', 'em0001', 'cm0001', 're0001'),
('om0002', '2018-01-06', 2018, 'ถอดอะไหล่', 'บะหมี่โหน่ง', 'em0001', 'cm0001', 're0001'),
('om0003', '2018-01-08', 2018, 'ถอดอะไหล่', 'บะหมี่โหน่ง', 'em0001', 'cm0001', 're0001'),
('om0004', '2018-02-20', 2018, 'ถอดอะไหล่', 'ชลวัช', 'em0001', 'cm0001', 're0003'),
('om0005', '2018-02-20', 2018, 'ถอดอะไหล่', 'ชลวัช', 'em0001', 'cm0001', 're0001'),
('om0006', '2018-02-27', 2018, 'เตรียมพื้น', 'ชลวัช', 'em0001', 'cm0001', 're0001');

-- --------------------------------------------------------

--
-- Table structure for table `order_spare`
--

CREATE TABLE `order_spare` (
  `os_id` varchar(11) NOT NULL COMMENT 'รหัสใบสั่งซื้อออะไหล่',
  `os_order_date` date NOT NULL COMMENT 'วันที่สั่งซื้ออะไหล่',
  `os_send_date` int(11) NOT NULL COMMENT 'จำนวนวันที่คาดว่าจะได้รับของ',
  `os_department` varchar(25) NOT NULL COMMENT 'แผนกที่สั่งซื้อ',
  `os_name_contact` varchar(25) NOT NULL COMMENT 'ชื่อผู้ติดต่อ',
  `emp_id` varchar(11) NOT NULL COMMENT 'รหัสพนักงาน',
  `os_date` date NOT NULL COMMENT 'วันที่ได้รับของ',
  `com_id` varchar(11) NOT NULL COMMENT 'รหัสบริษัทที่สั่งซื้อ',
  `rep_id` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `order_spare`
--

INSERT INTO `order_spare` (`os_id`, `os_order_date`, `os_send_date`, `os_department`, `os_name_contact`, `emp_id`, `os_date`, `com_id`, `rep_id`) VALUES
('si0001', '2017-12-07', 3, 'ถอดอะไหล่', 'บะหมี่โหน่ง', 'em0001', '2017-12-07', 'cm0002', 're0001'),
('si0002', '2017-12-07', 3, 'ถอดอะไหล่', 'บะหมี่โหน่ง', 'em0001', '2017-12-14', 'cm0001', 're0001'),
('si0003', '2017-12-28', 3, 'ถอดอะไหล่', 'บะหมี่โหน่ง', 'em0001', '2017-12-06', 'cm0001', 're0001'),
('si0004', '2018-01-05', 3, 'ถอดอะไหล่', 'บะหมี่โหน่ง', 'em0001', '2018-01-10', 'cm0001', 're0001'),
('si0005', '2018-01-10', 3, 'ถอดอะไหล่', 'บะหมี่โหน่ง', 'em0001', '2018-01-11', 'cm0001', 're0001'),
('si0006', '2018-01-24', 1, 'พ่นสี', 'ธนกร', 'em0001', '2018-01-26', 'cm0001', 're0001'),
('si0007', '2018-01-24', 3, 'ถอดอะไหล่', 'บะหมี่โหน่ง', 'em0001', '2018-01-17', 'cm0001', 're0001'),
('si0008', '2018-01-24', 3, 'ถอดอะไหล่', 'บะหมี่โหน่ง', 'em0001', '2018-01-17', 'cm0001', 're0001'),
('si0009', '2018-02-19', 3, 'เตรียมพื้น', 'บะหมี่โหน่ง', 'em0001', '2018-02-13', 'cm0001', 're0001'),
('si0010', '2018-02-20', 3, 'เตรียมพื้น', 'ชลวัช', 'em0001', '2018-02-20', 'cm0001', 're0003'),
('si0011', '2018-02-20', 3, 'เตรียมพื้น', 'ชลวัช', 'em0001', '2018-02-21', 'cm0001', 're0003'),
('si0012', '2018-02-20', 3, 'ถอดอะไหล่', 'ชลวัช', 'em0001', '2018-02-21', 'cm0001', 're0001'),
('si0013', '2018-02-20', 5, 'ถอดอะไหล่', 'ธนกร', 'em0001', '2018-02-07', 'cm0001', 're0003'),
('si0014', '2018-02-26', 3, 'เตรียมพื้น', 'บะหมี่โหน่ง', 'em0001', '2018-02-14', 'cm0001', 're0001'),
('si0015', '2018-02-26', 3, 'ถอดอะไหล่', 'ชลวัข', 'em0001', '2018-02-15', 'cm0001', 're0003'),
('si0016', '2018-02-26', 3, 'ถอดอะไหล่', 'ชลวัช', 'em0001', '2018-02-15', 'cm0001', 're0001'),
('si0017', '2018-02-27', 3, 'เตรียมพื้น', 'ชลวัช', 'em0001', '2018-02-14', 'cm0001', 're0001');

-- --------------------------------------------------------

--
-- Table structure for table `order_spare_amount`
--

CREATE TABLE `order_spare_amount` (
  `osa_id` int(11) NOT NULL COMMENT 'รหัสจำนวนที่สั่งซื้ออะไหล่',
  `osa_price` float NOT NULL COMMENT 'ราคาอะไหล่ที่สั่ง',
  `osa_amount` int(11) NOT NULL COMMENT 'จำนวนอะไหล่ที่สั่ง',
  `osa_unit` varchar(25) NOT NULL COMMENT 'หน่วยนับ',
  `osa_discount` float NOT NULL COMMENT 'ส่วนลดอะไหล่',
  `osa_stat` int(11) NOT NULL,
  `spare_id` varchar(11) NOT NULL,
  `os_id` varchar(11) NOT NULL,
  `notShow` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `order_spare_amount`
--

INSERT INTO `order_spare_amount` (`osa_id`, `osa_price`, `osa_amount`, `osa_unit`, `osa_discount`, `osa_stat`, `spare_id`, `os_id`, `notShow`) VALUES
(0, 14500, 10, '10', 15, 1, 'sp0002', 'si0001', 1),
(244, 14500, 10, '10', 15, 0, 'sp0002', 'si0002', 0),
(245, 14500, 10, '10', 15, 0, 'sp0002', 'si0002', 0),
(246, 14500, 10, '10', 15, 0, 'sp0002', 'si0003', 0),
(247, 14500, 10, '10', 15, 0, 'sp0002', 'si0003', 0),
(249, 14500, 10, '10', 15, 1, 'sp0002', 'si0003', 1),
(251, 14500, 10, '10', 15, 0, 'sp0001', 'si0004', 0),
(252, 14500, 10, '10', 15, 1, 'sp0004', 'si0001', 1),
(253, 14500, 10, '10', 15, 0, 'sp0004', 'si0001', 0),
(254, 10000, 10, '10', 7, 0, 'sp0003', 'si0003', 0),
(255, 10000, 10, '10', 7, 0, 'sp0004', 'si0006', 0),
(256, 14500, 10, '10', 15, 0, 'sp0009', 'si0007', 0),
(259, 14500, 4, '4', 15, 0, 'sp0003', 'si0006', 0),
(260, 14500, 4, '4', 15, 0, 'sp0004', 'si0008', 0),
(261, 14500, 10, '10', 15, 0, 'sp0005', 'si0003', 0),
(262, 14500, 10, '10', 15, 0, 'sp0005', 'si0003', 0),
(263, 14500, 10, '10', 15, 0, 'sp0005', 'si0003', 0),
(265, 14500, 10, '10', 15, 0, 'sp0005', 'si0003', 0),
(266, 14500, 10, '10', 15, 0, 'sp0006', 'si0003', 0),
(270, 14500, 10, '10', 15, 0, 'sp0003', 'si0005', 0),
(271, 14500, 10, '10', 15, 0, 'sp0003', 'si0005', 0),
(272, 14500, 10, '10', 15, 0, 'sp0003', 'si0005', 0),
(279, 14500, 10, '10', 15, 0, 'sp0004', 'si0008', 0),
(280, 14500, 10, '10', 14, 0, 'sp0004', 'si0008', 0),
(282, 10000, 10, '10', 15, 0, 'sp0004', 'si0011', 0),
(283, 14500, 10, '10', 15, 0, 'sp0003', 'si0012', 0),
(284, 14500, 10, '10', 15, 0, 'sp0003', 'si0012', 0),
(285, 10000, 10, '10', 15, 0, 'sp0003', 'si0012', 0),
(287, 1000, 15, '15', 10, 1, 'sp0002', 'si0013', 1),
(288, 1000, 15, '15', 10, 0, 'sp0002', 'si0013', 1),
(289, 500, 3, '3', 10, 0, 'sp0012', 'si0002', 0),
(290, 14500, 10, '10', 15, 0, 'sp0011', 'si0016', 0),
(291, 14500, 10, '10', 15, 0, 'sp0011', 'si0016', 0);

-- --------------------------------------------------------

--
-- Table structure for table `rcon_list`
--

CREATE TABLE `rcon_list` (
  `rcon_id` int(11) NOT NULL,
  `rcb_amount` int(11) NOT NULL,
  `rcb_status` int(11) NOT NULL,
  `tn_id` varchar(11) NOT NULL,
  `cons_id` varchar(11) NOT NULL,
  `emp_id` varchar(11) NOT NULL,
  `rcb_id` varchar(11) NOT NULL,
  `notShow` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rcon_list`
--

INSERT INTO `rcon_list` (`rcon_id`, `rcb_amount`, `rcb_status`, `tn_id`, `cons_id`, `emp_id`, `rcb_id`, `notShow`) VALUES
(1, 1, 0, 'tn0001', 'cn0001', 'em0001', 'rb0001', 1),
(2, 13, 0, 'tn0001', 'cn0002', 'em0001', 'rb0002', 1),
(3, 14, 0, 'tn0001', 'cn0003', 'em0001', 'rb0004', 1),
(4, 14, 0, 'tn0001', 'cn0003', 'em0001', 'rb0004', 1),
(5, 7, 0, 'tn0001', 'cn0003', 'em0001', 'rb0003', 1),
(6, 5, 0, 'tn0001', 'cn0002', 'em0001', 'rb0002', 1),
(9, 10, 1, 'tn0001', 'cn0001', 'em0001', 'rb0006', 1),
(10, 10, 1, 'tn0001', 'cn0007', 'em0001', 'rb0007', 1),
(11, 10, 0, 'tn0002', 'cn0003', 'em0001', 'rb0008', 0),
(14, 5, 0, 'tn0002', 'cn0004', 'em0001', 'rb0009', 0);

-- --------------------------------------------------------

--
-- Table structure for table `repair`
--

CREATE TABLE `repair` (
  `rep_id` varchar(11) NOT NULL COMMENT 'รหัสการซ่อม',
  `rep_mile` int(11) NOT NULL COMMENT 'ไมล์รถ',
  `re_status_ir` tinyint(1) NOT NULL COMMENT 'สถานะประกัน',
  `cr_id` varchar(11) NOT NULL,
  `sc_id` varchar(11) NOT NULL,
  `emp_id` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `repair`
--

INSERT INTO `repair` (`rep_id`, `rep_mile`, `re_status_ir`, `cr_id`, `sc_id`, `emp_id`) VALUES
('re0001', 10000, 1, 'cr0001', 'sc0001', 'em0001'),
('re0002', 0, 1, 'cr0001', 'sc0001', 'em0001'),
('re0003', 0, 1, 'cr0001', 'sc0001', 'em0001'),
('re0004', 0, 1, 'cr0001', 'sc0001', 'em0001'),
('re0005', 0, 1, 'cr0001', 'sc0001', 'em0001'),
('re0006', 0, 1, 'cr0001', 'sc0001', 'em0001'),
('re0007', 0, 1, 'cr0001', 'sc0001', 'em0001'),
('re0008', 0, 1, 'cr0001', 'sc0001', 'em0001'),
('re0009', 0, 1, 'cr0001', 'sc0001', 'em0001');

-- --------------------------------------------------------

--
-- Table structure for table `repair_list`
--

CREATE TABLE `repair_list` (
  `rl_id` varchar(11) NOT NULL COMMENT 'รหัสรายการซ่อม',
  `rl_name` varchar(25) NOT NULL COMMENT 'ชื่อรายการซ่อม',
  `rl_wages` float NOT NULL COMMENT 'ค่าแรง',
  `rl_lbcost` float NOT NULL COMMENT 'ส่วนลดค่าแรง',
  `rl_dclb` float NOT NULL COMMENT 'ส่วนลด',
  `rl_finish` datetime NOT NULL COMMENT 'เวลาเสร็จ',
  `rl_dice` varchar(5) NOT NULL COMMENT 'ลูกเต๋า',
  `tn_amount` int(11) NOT NULL COMMENT 'จำนวนช่าง',
  `rl_type` varchar(25) NOT NULL COMMENT 'ชนิดของรายการซ่อม',
  `brl_id` varchar(11) NOT NULL COMMENT 'รหัสรายการซ่อม'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `repair_list`
--

INSERT INTO `repair_list` (`rl_id`, `rl_name`, `rl_wages`, `rl_lbcost`, `rl_dclb`, `rl_finish`, `rl_dice`, `tn_amount`, `rl_type`, `brl_id`) VALUES
('rl0001', 'ซ่อมประตู', 4000, 400, 5, '2017-12-08 00:00:00', '15', 15, 'ซ่อม', 'brl0001'),
('rl0002', 'ขัดสี', 199600, 19960, 10, '2018-01-30 12:30:34', '20', 5, 'เปลี่ยน', 'brl0001'),
('rl0003', 'เผา', 199600, 19960, 10, '2018-01-30 12:30:34', '20', 5, 'เปลี่ยน', 'brl0001'),
('rl004', 'ซ่อมมมมมม', 999999, 99, 10, '2018-02-09 00:00:00', '13', 5, 'เปลี่ยน', 'brl0001');

-- --------------------------------------------------------

--
-- Table structure for table `repair_list2`
--

CREATE TABLE `repair_list2` (
  `rl_id2` varchar(11) NOT NULL COMMENT 'รหัสรายการซ่อม2',
  `rl_table_start` datetime NOT NULL COMMENT 'ตารางเริ่มงาน',
  `rl_table_deadline` datetime NOT NULL COMMENT 'ตารางกำหนดงานเสร็จ',
  `rl_id` varchar(11) NOT NULL COMMENT 'รหัสรายการซ่อม',
  `tn_id` varchar(11) NOT NULL COMMENT 'รหัสช่าง'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `repair_list2`
--

INSERT INTO `repair_list2` (`rl_id2`, `rl_table_start`, `rl_table_deadline`, `rl_id`, `tn_id`) VALUES
('r2001', '2017-12-01 00:00:00', '2017-12-05 00:00:00', 'rl0001', 'tn0001'),
('r2002', '2018-01-27 01:07:02', '2018-01-27 04:15:00', 'rl0002', 'tn0002'),
('r2003', '2018-01-27 13:17:00', '2018-01-27 16:35:00', 'rl0003', 'tn0002'),
('r2004', '2018-02-20 08:00:00', '2018-02-20 12:00:00', 'rl0003', 'tn0005'),
('r2005', '2018-02-20 11:00:00', '2018-02-20 16:00:00', 'rl004', 'tn0004'),
('r2006', '2018-02-20 13:24:00', '2018-02-20 15:30:00', 'rl0001', 'tn0006'),
('r2007', '2018-02-20 09:07:00', '2018-02-21 11:35:00', 'rl0002', 'tn0003');

-- --------------------------------------------------------

--
-- Table structure for table `return_color`
--

CREATE TABLE `return_color` (
  `rc_id` varchar(11) NOT NULL COMMENT 'รหัสใบคืนสี',
  `rc_date` date NOT NULL COMMENT 'วันที่ออกใบคืนสี',
  `color_id` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `return_color`
--

INSERT INTO `return_color` (`rc_id`, `rc_date`, `color_id`) VALUES
('rci0001', '2017-12-19', 'co0001'),
('rci0002', '2018-01-19', 'co0002'),
('rci0003', '2018-02-19', 'co0003'),
('rci0004', '2018-02-19', 'co0004'),
('rci0005', '2018-02-19', 'co0002'),
('rci0006', '2018-02-19', 'co0001'),
('rci0007', '2018-02-20', 'co0004'),
('rci0008', '2018-02-20', 'co0001');

-- --------------------------------------------------------

--
-- Table structure for table `return_consumable`
--

CREATE TABLE `return_consumable` (
  `rcb_id` varchar(11) NOT NULL COMMENT 'รหัสใบคืนวัสดุสิ้นเปลื้อง',
  `rcb_date` date NOT NULL COMMENT 'วันที่ออกใบคืนวัสดุ',
  `bcons_id` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `return_consumable`
--

INSERT INTO `return_consumable` (`rcb_id`, `rcb_date`, `bcons_id`) VALUES
('rb0001', '2017-12-20', 'bn0001'),
('rb0002', '2018-01-20', 'bn0002'),
('rb0003', '2018-01-20', 'bn0003'),
('rb0004', '2018-01-20', 'bn0004'),
('rb0005', '2018-02-19', 'bn0001'),
('rb0006', '2018-02-19', 'bn0001'),
('rb0007', '2018-02-19', 'bn0001'),
('rb0008', '2018-02-20', 'bn0001'),
('rb0009', '2018-02-20', 'bn0004');

-- --------------------------------------------------------

--
-- Table structure for table `return_spare`
--

CREATE TABLE `return_spare` (
  `rs_id` varchar(11) NOT NULL COMMENT 'รหัสใบคืนอะไหล่',
  `rs_date` date NOT NULL COMMENT 'วันที่ออกใบคืนอะไหล่',
  `bol_id` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `return_spare`
--

INSERT INTO `return_spare` (`rs_id`, `rs_date`, `bol_id`) VALUES
('rs0001', '2018-01-09', 'bs0001'),
('rs0002', '2018-01-18', 'bs0002'),
('rs0003', '2018-01-20', 'bs0003'),
('rs0004', '2018-02-01', 'bs0003'),
('rs0005', '2018-02-19', 'bs0004'),
('rs0006', '2018-02-19', 'bs0004'),
('rs0007', '2018-02-19', 'bs0002'),
('rs0008', '2018-02-19', 'bs0004'),
('rs0009', '2018-02-19', 'bs0001'),
('rs0010', '2018-02-20', 'bs0002'),
('rs0011', '2018-02-20', 'bs0003'),
('rs0012', '2018-02-20', 'bs0004');

-- --------------------------------------------------------

--
-- Table structure for table `rtc_list`
--

CREATE TABLE `rtc_list` (
  `rtc_id` int(11) NOT NULL,
  `rc_amount` int(11) NOT NULL,
  `rc_status` int(11) NOT NULL,
  `tn_id` varchar(11) CHARACTER SET utf8 NOT NULL,
  `emp_id` varchar(11) CHARACTER SET utf8 NOT NULL,
  `rc_id` varchar(11) CHARACTER SET utf8 NOT NULL,
  `scolor_id` varchar(11) CHARACTER SET utf8 NOT NULL,
  `notShow` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rtc_list`
--

INSERT INTO `rtc_list` (`rtc_id`, `rc_amount`, `rc_status`, `tn_id`, `emp_id`, `rc_id`, `scolor_id`, `notShow`) VALUES
(1, 10, 0, 'tn0001', 'em0001', 'rci0001', 'so0001', 1),
(2, 10, 0, 'tn0001', 'em0001', 'rci0001', 'so0001', 1),
(3, 10, 0, 'tn0001', 'em0001', 'rci0001', 'so0001', 1),
(9, 10, 0, 'tn0001', 'em0001', 'rci0002', 'so0003', 1),
(10, 10, 0, 'tn0001', 'em0001', 'rci0003', 'so0002', 1),
(11, 10, 0, 'tn0001', 'em0001', 'rci0002', 'so0003', 1),
(12, 10, 0, 'tn0001', 'em0001', 'rci0002', 'so0003', 1),
(13, 10, 0, 'tn0001', 'em0001', 'rci0002', 'so0005', 1),
(14, 10, 0, 'tn0001', 'em0001', 'rci0004', 'so0005', 1),
(15, 10, 1, 'tn0001', 'em0001', 'rci0004', 'so0003', 1),
(16, 10, 1, 'tn0001', 'em0001', 'rci0005', 'so0005', 1),
(17, 13, 1, 'tn0001', 'em0001', 'rci0006', 'so0005', 1),
(18, 10, 0, 'tn0002', 'em0001', 'rci0007', 'so0004', 0),
(19, 10, 0, 'tn0002', 'em0001', 'rci0008', 'so0004', 0);

-- --------------------------------------------------------

--
-- Table structure for table `rts_list`
--

CREATE TABLE `rts_list` (
  `rts_id` int(11) NOT NULL,
  `rs_amount` int(11) NOT NULL,
  `rs_status` int(11) NOT NULL,
  `tn_id` varchar(11) CHARACTER SET utf8 NOT NULL,
  `spare_id` varchar(11) CHARACTER SET utf8 NOT NULL,
  `emp_id` varchar(11) CHARACTER SET utf8 NOT NULL,
  `rs_id` varchar(11) CHARACTER SET utf8 NOT NULL,
  `notShow` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rts_list`
--

INSERT INTO `rts_list` (`rts_id`, `rs_amount`, `rs_status`, `tn_id`, `spare_id`, `emp_id`, `rs_id`, `notShow`) VALUES
(1, 10, 0, 'tn0001', 'sp0001', 'em0001', 'rs0001', 1),
(2, 10, 0, 'tn0001', 'sp0002', 'em0001', 'rs0001', 1),
(3, 10, 0, 'tn0001', 'sp0002', 'em0001', 'rs0001', 1),
(4, 10, 0, 'tn0001', 'sp0002', 'em0001', 'rs0001', 1),
(5, 10, 0, 'tn0001', 'sp0002', 'em0001', 'rs0001', 1),
(6, 10, 0, 'tn0001', 'sp0001', 'em0001', 'rs0001', 1),
(7, 10, 0, 'tn0001', 'sp0001', 'em0001', 'rs0001', 1),
(8, 14, 0, 'tn0001', 'sp0002', 'em0001', 'rs0003', 1),
(9, 4, 0, 'tn0001', 'sp0005', 'em0001', 'rs0003', 1),
(10, 10, 0, 'tn0001', 'sp0006', 'em0001', 'rs0004', 1),
(12, 10, 0, 'tn0001', 'sp0002', 'em0001', 'rs0005', 1),
(13, 10, 0, 'tn0002', 'sp0005', 'em0001', 'rs0005', 1),
(15, 10, 0, 'tn0001', 'sp0011', 'em0001', 'rs0006', 1),
(16, 10, 1, 'tn0001', 'sp0004', 'em0001', 'rs0006', 1),
(17, 10, 1, 'tn0001', 'sp0004', 'em0001', 'rs0007', 1),
(18, 10, 0, 'tn0001', 'sp0003', 'em0001', 'rs0008', 0),
(19, 10, 1, 'tn0002', 'sp0004', 'em0001', 'rs0009', 1),
(20, 10, 0, 'tn0001', 'sp0003', 'em0001', 'rs0009', 0),
(21, 10, 0, 'tn0001', 'sp0004', 'em0001', 'rs0010', 0),
(22, 10, 0, 'tn0002', 'sp0004', 'em0001', 'rs0011', 0),
(23, 1, 0, 'tn0002', 'sp0013', 'em0001', 'rs0012', 0),
(24, 1, 0, 'tn0002', 'sp0013', 'em0001', 'rs0012', 0);

-- --------------------------------------------------------

--
-- Table structure for table `sent_car`
--

CREATE TABLE `sent_car` (
  `sc_id` varchar(11) NOT NULL COMMENT 'รหัสใบส่งมอบรถ',
  `sc_datesent` date NOT NULL COMMENT 'วันที่ส่งมอบรถ',
  `sc_mile_be` int(11) NOT NULL COMMENT 'ไมล์รถก่อนซ่อม',
  `sc_mile_af` int(11) NOT NULL COMMENT 'ไมล์รถหลังซ่อม',
  `sc_name_reciept` varchar(25) NOT NULL COMMENT 'ชื่อผู้ที่มารับรถ',
  `sc_name_sent` varchar(25) NOT NULL COMMENT 'ชื่อผู้ที่ส่งมอบรถ'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sent_car`
--

INSERT INTO `sent_car` (`sc_id`, `sc_datesent`, `sc_mile_be`, `sc_mile_af`, `sc_name_reciept`, `sc_name_sent`) VALUES
('sc0001', '2017-12-07', 10000, 10001, 'ธนกร', 'เทพารักษ์');

-- --------------------------------------------------------

--
-- Table structure for table `spare`
--

CREATE TABLE `spare` (
  `spare_id` varchar(11) NOT NULL COMMENT 'รหัสอะไหล่',
  `spare_name` varchar(25) NOT NULL COMMENT 'ชื่ออะไหล่',
  `spare_type` varchar(25) NOT NULL COMMENT 'ชนิดอะไหล่',
  `spare_brand` varchar(25) NOT NULL COMMENT 'ยี่ห้ออะไหล่',
  `spare_gen` varchar(25) NOT NULL COMMENT 'รุ่นอะไหล่',
  `spare_color` varchar(25) NOT NULL COMMENT 'สีอะไหล่',
  `spare_price` float NOT NULL COMMENT 'ราคาอะไหล',
  `spare_dc` float NOT NULL COMMENT 'ส่วนลดอะไหล่',
  `spare_amount` int(11) NOT NULL COMMENT 'จำนวนอะไหล่'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `spare`
--

INSERT INTO `spare` (`spare_id`, `spare_name`, `spare_type`, `spare_brand`, `spare_gen`, `spare_color`, `spare_price`, `spare_dc`, `spare_amount`) VALUES
('sp0001', 'ประตู', 'เปลี่ยน', 'อีซูซุ', 'ดีแม็ก', 'ดำแดงเหลือง', 40000, 5, 75),
('sp0002', 'กันชน', 'เปลี่ยน', 'อีซูซุ', 'ดีแม็ก', 'เขียวขาว', 12000, 15, 277),
('sp0003', 'ไฟท้ายด้านซ้าย', 'รถเล็ก', 'มาสด้า', 'มาสด้า3', 'ขาว', 10000, 10, 143),
('sp0004', 'ไฟหน้าด้านซ้าย', 'รถเล็ก', 'อีซูซุ', 'ดีแม๊ก', 'ขาว', 10000, 10, 15),
('sp0005', 'ไฟเลี้ยวด้านซ้าย', 'รถเล็ก', 'มาสด้า', 'มาสด้า3', 'ขาว', 10000, 10, 35),
('sp0006', 'ไฟท้ายด้านซ้าย', 'รถเล็ก', 'อีซูซุ', 'ดีแม๊ก', 'ขาว', 10000, 10, 20),
('sp0007', 'ไฟท้ายด้านซ้าย', 'รถเล็ก', 'มาสด้า', 'มาสด้า3', 'ขาว', 10000, 10, 10),
('sp0008', 'ไฟท้ายด้านซ้าย', 'รถเล็ก', 'มาสด้า', 'มาสด้า3', 'ขาว', 10000, 10, 10),
('sp0009', 'ไฟท้ายด้านซ้าย', 'รถเล็ก', 'มาสด้า', 'มาสด้า3', 'ขาว', 10000, 10, 10),
('sp0010', 'ไฟท้ายด้านซ้าย', 'รถเล็ก', '', 'มาสด้า3', 'ขาว', 10000, 10, 0),
('sp0011', 'ไฟท้ายด้านซ้าย', 'รถเล็ก', 'ขาว', 'มาสด้า3', 'ขาว', 10000, 10, 0),
('sp0012', 'ไฟท้ายด้านซ้าย', 'รถเล็ก', 'ขาว', 'มาสด้า3', 'ขาว', 10000, 10, 1),
('sp0013', 'กันชน', 'รถเล็ก', 'ดำ', 'มาสด้า3', 'ดำ', 100000, 10, 15),
('sp0014', 'มาสด้า3', 'รถเล็ก', 'ขาว', 'มาสด้า3', 'ขาว', 10000, 10, 0),
('sp0015', 'มาสด้า3', 'รถเล็ก', 'ขาว', 'มาสด้า3', 'ขาว', 10000, 10, 0),
('sp0016', 'มาสด้า3', 'รถเล็ก', 'ขาว', 'มาสด้า3', 'ขาว', 10000, 10, 0);

-- --------------------------------------------------------

--
-- Table structure for table `stock_color`
--

CREATE TABLE `stock_color` (
  `scolor_id` varchar(11) NOT NULL COMMENT 'รหัสสต๊อกสี',
  `scolor_name` varchar(25) NOT NULL COMMENT 'ชื่อสี',
  `scolor_brand` varchar(25) NOT NULL COMMENT 'ยี่ห้อสี',
  `scolor_type` varchar(25) NOT NULL COMMENT 'ชนิดสี',
  `scolor_cost` float NOT NULL COMMENT 'ราคาสี',
  `scolor_quantity` float NOT NULL COMMENT 'ปริมาณสี',
  `scolor_amount` int(11) NOT NULL COMMENT 'จำนวนสี',
  `scolor_dc` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stock_color`
--

INSERT INTO `stock_color` (`scolor_id`, `scolor_name`, `scolor_brand`, `scolor_type`, `scolor_cost`, `scolor_quantity`, `scolor_amount`, `scolor_dc`) VALUES
('so0001', 'สีไม้ระบายน้ำ', 'นกแก้ว', 'น้ำ', 1000, 1000, 55, 10),
('so0002', 'ดำเงา', 'มาสด้า3', 'เลือกชนิด', 10, 10000, 43, 10),
('so0003', 'ดำเงา', 'cromax', 'สี OEM', 10000, 1000, 70, 10),
('so0004', 'ฟ้าคราม', 'cromax', 'สี OEM', 1000, 10000, 4, 10),
('so0005', 'ดำเงา', 'cromax', 'สี 1K', 1000, 10000, 43, 10),
('so0006', 'ไข่มุก', 'cromax', 'สี 1K', 10000, 10000, 0, 0),
('so0007', 'ดำไข่มุก', 'cromax', 'สี 1K', 10000, 10000, 0, 0),
('so0008', 'ดำเงา', 'cromax', 'สี 1K', 10000, 10000, 0, 0),
('so0009', 'ดำเงา', 'cromax', 'สี OEM', 10000, 10000, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `table_check`
--

CREATE TABLE `table_check` (
  `tc_id` varchar(11) NOT NULL COMMENT 'รหัสตารางตรวจสอบ',
  `tc_date` date NOT NULL COMMENT 'วันที่',
  `tc_time` time NOT NULL COMMENT 'เวลาที่ตรวจสอบ',
  `tc_status` tinyint(1) NOT NULL COMMENT 'สถานะการตรวจสอบ',
  `rep_id` varchar(11) NOT NULL COMMENT 'รหัสใบซ่อม'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `table_check`
--

INSERT INTO `table_check` (`tc_id`, `tc_date`, `tc_time`, `tc_status`, `rep_id`) VALUES
('tci0001', '2017-12-20', '08:00:00', 1, 're0001');

-- --------------------------------------------------------

--
-- Table structure for table `technician`
--

CREATE TABLE `technician` (
  `tn_id` varchar(11) NOT NULL COMMENT 'รหัสช่าง',
  `tn_name` varchar(25) NOT NULL COMMENT 'ชื่อช่าง',
  `tn_position` varchar(25) NOT NULL COMMENT 'ตำแหน่งช่าง'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `technician`
--

INSERT INTO `technician` (`tn_id`, `tn_name`, `tn_position`) VALUES
('tn0001', 'ธนกร', 'ผสมสี'),
('tn0002', 'ธงไท', 'ประกอบ'),
('tn0003', 'ชลวัช', 'ถอด'),
('tn0004', 'วัทฑิกร', 'ขัดสี'),
('tn0005', 'กิตติพงษ์', 'ถอด'),
('tn0006', 'สุรพล', 'ประกอบ');

-- --------------------------------------------------------

--
-- Table structure for table `time_hours`
--

CREATE TABLE `time_hours` (
  `th_id` int(11) NOT NULL COMMENT 'รหัสชั่วโมงการทำงาน',
  `th_start` date NOT NULL COMMENT 'วันที่เริ่มการทำงาน',
  `th_finish` date NOT NULL COMMENT 'วันที่เสร็จ',
  `th_department` varchar(25) NOT NULL COMMENT 'เก็บแผนกการทำงานของช่าง',
  `tn_id` varchar(11) NOT NULL COMMENT 'รหัสช่าง',
  `rl_id2` varchar(11) NOT NULL COMMENT 'รหัสรายการซ่อม2'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `time_hours`
--

INSERT INTO `time_hours` (`th_id`, `th_start`, `th_finish`, `th_department`, `tn_id`, `rl_id2`) VALUES
(1, '2018-05-21', '2018-05-28', 'ประกอบ', 'tn0001', 'r2001'),
(2, '2018-05-21', '0000-00-00', 'ขัดสี', 'tn0005', 'r2004'),
(3, '2018-05-28', '0000-00-00', 'ขัดสี', 'tn0001', 'r2001'),
(4, '2018-05-28', '0000-00-00', 'ถอดอะไหล่', 'tn0003', 'r2007');

-- --------------------------------------------------------

--
-- Table structure for table `time_start`
--

CREATE TABLE `time_start` (
  `ts_id` int(11) NOT NULL COMMENT 'รหัสเวลาเริ่มหยุดการทำงาน',
  `ts_date_start` date NOT NULL COMMENT 'วันที่เริ่มทำ',
  `ts_time_start` time NOT NULL COMMENT 'เวลาที่เริ่มทำ',
  `ts_time_stop` time NOT NULL COMMENT 'เวลาที่หยุดทำ',
  `th_id` int(11) NOT NULL COMMENT 'รหัสชั่วโมงการทำงาน'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `time_start`
--

INSERT INTO `time_start` (`ts_id`, `ts_date_start`, `ts_time_start`, `ts_time_stop`, `th_id`) VALUES
(130, '2018-05-21', '14:08:35', '13:50:30', 1),
(131, '2018-05-21', '14:08:55', '00:00:00', 2),
(132, '2018-05-28', '13:50:35', '00:00:00', 3),
(133, '2018-05-28', '13:50:52', '00:00:00', 4);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `add_consumable`
--
ALTER TABLE `add_consumable`
  ADD PRIMARY KEY (`atc_id`),
  ADD KEY `bcons_id` (`bcons_id`);

--
-- Indexes for table `add_stock_color`
--
ALTER TABLE `add_stock_color`
  ADD PRIMARY KEY (`addcolor_id`),
  ADD KEY `color_id` (`color_id`);

--
-- Indexes for table `add_to_spare`
--
ALTER TABLE `add_to_spare`
  ADD PRIMARY KEY (`ATS_id`),
  ADD KEY `bol_id` (`bol_id`);

--
-- Indexes for table `bill_claim`
--
ALTER TABLE `bill_claim`
  ADD PRIMARY KEY (`bc_id`),
  ADD KEY `ir_id` (`ir_id`),
  ADD KEY `op_id` (`op_id`);

--
-- Indexes for table `bill_color`
--
ALTER TABLE `bill_color`
  ADD PRIMARY KEY (`color_id`),
  ADD KEY `tn_id` (`tn_id`),
  ADD KEY `rep_id` (`rep_id`),
  ADD KEY `oc_id` (`oc_id`);

--
-- Indexes for table `bill_color_list`
--
ALTER TABLE `bill_color_list`
  ADD PRIMARY KEY (`bcl_id`),
  ADD KEY `formc_id` (`formc_id`),
  ADD KEY `scolor_id` (`scolor_id`);

--
-- Indexes for table `bill_comnsumable`
--
ALTER TABLE `bill_comnsumable`
  ADD PRIMARY KEY (`bcons_id`),
  ADD KEY `tn_id` (`tn_id`),
  ADD KEY `rep_id` (`rep_id`),
  ADD KEY `om_id` (`om_id`);

--
-- Indexes for table `bill_of_landi`
--
ALTER TABLE `bill_of_landi`
  ADD PRIMARY KEY (`bol_id`),
  ADD KEY `rep_id` (`rep_id`),
  ADD KEY `tn_id` (`tn_id`),
  ADD KEY `os_id` (`os_id`);

--
-- Indexes for table `bill_repair_list`
--
ALTER TABLE `bill_repair_list`
  ADD PRIMARY KEY (`brl_id`),
  ADD KEY `op_id` (`op_id`);

--
-- Indexes for table `car`
--
ALTER TABLE `car`
  ADD PRIMARY KEY (`car_id`),
  ADD KEY `cus_id` (`cus_id`);

--
-- Indexes for table `car_receipt`
--
ALTER TABLE `car_receipt`
  ADD PRIMARY KEY (`cr_id`);

--
-- Indexes for table `comnsumable`
--
ALTER TABLE `comnsumable`
  ADD PRIMARY KEY (`cons_id`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`com_id`);

--
-- Indexes for table `consumable_amount`
--
ALTER TABLE `consumable_amount`
  ADD PRIMARY KEY (`cona_id`),
  ADD KEY `cons_id` (`cons_id`),
  ADD KEY `om_id` (`om_id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`cus_id`);

--
-- Indexes for table `customer2`
--
ALTER TABLE `customer2`
  ADD PRIMARY KEY (`CustomerID`);

--
-- Indexes for table `damage`
--
ALTER TABLE `damage`
  ADD PRIMARY KEY (`dm_id`),
  ADD KEY `op_id` (`op_id`);

--
-- Indexes for table `date`
--
ALTER TABLE `date`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `document_check`
--
ALTER TABLE `document_check`
  ADD PRIMARY KEY (`dc_id`),
  ADD KEY `car_id` (`car_id`),
  ADD KEY `rl_id` (`rl_id`);

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`emp_id`);

--
-- Indexes for table `fixed`
--
ALTER TABLE `fixed`
  ADD PRIMARY KEY (`fix_id`),
  ADD KEY `car_id` (`car_id`),
  ADD KEY `cus_id` (`cus_id`),
  ADD KEY `rep_id` (`rep_id`);

--
-- Indexes for table `formulas_color`
--
ALTER TABLE `formulas_color`
  ADD PRIMARY KEY (`formc_id`);

--
-- Indexes for table `image`
--
ALTER TABLE `image`
  ADD PRIMARY KEY (`img_id`),
  ADD KEY `rl_id` (`rl_id`),
  ADD KEY `rep_id` (`rep_id`);

--
-- Indexes for table `insurance`
--
ALTER TABLE `insurance`
  ADD PRIMARY KEY (`ir_id`);

--
-- Indexes for table `ist_bill_con`
--
ALTER TABLE `ist_bill_con`
  ADD PRIMARY KEY (`bcn_id`);

--
-- Indexes for table `list_bill_color`
--
ALTER TABLE `list_bill_color`
  ADD PRIMARY KEY (`col_id`);

--
-- Indexes for table `list_bill_spare`
--
ALTER TABLE `list_bill_spare`
  ADD PRIMARY KEY (`lbs_id`),
  ADD KEY `bol_id` (`bol_id`),
  ADD KEY `spare_id` (`spare_id`);

--
-- Indexes for table `list_color`
--
ALTER TABLE `list_color`
  ADD PRIMARY KEY (`list_color_id`),
  ADD KEY `scolor_id` (`scolor_id`),
  ADD KEY `addcolor_id` (`addcolor_id`),
  ADD KEY `com_id` (`com_id`);

--
-- Indexes for table `list_consumable`
--
ALTER TABLE `list_consumable`
  ADD PRIMARY KEY (`list_cons_id`),
  ADD KEY `atc_id` (`atc_id`),
  ADD KEY `cons_id` (`cons_id`);

--
-- Indexes for table `list_spare`
--
ALTER TABLE `list_spare`
  ADD PRIMARY KEY (`list_spare_id`),
  ADD KEY `spare_id` (`spare_id`),
  ADD KEY `ATS_id` (`ATS_id`),
  ADD KEY `com_id` (`com_id`);

--
-- Indexes for table `offer_price`
--
ALTER TABLE `offer_price`
  ADD PRIMARY KEY (`op_id`),
  ADD KEY `rep_id` (`rep_id`);

--
-- Indexes for table `order_color`
--
ALTER TABLE `order_color`
  ADD PRIMARY KEY (`oc_id`),
  ADD KEY `emp_id` (`emp_id`),
  ADD KEY `com_id` (`com_id`),
  ADD KEY `rep_id` (`rep_id`);

--
-- Indexes for table `order_color_amount`
--
ALTER TABLE `order_color_amount`
  ADD PRIMARY KEY (`oca_id`),
  ADD KEY `scolor_id` (`scolor_id`),
  ADD KEY `oc_id` (`oc_id`);

--
-- Indexes for table `order_consumable`
--
ALTER TABLE `order_consumable`
  ADD PRIMARY KEY (`om_id`),
  ADD KEY `com_id` (`com_id`),
  ADD KEY `emp_id` (`emp_id`),
  ADD KEY `rep_id` (`rep_id`);

--
-- Indexes for table `order_spare`
--
ALTER TABLE `order_spare`
  ADD PRIMARY KEY (`os_id`),
  ADD KEY `emp_id` (`emp_id`),
  ADD KEY `com_id` (`com_id`),
  ADD KEY `rep_id` (`rep_id`);

--
-- Indexes for table `order_spare_amount`
--
ALTER TABLE `order_spare_amount`
  ADD PRIMARY KEY (`osa_id`),
  ADD KEY `spare_id` (`spare_id`),
  ADD KEY `os_id` (`os_id`);

--
-- Indexes for table `rcon_list`
--
ALTER TABLE `rcon_list`
  ADD PRIMARY KEY (`rcon_id`),
  ADD KEY `rcb_id` (`rcb_id`),
  ADD KEY `emp_id` (`emp_id`),
  ADD KEY `tn_id` (`tn_id`),
  ADD KEY `cons_id` (`cons_id`);

--
-- Indexes for table `repair`
--
ALTER TABLE `repair`
  ADD PRIMARY KEY (`rep_id`),
  ADD KEY `cr_id` (`cr_id`),
  ADD KEY `sc_id` (`sc_id`),
  ADD KEY `emp_id` (`emp_id`);

--
-- Indexes for table `repair_list`
--
ALTER TABLE `repair_list`
  ADD PRIMARY KEY (`rl_id`),
  ADD KEY `brl_id` (`brl_id`);

--
-- Indexes for table `repair_list2`
--
ALTER TABLE `repair_list2`
  ADD PRIMARY KEY (`rl_id2`),
  ADD KEY `rl_id` (`rl_id`),
  ADD KEY `tn_id` (`tn_id`);

--
-- Indexes for table `return_color`
--
ALTER TABLE `return_color`
  ADD PRIMARY KEY (`rc_id`),
  ADD KEY `color_id` (`color_id`);

--
-- Indexes for table `return_consumable`
--
ALTER TABLE `return_consumable`
  ADD PRIMARY KEY (`rcb_id`),
  ADD KEY `bcons_id` (`bcons_id`);

--
-- Indexes for table `return_spare`
--
ALTER TABLE `return_spare`
  ADD PRIMARY KEY (`rs_id`),
  ADD KEY `bol_id` (`bol_id`);

--
-- Indexes for table `rtc_list`
--
ALTER TABLE `rtc_list`
  ADD PRIMARY KEY (`rtc_id`),
  ADD KEY `rc_id` (`rc_id`),
  ADD KEY `emp_id` (`emp_id`),
  ADD KEY `tn_id` (`tn_id`),
  ADD KEY `scolor_id` (`scolor_id`);

--
-- Indexes for table `rts_list`
--
ALTER TABLE `rts_list`
  ADD PRIMARY KEY (`rts_id`),
  ADD KEY `emp_id` (`emp_id`),
  ADD KEY `rs_id` (`rs_id`),
  ADD KEY `tn_id` (`tn_id`),
  ADD KEY `spare_id` (`spare_id`);

--
-- Indexes for table `sent_car`
--
ALTER TABLE `sent_car`
  ADD PRIMARY KEY (`sc_id`);

--
-- Indexes for table `spare`
--
ALTER TABLE `spare`
  ADD PRIMARY KEY (`spare_id`);

--
-- Indexes for table `stock_color`
--
ALTER TABLE `stock_color`
  ADD PRIMARY KEY (`scolor_id`);

--
-- Indexes for table `table_check`
--
ALTER TABLE `table_check`
  ADD PRIMARY KEY (`tc_id`),
  ADD KEY `rep_id` (`rep_id`);

--
-- Indexes for table `technician`
--
ALTER TABLE `technician`
  ADD PRIMARY KEY (`tn_id`);

--
-- Indexes for table `time_hours`
--
ALTER TABLE `time_hours`
  ADD PRIMARY KEY (`th_id`),
  ADD KEY `tn_id` (`tn_id`),
  ADD KEY `rl_id2` (`rl_id2`);

--
-- Indexes for table `time_start`
--
ALTER TABLE `time_start`
  ADD PRIMARY KEY (`ts_id`),
  ADD KEY `th_id` (`th_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `consumable_amount`
--
ALTER TABLE `consumable_amount`
  MODIFY `cona_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'รหัสจำนวนที่สั่งซื้อ', AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `date`
--
ALTER TABLE `date`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=290;

--
-- AUTO_INCREMENT for table `ist_bill_con`
--
ALTER TABLE `ist_bill_con`
  MODIFY `bcn_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `list_bill_color`
--
ALTER TABLE `list_bill_color`
  MODIFY `col_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `list_bill_spare`
--
ALTER TABLE `list_bill_spare`
  MODIFY `lbs_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `list_color`
--
ALTER TABLE `list_color`
  MODIFY `list_color_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'รหัสรายการเพิ่มสีลง', AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `list_consumable`
--
ALTER TABLE `list_consumable`
  MODIFY `list_cons_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'รหัสรายการเพิ่มวัสดุลงสต๊อค', AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `list_spare`
--
ALTER TABLE `list_spare`
  MODIFY `list_spare_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'รหัสรายการเพิ่มอะไหล่ลง', AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `order_color_amount`
--
ALTER TABLE `order_color_amount`
  MODIFY `oca_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'รหัสจำนวนที่สั่งซื้อสี', AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `order_spare_amount`
--
ALTER TABLE `order_spare_amount`
  MODIFY `osa_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'รหัสจำนวนที่สั่งซื้ออะไหล่', AUTO_INCREMENT=292;

--
-- AUTO_INCREMENT for table `rcon_list`
--
ALTER TABLE `rcon_list`
  MODIFY `rcon_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `rtc_list`
--
ALTER TABLE `rtc_list`
  MODIFY `rtc_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `rts_list`
--
ALTER TABLE `rts_list`
  MODIFY `rts_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `time_hours`
--
ALTER TABLE `time_hours`
  MODIFY `th_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'รหัสชั่วโมงการทำงาน', AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `time_start`
--
ALTER TABLE `time_start`
  MODIFY `ts_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'รหัสเวลาเริ่มหยุดการทำงาน', AUTO_INCREMENT=134;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `add_consumable`
--
ALTER TABLE `add_consumable`
  ADD CONSTRAINT `add_consumable_ibfk_1` FOREIGN KEY (`bcons_id`) REFERENCES `bill_comnsumable` (`bcons_id`);

--
-- Constraints for table `add_stock_color`
--
ALTER TABLE `add_stock_color`
  ADD CONSTRAINT `add_stock_color_ibfk_1` FOREIGN KEY (`color_id`) REFERENCES `bill_color` (`color_id`);

--
-- Constraints for table `add_to_spare`
--
ALTER TABLE `add_to_spare`
  ADD CONSTRAINT `add_to_spare_ibfk_1` FOREIGN KEY (`bol_id`) REFERENCES `bill_of_landi` (`bol_id`);

--
-- Constraints for table `bill_claim`
--
ALTER TABLE `bill_claim`
  ADD CONSTRAINT `bill_claim_ibfk_1` FOREIGN KEY (`ir_id`) REFERENCES `insurance` (`ir_id`),
  ADD CONSTRAINT `bill_claim_ibfk_2` FOREIGN KEY (`op_id`) REFERENCES `offer_price` (`op_id`);

--
-- Constraints for table `bill_color`
--
ALTER TABLE `bill_color`
  ADD CONSTRAINT `bill_color_ibfk_4` FOREIGN KEY (`tn_id`) REFERENCES `technician` (`tn_id`),
  ADD CONSTRAINT `bill_color_ibfk_7` FOREIGN KEY (`rep_id`) REFERENCES `repair` (`rep_id`),
  ADD CONSTRAINT `bill_color_ibfk_8` FOREIGN KEY (`oc_id`) REFERENCES `order_color` (`oc_id`);

--
-- Constraints for table `bill_color_list`
--
ALTER TABLE `bill_color_list`
  ADD CONSTRAINT `bill_color_list_ibfk_1` FOREIGN KEY (`formc_id`) REFERENCES `formulas_color` (`formc_id`),
  ADD CONSTRAINT `bill_color_list_ibfk_2` FOREIGN KEY (`scolor_id`) REFERENCES `stock_color` (`scolor_id`);

--
-- Constraints for table `bill_comnsumable`
--
ALTER TABLE `bill_comnsumable`
  ADD CONSTRAINT `bill_comnsumable_ibfk_1` FOREIGN KEY (`rep_id`) REFERENCES `repair` (`rep_id`),
  ADD CONSTRAINT `bill_comnsumable_ibfk_2` FOREIGN KEY (`tn_id`) REFERENCES `technician` (`tn_id`),
  ADD CONSTRAINT `bill_comnsumable_ibfk_3` FOREIGN KEY (`om_id`) REFERENCES `order_consumable` (`om_id`);

--
-- Constraints for table `bill_of_landi`
--
ALTER TABLE `bill_of_landi`
  ADD CONSTRAINT `bill_of_landi_ibfk_1` FOREIGN KEY (`rep_id`) REFERENCES `repair` (`rep_id`),
  ADD CONSTRAINT `bill_of_landi_ibfk_2` FOREIGN KEY (`tn_id`) REFERENCES `technician` (`tn_id`);

--
-- Constraints for table `bill_repair_list`
--
ALTER TABLE `bill_repair_list`
  ADD CONSTRAINT `bill_repair_list_ibfk_1` FOREIGN KEY (`op_id`) REFERENCES `offer_price` (`op_id`);

--
-- Constraints for table `car`
--
ALTER TABLE `car`
  ADD CONSTRAINT `car_ibfk_1` FOREIGN KEY (`cus_id`) REFERENCES `customer` (`cus_id`);

--
-- Constraints for table `consumable_amount`
--
ALTER TABLE `consumable_amount`
  ADD CONSTRAINT `consumable_amount_ibfk_1` FOREIGN KEY (`cons_id`) REFERENCES `comnsumable` (`cons_id`),
  ADD CONSTRAINT `consumable_amount_ibfk_2` FOREIGN KEY (`om_id`) REFERENCES `order_consumable` (`om_id`);

--
-- Constraints for table `damage`
--
ALTER TABLE `damage`
  ADD CONSTRAINT `damage_ibfk_1` FOREIGN KEY (`op_id`) REFERENCES `offer_price` (`op_id`);

--
-- Constraints for table `document_check`
--
ALTER TABLE `document_check`
  ADD CONSTRAINT `document_check_ibfk_1` FOREIGN KEY (`car_id`) REFERENCES `car` (`car_id`),
  ADD CONSTRAINT `document_check_ibfk_2` FOREIGN KEY (`rl_id`) REFERENCES `repair_list` (`rl_id`);

--
-- Constraints for table `fixed`
--
ALTER TABLE `fixed`
  ADD CONSTRAINT `fixed_ibfk_1` FOREIGN KEY (`car_id`) REFERENCES `car` (`car_id`),
  ADD CONSTRAINT `fixed_ibfk_2` FOREIGN KEY (`cus_id`) REFERENCES `customer` (`cus_id`),
  ADD CONSTRAINT `fixed_ibfk_3` FOREIGN KEY (`rep_id`) REFERENCES `repair` (`rep_id`);

--
-- Constraints for table `image`
--
ALTER TABLE `image`
  ADD CONSTRAINT `image_ibfk_1` FOREIGN KEY (`rl_id`) REFERENCES `repair_list` (`rl_id`),
  ADD CONSTRAINT `image_ibfk_2` FOREIGN KEY (`rep_id`) REFERENCES `repair` (`rep_id`);

--
-- Constraints for table `list_bill_spare`
--
ALTER TABLE `list_bill_spare`
  ADD CONSTRAINT `list_bill_spare_ibfk_1` FOREIGN KEY (`bol_id`) REFERENCES `bill_of_landi` (`bol_id`),
  ADD CONSTRAINT `list_bill_spare_ibfk_2` FOREIGN KEY (`spare_id`) REFERENCES `spare` (`spare_id`);

--
-- Constraints for table `list_color`
--
ALTER TABLE `list_color`
  ADD CONSTRAINT `list_color_ibfk_3` FOREIGN KEY (`scolor_id`) REFERENCES `stock_color` (`scolor_id`),
  ADD CONSTRAINT `list_color_ibfk_4` FOREIGN KEY (`addcolor_id`) REFERENCES `add_stock_color` (`addcolor_id`),
  ADD CONSTRAINT `list_color_ibfk_5` FOREIGN KEY (`com_id`) REFERENCES `company` (`com_id`);

--
-- Constraints for table `list_consumable`
--
ALTER TABLE `list_consumable`
  ADD CONSTRAINT `list_consumable_ibfk_1` FOREIGN KEY (`atc_id`) REFERENCES `add_consumable` (`atc_id`),
  ADD CONSTRAINT `list_consumable_ibfk_2` FOREIGN KEY (`cons_id`) REFERENCES `comnsumable` (`cons_id`);

--
-- Constraints for table `list_spare`
--
ALTER TABLE `list_spare`
  ADD CONSTRAINT `list_spare_ibfk_1` FOREIGN KEY (`ATS_id`) REFERENCES `add_to_spare` (`ATS_id`),
  ADD CONSTRAINT `list_spare_ibfk_2` FOREIGN KEY (`spare_id`) REFERENCES `spare` (`spare_id`),
  ADD CONSTRAINT `list_spare_ibfk_3` FOREIGN KEY (`com_id`) REFERENCES `company` (`com_id`);

--
-- Constraints for table `offer_price`
--
ALTER TABLE `offer_price`
  ADD CONSTRAINT `offer_price_ibfk_1` FOREIGN KEY (`rep_id`) REFERENCES `repair` (`rep_id`);

--
-- Constraints for table `order_color`
--
ALTER TABLE `order_color`
  ADD CONSTRAINT `order_color_ibfk_1` FOREIGN KEY (`com_id`) REFERENCES `company` (`com_id`),
  ADD CONSTRAINT `order_color_ibfk_3` FOREIGN KEY (`emp_id`) REFERENCES `employee` (`emp_id`),
  ADD CONSTRAINT `order_color_ibfk_4` FOREIGN KEY (`rep_id`) REFERENCES `repair` (`rep_id`);

--
-- Constraints for table `order_color_amount`
--
ALTER TABLE `order_color_amount`
  ADD CONSTRAINT `order_color_amount_ibfk_1` FOREIGN KEY (`scolor_id`) REFERENCES `stock_color` (`scolor_id`),
  ADD CONSTRAINT `order_color_amount_ibfk_2` FOREIGN KEY (`oc_id`) REFERENCES `order_color` (`oc_id`);

--
-- Constraints for table `order_consumable`
--
ALTER TABLE `order_consumable`
  ADD CONSTRAINT `order_consumable_ibfk_1` FOREIGN KEY (`com_id`) REFERENCES `company` (`com_id`),
  ADD CONSTRAINT `order_consumable_ibfk_3` FOREIGN KEY (`emp_id`) REFERENCES `employee` (`emp_id`),
  ADD CONSTRAINT `order_consumable_ibfk_4` FOREIGN KEY (`rep_id`) REFERENCES `repair` (`rep_id`);

--
-- Constraints for table `order_spare`
--
ALTER TABLE `order_spare`
  ADD CONSTRAINT `order_spare_ibfk_1` FOREIGN KEY (`emp_id`) REFERENCES `employee` (`emp_id`),
  ADD CONSTRAINT `order_spare_ibfk_4` FOREIGN KEY (`com_id`) REFERENCES `company` (`com_id`),
  ADD CONSTRAINT `order_spare_ibfk_5` FOREIGN KEY (`rep_id`) REFERENCES `repair` (`rep_id`);

--
-- Constraints for table `order_spare_amount`
--
ALTER TABLE `order_spare_amount`
  ADD CONSTRAINT `order_spare_amount_ibfk_1` FOREIGN KEY (`spare_id`) REFERENCES `spare` (`spare_id`),
  ADD CONSTRAINT `order_spare_amount_ibfk_2` FOREIGN KEY (`os_id`) REFERENCES `order_spare` (`os_id`);

--
-- Constraints for table `rcon_list`
--
ALTER TABLE `rcon_list`
  ADD CONSTRAINT `rcon_list_ibfk_1` FOREIGN KEY (`rcb_id`) REFERENCES `return_consumable` (`rcb_id`),
  ADD CONSTRAINT `rcon_list_ibfk_2` FOREIGN KEY (`emp_id`) REFERENCES `employee` (`emp_id`),
  ADD CONSTRAINT `rcon_list_ibfk_3` FOREIGN KEY (`tn_id`) REFERENCES `technician` (`tn_id`),
  ADD CONSTRAINT `rcon_list_ibfk_4` FOREIGN KEY (`cons_id`) REFERENCES `comnsumable` (`cons_id`);

--
-- Constraints for table `repair`
--
ALTER TABLE `repair`
  ADD CONSTRAINT `repair_ibfk_1` FOREIGN KEY (`cr_id`) REFERENCES `car_receipt` (`cr_id`),
  ADD CONSTRAINT `repair_ibfk_2` FOREIGN KEY (`sc_id`) REFERENCES `sent_car` (`sc_id`),
  ADD CONSTRAINT `repair_ibfk_3` FOREIGN KEY (`emp_id`) REFERENCES `employee` (`emp_id`);

--
-- Constraints for table `repair_list`
--
ALTER TABLE `repair_list`
  ADD CONSTRAINT `repair_list_ibfk_1` FOREIGN KEY (`brl_id`) REFERENCES `bill_repair_list` (`brl_id`);

--
-- Constraints for table `repair_list2`
--
ALTER TABLE `repair_list2`
  ADD CONSTRAINT `repair_list2_ibfk_1` FOREIGN KEY (`rl_id`) REFERENCES `repair_list` (`rl_id`),
  ADD CONSTRAINT `repair_list2_ibfk_2` FOREIGN KEY (`tn_id`) REFERENCES `technician` (`tn_id`);

--
-- Constraints for table `return_color`
--
ALTER TABLE `return_color`
  ADD CONSTRAINT `return_color_ibfk_1` FOREIGN KEY (`color_id`) REFERENCES `bill_color` (`color_id`);

--
-- Constraints for table `return_consumable`
--
ALTER TABLE `return_consumable`
  ADD CONSTRAINT `return_consumable_ibfk_1` FOREIGN KEY (`bcons_id`) REFERENCES `bill_comnsumable` (`bcons_id`);

--
-- Constraints for table `return_spare`
--
ALTER TABLE `return_spare`
  ADD CONSTRAINT `return_spare_ibfk_1` FOREIGN KEY (`bol_id`) REFERENCES `bill_of_landi` (`bol_id`);

--
-- Constraints for table `rtc_list`
--
ALTER TABLE `rtc_list`
  ADD CONSTRAINT `rtc_list_ibfk_1` FOREIGN KEY (`rc_id`) REFERENCES `return_color` (`rc_id`),
  ADD CONSTRAINT `rtc_list_ibfk_2` FOREIGN KEY (`emp_id`) REFERENCES `employee` (`emp_id`),
  ADD CONSTRAINT `rtc_list_ibfk_3` FOREIGN KEY (`tn_id`) REFERENCES `technician` (`tn_id`),
  ADD CONSTRAINT `rtc_list_ibfk_4` FOREIGN KEY (`scolor_id`) REFERENCES `stock_color` (`scolor_id`);

--
-- Constraints for table `rts_list`
--
ALTER TABLE `rts_list`
  ADD CONSTRAINT `rts_list_ibfk_1` FOREIGN KEY (`emp_id`) REFERENCES `employee` (`emp_id`),
  ADD CONSTRAINT `rts_list_ibfk_2` FOREIGN KEY (`rs_id`) REFERENCES `return_spare` (`rs_id`),
  ADD CONSTRAINT `rts_list_ibfk_3` FOREIGN KEY (`tn_id`) REFERENCES `technician` (`tn_id`),
  ADD CONSTRAINT `rts_list_ibfk_4` FOREIGN KEY (`spare_id`) REFERENCES `spare` (`spare_id`);

--
-- Constraints for table `table_check`
--
ALTER TABLE `table_check`
  ADD CONSTRAINT `table_check_ibfk_1` FOREIGN KEY (`rep_id`) REFERENCES `repair` (`rep_id`);

--
-- Constraints for table `time_hours`
--
ALTER TABLE `time_hours`
  ADD CONSTRAINT `time_hours_ibfk_1` FOREIGN KEY (`tn_id`) REFERENCES `technician` (`tn_id`),
  ADD CONSTRAINT `time_hours_ibfk_2` FOREIGN KEY (`rl_id2`) REFERENCES `repair_list2` (`rl_id2`);

--
-- Constraints for table `time_start`
--
ALTER TABLE `time_start`
  ADD CONSTRAINT `time_start_ibfk_1` FOREIGN KEY (`th_id`) REFERENCES `time_hours` (`th_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
