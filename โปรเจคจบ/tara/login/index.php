<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<title>Daily UI - Day 1 Sign In</title>

	<!-- Google Fonts -->
	<link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700|Lato:400,100,300,700,900' rel='stylesheet' type='text/css'>

	<link rel="stylesheet" href="cssFame/animate.css">
	<!-- Custom Stylesheet -->
	<link rel="stylesheet" href="cssFame/style2.css">

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
</head>

<body>
	<div class="container">
		<div class="top">
			<h1 id="title" class="hidden"><span id="logo">Project <span>Tara</span></span></h1>
		</div>
		<div class="login-box animated fadeInUp">
			<div class="box-header">
				<h2>Log In</h2>
			</div>
			<form action="login.php" method="post">
			<label for="username">Username</label>
			<br/>
			<input type="text" id="id" name="id">
			<br/>
			<label for="password">Password</label>
			<br/>
			<input type="password" id="pass" name="pass">
			<br/>
			<button type="submit">Sign In</button>
			<br/>
			</form>
			<!--##############################################################################################################-->
			<?php include 'conn.php'; ?>
			<?php 
  				$sql ="SELECT * FROM `user`";
  				$query = mysqli_query($conn,$sql);
  				echo "<br>";
  				echo "<table width='340' border='1'>";
  				echo "<tr><td>ID</td><td>pass</td><td>name</td><td>Admin</td></tr>";
  				while ($result = mysqli_fetch_array($query,MYSQLI_ASSOC)) {
    				echo "<tr>";
    				echo "<td width='100'>";
    				echo $result["ID_User"];
    				echo "</td>";
    				echo "<td width='100'>";
    				echo $result["US_pass"];
    				echo "</td>";
    				echo "<td width='100'>";
    				echo $result["US_name"];
    				echo "</td>";
    				echo "<td width='100'>";
    				echo $result["US_Status"];
    				echo "</td>";
    				echo "</tr>";
  				}
  				echo "</table>";
			?>
			<a href="#"><p class="small">Forgot your password?</p></a>
		</div>
	</div>
</body>

<script>
	$(document).ready(function () {
    	$('#logo').addClass('animated fadeInDown');
    	$("input:text:visible:first").focus();
	});
	$('#username').focus(function() {
		$('label[for="username"]').addClass('selected');
	});
	$('#username').blur(function() {
		$('label[for="username"]').removeClass('selected');
	});
	$('#password').focus(function() {
		$('label[for="password"]').addClass('selected');
	});
	$('#password').blur(function() {
		$('label[for="password"]').removeClass('selected');
	});
</script>

</html>