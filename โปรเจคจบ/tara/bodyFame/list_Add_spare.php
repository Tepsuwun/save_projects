<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<?php 
include"conn.php";
$i = 1;
$num = "";
$sql = "SELECT * FROM list_spare,spare WHERE list_spare.spare_id = spare.spare_id order by ATS_id ASC";
$query = mysqli_query($conn,$sql);
echo "<form action=\"check_conf.php?page=list_Add_spare\" method=\"post\">";
echo "<table border=\"0\" class=\"table table-striped\" width=\"100%\"> ";
echo "<thead><tr class=\"info\">";
echo "<th> <p align = 'center'> รหัสรายการเพิ่มอะไหล่ลงสต๊อค	</p></th>";
echo "<th> <p align = 'center'> ชื่อรายการอะไหล่	</p></th>";
echo "<th> <p align = 'center'> ชนิดวอะไหล่		</p></th>";
echo "<th> <p align = 'center'> ราคาอะไหล่					</p></th>";
echo "<th> <p align = 'center'> จำนวนอะไหล่ที่เพิ่มลงสต๊อค		</p></th>";
echo "<th> <p align = 'center'> จำนวนอะไหล่ในสต๊อค			</p></th>";
echo "<th> <p align = 'center'> สถานะการอนัติ/ไม่อนุมัติ	</p></th>";
echo "</tr></thead><tbody>";
while ($result = mysqli_fetch_array($query,MYSQLI_ASSOC)) {
	if ($i==1) {	
		echo "<tr class=\"active\">";
		$i = 0;
	}else{
		echo "<tr class=\"success\">";
		$i = 1;
	}
	$ID = $result["ATS_id"];
	if ($ID == $num) {
		echo "<th><p align = 'center'> ,, </p></th>";
	}else{
		echo "<th><p align = 'center'>".$ID."</p></th>";
		$num = $ID;
	}
	echo "<td><p align = 'center'>".$result["spare_name"].			"</p></td>";
	echo "<td><p align = 'center'>".$result["spare_type"].			"</p></td>";
	echo "<td><p align = 'center'>".$result["list_spare_price"].	"</p></td>";
	echo "<td><p align = 'center'>".$result["list_spare_amount"].	"</p></td>";
	echo "<td><p align = 'center'>".$result["spare_amount"].		"</p></td>";
	$stat=$result["list_spare_stat"];
	$notShow=$result["notShow"];
	$list_id = $result["list_spare_id"];
	if ($notShow == 1) {
		if ($stat==1) {
			echo "<td><p align = 'center'>อนุมัติ</p></td>";
		}else{
			echo "<td><p align = 'center'>ไม่อนุมัติ</p></td>";
		}
	}else{
		echo "<td><input type=\"radio\" name=\"statOK$list_id\" value=\"no\">ไม่อนุมัติ$list_id<br>
  			<input type=\"radio\" name=\"statOK$list_id\" value=\"yes\">อนุมัติ$list_id</td>";
	}
	echo "</tr>";
}
echo "</tbody></table>";
echo "<p align = 'right'><input type=\"submit\" value=\"Submit\"></p></form>";
?>