﻿
<?php
session_start();
if (isset($_GET["command"])==true) {
  $command = $_GET["command"];
  switch ($command) {
    case 'clear':
      session_destroy();
      die("ล้างค่าเรียบร้อย");
      break;
    case 'show':
      if (isset($_SESSION['code_Color'])==false) {
        die("เลือกสีสิครับ");
      }else{
        $ses_codeColor = $_SESSION['code_Color'];
        show_sum_color($ses_codeColor);
      }
  echo "</table>";
      break;
    default:
      die("ไม่มีคำสั่ง $command");
      break;
  }
}else{
  $X=$_GET["x"];
  $Y=$_GET["y"];
  $Z=$_GET["z"];
  $lvl=$_GET["lvl"];
  echo "code_Color = $X, $Y, $Z, $lvl ";
  $code_Color = code_Color($X, $Y, $Z, $lvl);
  echo "[$code_Color]";
  if (isset($_SESSION['code_Color'])==false) {
    $_SESSION['code_Color'] = array();
  }
  array_push($_SESSION['code_Color'], $code_Color);
  $ses_codeColor = $_SESSION['code_Color'];
  show_sum_color($ses_codeColor);
  /*$ses_codeColor = $_SESSION['code_Color'];
  echo "<table>";
  foreach ($ses_codeColor as $key => $value) {
    echo "<tr><th> $key </th><th> $value </th><th width=\"25\" bgcolor=\"$value\">     </th></tr>";
  }
  echo "</table>";*/
}
function show_sum_color($ses_codeColor){
        $count_ses = count($ses_codeColor);
        //echo $count_ses;
        $sum_r = 0;
        $sum_g = 0;
        $sum_b = 0;
        for ($i=0; $i < $count_ses; $i++) { 
        $r = substr($ses_codeColor[$i], 1 ,2); 
        $g = substr($ses_codeColor[$i], 3 ,2); 
        $b = substr($ses_codeColor[$i], 5 ,2); 
        //echo "<br>1. $r $g $b";
        $r = hexdec($r); 
        $g = hexdec($g); 
        $b = hexdec($b); 
        //echo "<br>2. $r $g $b<br>";
        $per_c = (100/$count_ses)/100;
        //echo '3. '.$per_c."<br>";
        /*$r = intval( $r );
        $g = intval( $g );
        $b = intval( $b );*/
        $r = $r*$per_c;
        $g = $g*$per_c;
        $b = $b*$per_c;
        /*$r = round($r, 2);
        $g = round($g, 2);
        $b = round($b, 2);*/
        //echo "4. $r $g $b<br>";
        $sum_r += $r;
        $sum_g += $g;
        $sum_b += $b;

        //echo "5. $sum_r $sum_g $sum_b<br>";
        }
        //echo "<br>###############################################<br>";
        $sum_r = round($sum_r, 0);
        $sum_g = round($sum_g, 0);
        $sum_b = round($sum_b, 0);
        //echo "$sum_r $sum_g $sum_b<br>";
        $sum_r = dechex($sum_r); 
        $sum_g = dechex($sum_g); 
        $sum_b = dechex($sum_b);
        if(strlen($sum_r) < 2){
          $sum_r = "0$sum_r";
        }
        if(strlen($sum_g) < 2){
          $sum_g = "0$sum_g";
        }
        if(strlen($sum_b) < 2){
          $sum_b = "0$sum_b";
        }
        //echo "$sum_r $sum_g $sum_b<br>";
        $sum_color = "#$sum_r$sum_g$sum_b";
        //echo $sum_color;
        //echo "<br><br>";
        echo "<form action=\"per-c.php\" method=\"get\">";
        echo "<table>";
        echo "<tr><th> Sum </th><th> $sum_color </th><th width=\"35\" height=\"35\" bgcolor=\"$sum_color\">  </th><th> 100% </th></tr>";

        foreach ($ses_codeColor as $key => $value) {
          $per_c2 = $per_c*100;
          $per_c2 = round($per_c2, 2);
          $key++;
          echo "<tr><th> $key </th><th> $value </th><th width=\"35\" height=\"35\" bgcolor=\"$value\">     </th><th> <input type=\"number\" name=\"$key\" MAX =\"100\" value=\"$per_c2\" OnChange=\"fncSum('A');\" style=\"width: 50px;\">%</th></tr>";
        }
        echo "<tr><td colspan=\"4\" align=\"right\"><input type=\"submit\" name=\"\"></td></tr>";
        echo "</form>";
}
function code_Color($X, $Y, $Z, $lvl){
if ($lvl == 'l') {
		$h = $X/256;
		$s = $Y/256;
		$l = $Z/256;
		$text = hslToRgb($h, $s, $l);
	}else if ($lvl == 'S') {
		$h = $X/256;
		$s = $Z/256;
		$l = $Y/256;
		$text = hslToRgb($h, $s, $l);
	}else if ($lvl == 'H') {	
		$h = $Z/256;
		$s = $X/256;
		$l = $Y/256;
		$text = hslToRgb($h, $s, $l);
	}else{
		$X = dechex ( $X );
		$Y = dechex ( $Y );
		$Z = dechex ( $Z );
		if(strlen($X) < 2){
			$X = "0$X";
		}
		if(strlen($Y) < 2){
			$Y = "0$Y";
		}
		if(strlen($X) < 2){
			$Z = "0$Z";
		}
		if ($lvl == 'B') {
			$text = "#$X$Y$Z";
		}else if ($lvl == 'G') {
			$text = "#$X$Z$Y";
		}else if ($lvl == 'R') {
			$text = "#$Z$X$Y";
		}
	}
	//echo $text;
  return $text;
}

function hslToRgb($h, $s, $l){
	$r;
	$g;
	$b;
	if ($s == 0) {
    $r = $g = $b = $l; // achromatic
  } else {
    function hue2rgb($p, $q, $t) {
      if ($t < 0) $t += 1;
      if ($t > 1) $t -= 1;
      if ($t < 1/6) return $p + ($q - $p) * 6 * $t;
      if ($t < 1/2) return $q;
      if ($t < 2/3) return $p + ($q - $p) * (2/3 - $t) * 6;
      return $p;
    }
  $q = $l < 0.5 ? $l * (1 + $s) : $l + $s - $l * $s;
  $p = 2 * $l - $q;
  #echo "q = $p , p = $p";
  $r = hue2rgb($p, $q, $h + 1/3);
  $g = hue2rgb($p, $q, $h);
  $b = hue2rgb($p, $q, $h - 1/3);
  #echo "<br>r = $r , g = $g , b = $b";
  $r = $r * 255 ;
  $g = $g * 255 ;
  $b = $b * 255 ;
  #echo "<br>r = $r , g = $g , b = $b";
  $r = round($r, 0);
  $g = round($g, 0);
  $b = round($b, 0);
  #echo "<br>r = $r , g = $g , b = $b";
  $x=16*16;
  $r = dechex($r);
  $g = dechex($g);
  $b = dechex($b);
  if(strlen($r) < 2){
    $r = "0$r";
  }
  if(strlen($g) < 2){
    $g = "0$g";
  }
  if(strlen($b) < 2){
    $b = "0$b";
  }
  #echo "<br>r = $r , g = $g , b = $b";
  $code_color = "#$r$g$b";
  #echo "<br>$code_color<br>";
  return $code_color;
  }
}
?>



<script type="text/javascript">

</script>