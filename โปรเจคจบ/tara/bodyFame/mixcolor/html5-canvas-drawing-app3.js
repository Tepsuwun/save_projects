var canvas;
var context;
var canvasWidth = 600;
var canvasHeight = 300;
var clickX = new Array();
var clickY = new Array();
var paint = false;
var curColor = colorPurple;
var totalLoadResources = 8;
var curLoadResNum = 0;
var locX = 0;
var locY = 0;
var locZ = 0;
var lvl = 'l';
var text;
document.getElementById("lvlColor").innerHTML = "lvlColor" + " : " + lvl;

function resourceLoaded()
{
	if(++curLoadResNum >= totalLoadResources){
		redraw();
	}
}

function prepareCanvas()
{
	if(!lvl){
		lvl = 'l';
	}
	if (!locX) {
		locX = 128;
	}
	if (!locY) {
		locY = 128;
	}
	if (!locZ) {
		locZ = 128;
	}
	var canvasDiv = document.getElementById('canvasDiv');
	canvas = document.createElement('canvas');
	canvas.setAttribute('width', canvasWidth);
	canvas.setAttribute('height', canvasHeight);
	canvas.setAttribute('id', 'canvas');
	canvasDiv.appendChild(canvas);
	if(typeof G_vmlCanvasManager != 'undefined') {
		canvas = G_vmlCanvasManager.initElement(canvas);
	}
	context = canvas.getContext("2d"); 
	
	$('#canvas').mousedown(function(e)
	{
		var mouseX = e.pageX - this.offsetLeft;
		var mouseY = e.pageY - this.offsetTop;
		paint = true;
		clickX = new Array();
		clickY = new Array();
		addClick(mouseX, mouseY, false);
		redraw();
		main_colors(locZ,lvl);
		degree_main_colors(locX,locY,lvl);
	});
	
	$('#canvas').mousemove(function(e){
		if(paint==true){
			addClick(e.pageX - this.offsetLeft, e.pageY - this.offsetTop, true);
			redraw();
			colors(locX,locY,locZ,lvl);
			degree_main_colors(locX,locY,lvl);
			Res=3;
			main_colors(locZ,lvl,Res,lvl);
			arc(locX,locY);
			degree_arc(locZ);
		}
	});
	
	$('#canvas').mouseup(function(e){
		paint = false;
		clearCanvas();
	  	redraw();
		clearMain_colors();
	  	main_colors(locZ,lvl);
		degree_main_colors(locX,locY,lvl);
		arc(locX,locY);
		degree_arc(locZ);
		colors(locX,locY,locZ,lvl);
	});
	
	$('#canvas').mouseleave(function(e){
		paint = false;
	});
	if (locX = 'undefined') {
		locX = 128;
	}
	if (locY = 'undefined') {
		locY = 128;
	}
	if (locZ = 'undefined') {
		locZ = 128;
	}
	main_colors(locZ,lvl);
	degree_main_colors(locX,locY,lvl);
	colors(locX,locY,locZ,lvl);
	arc(locX,locY);
	degree_arc(locZ);
}

function addClick(x, y, dragging)
{
	clickX.push(x);
	clickY.push(y);

}

function clearCanvas()
{
	context.clearRect(0, 0, canvasWidth, canvasHeight);
}
function clearMain_colors()
{
	context.clearRect(0, 0, 300, 300);
}

function redraw()
{
	
	//clearCanvas();
	locX = clickX[clickX.length-1];
	if (locX < 256) {
		locX2 = clickX[clickX.length-1];
	}
	if (locX > 300 && locX < 400) {
		locZ = clickY[clickY.length-1];
		if (locZ > 256) {
			locZ = 256;
		}
			var Res=10;
		main_colors(locZ,lvl,Res);
	}
	if (locX > 256) {
		locX = locX2;
	}else if(locX < 256){
		locY = clickY[clickY.length-1];
		if (locY > 256) {
			locY = 256;
		}
	}
	
	if (clickX == "") {
		//context.fillText("clickX =>", 0, 570);
		//context.fillText("clickY =>", 0, 590);
	}else{
		//context.fillText(clickX, 0, 570);
		//context.fillText(clickY, 0, 590);
		document.getElementById("length_clickX").innerHTML = "clickX" + " : " + locX;
		document.getElementById("length_clickY").innerHTML = "clickY" + " : " + locY;
		document.getElementById("length_clickZ").innerHTML = "locZ" + " : " + locZ;
		document.getElementById("Array_clickX").innerHTML = "Array_clickX" + " : [" + clickX + "]";
		document.getElementById("Array_clickY").innerHTML = "Array_clickY" + " : [" + clickY + "]";
	}
	//clickX = new Array();
	//clickY = new Array();
	//context.clearRect(0,0,640,480);
}
function main_colors(Z,lvl,Res) {
	document.getElementById("lvlColor").innerHTML = "lvlColor" + " : " + lvl;
	var x = 0;
	var y = 0;
	var Width  = 1;
	var Height = 1;
	if(!Res){
		Res=1;
	}else{
		Width = Res;
		Height = Res;
	}
	if(!lvl){
		lvl = 'l';
	}
	//var Res    = 1;
	for (var i = 0; i <= 255/Res; i++) {
		for (var j = 0; j <= 255/Res; j++) {
			context.beginPath();
			//hsl(hue, saturation, lightness)
			//context.fillStyle = 'hsl('+i*Res+', '+ j*Res/2.55 +'%, 50%)';
			if (lvl == 'B') {
				context.fillStyle = 'rgb('+ i*Res +','+ j*Res +','+ Z +')';
			}else if (lvl == 'G'){
				context.fillStyle = 'rgb('+ i*Res +','+ Z +','+ j*Res +')';
			}else if (lvl == 'R'){
				context.fillStyle = 'rgb('+ Z +','+ i*Res +','+ j*Res +')';
			}else if (lvl == 'l'){
				context.fillStyle = 'hsl('+i*Res*360/255+', '+ j*Res/2.55 +'%, '+ Z/2.56 +'%)';
			}else if (lvl == 'S'){
				context.fillStyle = 'hsl('+i*Res*360/255+', '+ Z/2.56 +'%, '+ j*Res/2.55 +'%)';
			}else if (lvl == 'H'){
				context.fillStyle = 'hsl('+Z*360/256+', '+ i*Res/2.55 +'%, '+ j*Res/2.55 +'%)';
			}
			//context.fillStyle = 'rgb('+ i*Res +','+ j*Res +','+ Z +')';
			//document.getElementById("_100Z").innerHTML = "Z%" + " : " + Z/2.56;
			context.rect(i*Width+x, j*Height+y, Width, Height);
			context.fill();
		}
	}
	context.fillStyle = '#000000';
}
function degree_main_colors(locX,locY,lvl) {
	if(!lvl){
		lvl = 'R';
	}
	var x = 300;
	var y = 0;
	var Width  = 10;
	var Height = 1;
	var Res    = 1;
	for (var i = 0; i <= 255/Res; i++) {
		context.beginPath();
		if (lvl == 'B') {
			context.fillStyle = 'rgb('+ locX +','+ locY +','+ i +')';
		}else if (lvl == 'G') {
			context.fillStyle = 'rgb('+ locX +','+ i +','+ locY +')';
		}else if (lvl == 'R') {
			context.fillStyle = 'rgb('+ i +','+ locX +','+ locY +')';
		}else if (lvl == 'l'){
			context.fillStyle = 'hsl('+ locX*360/256 +', '+ locY/2.56 +'%, '+ i/2.56 +'%)';
		}else if (lvl == 'S'){
			context.fillStyle = 'hsl('+ locX*360/256 +', '+ i/2.56 +'%, '+ locY/2.56 +'%)';
		}else if (lvl == 'H'){
			context.fillStyle = 'hsl('+ i*360/256 +', '+ locX/2.56 +'%, '+ locY/2.56 +'%)';
		}
		context.rect(x, i*Height+y, Width, Height);
		context.fill();
	}
}
function colors(X,Y,Z,lvl) {
	if(!lvl){
		lvl = 'R';
	}
	if(!X) X=0;
    if(!Y) Y=0;
    if(!Z) Z=0;
	var x = 400;
	var y = 50;
	var Width  = 100;
	var Height = 100;
	context.beginPath();
	if (lvl == 'B') {
		context.fillStyle = 'rgb('+ X +','+ Y +','+ Z +')';
	}else if (lvl == 'G') {
		context.fillStyle = 'rgb('+ X +','+ Z +','+ Y +')';
	}else if (lvl == 'R') {
		context.fillStyle = 'rgb('+ Z +','+ X +','+ Y +')';
	}else if (lvl == 'l') {
		context.fillStyle = 'hsl('+ X*360/256 +', '+ Y/2.56 +'%, '+ Z/2.56 +'%)';
	}else if (lvl == 'S') {
		context.fillStyle = 'hsl('+ X*360/256 +', '+ Z/2.56 +'%, '+ Y/2.56 +'%)';
	}else if (lvl == 'H') {
		context.fillStyle = 'hsl('+ Z*360/256 +', '+ X/2.56 +'%, '+ Y/2.56 +'%)';
	}
	context.rect(x, y, Width, Height);
	context.fill();

	context.beginPath();
	context.fillStyle = '#FFF';
	context.rect(x-65, y+Height+5, 230, 50);
	context.fill();
	context.fillStyle = '#000';
	context.font = '10pt Calibri';
	//context.font = '15pt';
	//var text = 'rgb('+ X +','+ Y +','+ Z +')';
	if (lvl == 'B') {
		text = 'rgb('+ X +','+ Y +','+ Z +')';
	}else if (lvl == 'G') {
		text = 'rgb('+ X +','+ Z +','+ Y +')';
	}else if (lvl == 'R') {
		text = 'rgb('+ Z +','+ X +','+ Y +')';
	}else if (lvl == 'l') {
		int_X = X*360/256;
		int_X = toFixed( int_X,0 );
		int_Y = Y/2.56;
		int_Y = toFixed( int_Y,0 );
		int_Z = Z/2.56;
		int_Z = toFixed( int_Z,0 );
		text = 'hsl('+ int_X +', '+ int_Y +'%, '+ int_Z +'%)';
	}else if (lvl == 'S') {
		int_X = X*360/256;
		int_X = toFixed( int_X,0 );
		int_Y = Y/2.56;
		int_Y = toFixed( int_Y,0 );
		int_Z = Z/2.56;
		int_Z = toFixed( int_Z,0 );
		text = 'hsl('+ int_X +', '+ int_Z +'%, '+ int_Y +'%)';
	}else if (lvl == 'H') {
		int_X = X/2.56;
		int_X = toFixed( int_X,0 );
		int_Y = Y/2.56;
		int_Y = toFixed( int_Y,0 );
		int_Z = Z*360/256;
		int_Z = toFixed( int_Z,0 );
		text = 'hsl('+ int_Z +', '+ int_X +'%, '+ int_Y +'%)';
	}
	context.textAlign = 'center';
	context.fillText(text, x+50, y+Height+20);
	if (lvl == 'l') {
		h = X/256;
		s = Y/256;
		l = Z/256;
		text = hslToRgb(h, s, l);
	}else if (lvl == 'S') {
		h = X/256;
		s = Z/256;
		l = Y/256;
		text = hslToRgb(h, s, l);
	}else if (lvl == 'H') {	
		h = Z/256;
		s = X/256;
		l = Y/256;
		text = hslToRgb(h, s, l);
	}else{
		X = X.toString(16);
		Y = Y.toString(16);
		Z = Z.toString(16);
		Xl = X.length;
		Yl = Y.length;
		Zl = Z.length;
		if (Xl == 1) {
			var X = '0'+X;
		}
		if (Yl == 1) {
			var Y = '0'+Y;
		}
		if (Zl == 1) {
			var Z = '0'+Z;
		}
		if (lvl == 'B') {
			text = '#'+ X +''+ Y +''+ Z +'';
		}else if (lvl == 'G') {
			text = '#'+ X +''+ Z +''+ Y +'';
		}else if (lvl == 'R') {
			text = '#'+ Z +''+ X +''+ Y +'';
		}
	}
	//var text = '#'+ X +''+ Y +''+ Z +'';
	context.fillText(text, x+50, y+Height+40);

	document.getElementById("Color").innerHTML = "code_Color" + " : " + text;
	context.beginPath();
	context.fillStyle = text;
	context.rect(x+25, y+150, Width-50, Height-50);
	context.fill();
}
function arc(X,Y) {
	context.beginPath();
	context.arc(X, Y, 6, 0, 2 * Math.PI, false);
	//context.fillStyle = 'black';
	//context.fill();
	context.lineWidth = 2;
	context.strokeStyle = '#FFF';
	context.stroke();

	context.beginPath();
	context.arc(X, Y, 8, 0, 2 * Math.PI, false);
	context.lineWidth = 2;
	context.strokeStyle = '#000';
	context.stroke();
}
function degree_arc(Z) {
	context.beginPath();
	context.arc(305, Z, 6, 0, 2 * Math.PI, false);
	//context.fillStyle = 'black';
	//context.fill();
	context.lineWidth = 2;
	context.strokeStyle = '#FFF';
	context.stroke();

	context.beginPath();
	context.arc(305, Z, 8, 0, 2 * Math.PI, false);
	context.lineWidth = 2;
	context.strokeStyle = '#000';
	context.stroke();
}
function lvlColor(lvl2) {
	if(!lvl2) lvl2='l';
	lvl = lvl2;
	//document.getElementById("lvlColor").innerHTML = "lvlColor" + " : " + lvl + " " + locX;
	main_colors(locZ,lvl);
	degree_main_colors(locX,locY,lvl);
	colors(locX,locY,locZ,lvl);
	arc(locX,locY);
	degree_arc(locZ);
}
function toFixed(num, pre){
    num *= Math.pow(10, pre);
    num = (Math.round(num, pre) + (((num - Math.round(num, pre))>=0.5)?1:0)) / Math.pow(10, pre);
    return num.toFixed(pre);
}
function hslToRgb(h, s, l) {
  var r, g, b;

  if (s == 0) {
    r = g = b = l; // achromatic
  } else {
    function hue2rgb(p, q, t) {
      if (t < 0) t += 1;
      if (t > 1) t -= 1;
      if (t < 1/6) return p + (q - p) * 6 * t;
      if (t < 1/2) return q;
      if (t < 2/3) return p + (q - p) * (2/3 - t) * 6;
      return p;
    }

    var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
    var p = 2 * l - q;

    r = hue2rgb(p, q, h + 1/3);
    g = hue2rgb(p, q, h);
    b = hue2rgb(p, q, h - 1/3);
  }
  r = r * 255 ;
  g = g * 255 ;
  b = b * 255 ;
  /*function toFixed(num, pre){
    num *= Math.pow(10, pre);
    num = (Math.round(num, pre) + (((num - Math.round(num, pre))>=0.5)?1:0)) / Math.pow(10, pre);
    return num.toFixed(pre);
  }*/
  r = toFixed( r,0 );
  g = toFixed( g,0 );
  b = toFixed( b,0 );
  r = parseInt(r);
  g = parseInt(g);
  b = parseInt(b);
  r = r.toString(16);
  g = g.toString(16);
  b = b.toString(16);
  rl = r.length;
  gl = g.length;
  bl = b.length;
  if (rl == 1) {
    var r = '0'+r;
  }
  if (gl == 1) {
    var g = '0'+g;
  }
  if (bl == 1) {
    var b = '0'+b;
  }
  var code_color = '#'+r+g+b; 
  return [ code_color ];
}

//###########################################################################
function Code_colors(X,Y,Z,lvl) {
	if (lvl == 'l') {
		h = X/256;
		s = Y/256;
		l = Z/256;
		text = hslToRgb(h, s, l);
	}else if (lvl == 'S') {
		h = X/256;
		s = Z/256;
		l = Y/256;
		text = hslToRgb(h, s, l);
	}else if (lvl == 'H') {	
		h = Z/256;
		s = X/256;
		l = Y/256;
		text = hslToRgb(h, s, l);
	}else{
		X = X.toString(16);
		Y = Y.toString(16);
		Z = Z.toString(16);
		Xl = X.length;
		Yl = Y.length;
		Zl = Z.length;
		if (Xl == 1) {
			var X = '0'+X;
		}
		if (Yl == 1) {
			var Y = '0'+Y;
		}
		if (Zl == 1) {
			var Z = '0'+Z;
		}
		if (lvl == 'B') {
			text = '#'+ X +''+ Y +''+ Z +'';
		}else if (lvl == 'G') {
			text = '#'+ X +''+ Z +''+ Y +'';
		}else if (lvl == 'R') {
			text = '#'+ Z +''+ X +''+ Y +'';
		}
	}
	//document.getElementById("Color2").innerHTML = "code_Color_text" + " : " + text;
	return [text];
}

function showResult(x,y,z,lvl) {
  /*if (str.length==0) { 
    document.getElementById("livesearch").innerHTML="";
    document.getElementById("livesearch").style.border="0px";
    return;
  }*/
  if (window.XMLHttpRequest) {
    xmlhttp=new XMLHttpRequest();
  } else { 
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) { 
      	document.getElementById("livesearch").innerHTML=this.responseText;
      	document.getElementById("livesearch").style.border="1px solid #A5ACB2";
    }
  }
  xmlhttp.open("GET","livesearch.php?x="+x+"&y="+y+"&z="+z+"&lvl="+lvl,true);
  xmlhttp.send();
}

function command(command) {
  if (window.XMLHttpRequest) {
    xmlhttp=new XMLHttpRequest();
  } else { 
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) { 
      	document.getElementById("livesearch").innerHTML=this.responseText;
      	document.getElementById("livesearch").style.border="1px solid #A5ACB2";
    }
  }
  xmlhttp.open("GET","livesearch.php?command="+command,true);
  xmlhttp.send();
}
