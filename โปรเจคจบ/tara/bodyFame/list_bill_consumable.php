<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<?php 
include"conn.php";
$i = 1;
$num = "";
$sql = "SELECT * FROM ist_bill_con,comnsumable WHERE ist_bill_con.cons_id = comnsumable.cons_id order by bcons_id ASC";
$query = mysqli_query($conn,$sql);
echo "<form action=\"check_conf.php?page=ist_bill_consumable\" method=\"post\">";
echo "<table border=\"0\" class=\"table table-striped\" width=\"100%\"> ";
echo "<thead><tr class=\"info\">";
echo "<th> <p align = 'center'> ใบเบิกวัสดุสิ้นเปลื้อง	</p></th>";
echo "<th> <p align = 'center'> ชื่อรายการวัสดุสิ้นเปลือง	</p></th>";
echo "<th> <p align = 'center'> ชนิดวัสดุสิ้นเปลือง		</p></th>";
echo "<th> <p align = 'center'> ราคาสี					</p></th>";
echo "<th> <p align = 'center'> จำนวนสีที่เพิ่มลงสต๊อค		</p></th>";
echo "<th> <p align = 'center'> จำนวนสีในสต๊อค			</p></th>";
echo "<th> <p align = 'center'> สถานะการอนัติ/ไม่อนุมัติ	</p></th>";
echo "</tr></thead><tbody>";
while ($result = mysqli_fetch_array($query,MYSQLI_ASSOC)) {
	if ($i==1) {	
		echo "<tr class=\"active\">";
		$i = 0;
	}else{
		echo "<tr class=\"success\">";
		$i = 1;
	}
	$ID = $result["atc_id"];
	if ($ID == $num) {
		echo "<th><p align = 'center'> ,, </p></th>";
	}else{
		echo "<th><p align = 'center'>".$ID."</p></th>";
		$num = $ID;
	}
	echo "<td><p align = 'center'>".$result["cons_name"].			"</p></td>";
	echo "<td><p align = 'center'>".$result["cons_type"].			"</p></td>";
	echo "<td><p align = 'center'>".$result["bcn_price"].	"</p></td>";
	echo "<td><p align = 'center'>".$result["bcn_amount"].	"</p></td>";
	echo "<td><p align = 'center'>".$result["cons_amount"].		"</p></td>";
	$stat=$result["bcn_stat"];
	$notShow=$result["notShow"];
	$list_id = $result["bcn_id"];
	if ($notShow == 1) {
		if ($stat==1) {
			echo "<td><p align = 'center'>อนุมัติ</p></td>";
		}else{
			echo "<td><p align = 'center'>ไม่อนุมัติ</p></td>";
		}
	}else{
		echo "<td><input type=\"radio\" name=\"statOK$list_id\" value=\"no\">ไม่อนุมัติ$list_id<br>
  			<input type=\"radio\" name=\"statOK$list_id\" value=\"yes\">อนุมัติ$list_id</td>";
	}
	echo "</tr>";
}
echo "</tbody></table>";
echo "<p align = 'right'><input type=\"submit\" value=\"Submit\"></p></form>";
?>