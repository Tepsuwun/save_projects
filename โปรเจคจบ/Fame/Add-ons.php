﻿<?php 
ini_set('max_execution_time', 300); //300 seconds = 5 minutes
include 'function.php'; 
$ID_Mi = $_POST["ID_Mi"];
$date = $_POST["date"];
$time = $_POST["time"];
$dateTime = $date." ".$time.":00";
echo "$dateTime";
$PhasSum = download_Pw($dateTime,$ID_Mi);
$PhasSumDay = download_PwDay($dateTime,$ID_Mi);
$PhasTime2 = download_Time2($dateTime);
$PhasSumMonth = download_PwMonth($dateTime,$ID_Mi);
  /*foreach ($PhasSumMonth as $key => $value) {
    echo "<br>";
    echo $key;
    $A = $key;
    echo "<br>";
    foreach ($PhasSumMonth[$A] as $key2 => $value2) {
        echo $key2."=".$value2."<br>";
    }
  }*/
$AA="sdsdsdsdsddsdsdsdsdsdsdsdsdsasdsasd";
$A = "Time1";
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>

<!-- ตั้งค่า -->
<script type="text/javascript">
$(function () {
    $('#mailgegraph').highcharts({
        //ชื่อกราฟ
        title: {
            text: 'กราฟพลังงานรายMain',
            x: -20 //center
        },
        subtitle: {
            text: '<?php echo "แสดงค่าพลังงานศะสมที่ได้จากการผลิตพลังงานจากเซลล์แสงอาทิตย์ในแต่ละนาทีระหว่าง ".$dateTime." ถึง ".$dateTime; ?>',
            x: -20
        },
        //แนวนอน
        xAxis: {
            /*categories: ['เม.ย. 58', 'พ.ค. 58', 'มิ.ย. 58',
                'ก.ค. 58', 'ส.ค. 58', 'ก.ย. 58', 'ต.ค. 58', 'พ.ย. 58', 'ธ.ค. 58',
                'ม.ค. 59', 'ก.พ. 59', 'มี.ค. 59', 'เม.ย. 59', 'พ.ค. 59', 'มิ.ย. 59',
                'ก.ค. 59', 'ส.ค. 59', 'ก.ย. 59', 'ต.ค. 59', 'พ.ย. 59', 'ธ.ค. 59',
                'ม.ค. 60', 'ก.พ. 60', 'มี.ค. 60', 'เม.ย. 60', 'พ.ค. 60', 'มิ.ย. 60',
                'ก.ค. 60', 'ส.ค. 60']*/
            categories: [
                <?php 
                for ($i = 0; $i <= count($PhasTime2)-1; $i++) {
                    if($i>0){
                        echo ",";
                    }
                    echo "'".$PhasTime2[$i]."'";
                }
                 ?>
            ]
        },
        yAxis: {
            title: {
                text: 'จำนวนพลังงานที่ได้ (Kw/h)'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: 'บาท'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
        //     name: 'Tokyo',
        //     data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]
        // }, {
            name: 'เฟส1',
            data: [
                <?php 
                $i = 0;
                foreach ($PhasSum["Phas1"] as $key => $valu) {
                    if($i>0){
                        echo ',';
                    }
                    echo number_format($valu, 2, '.', '');
                    $i++;
                }
            ?>
            ]
        }, 
         
        {
            name: 'เฟส 2',
            data: [ 
            <?php 
                $i = 0;
                foreach ($PhasSum["Phas2"] as $key => $valu) {
                    if($i>0){
                        echo ',';
                    }
                    echo number_format($valu, 2, '.', '');
                    $i++;
                }
            ?>
            ]
        },
        {
            name: 'เฟส 3',
            data: [ 
            <?php 
                $i = 0;
                foreach ($PhasSum["Phas3"] as $key => $valu) {
                    if($i>0){
                        echo ',';
                    }
                    echo number_format($valu, 2, '.', '');
                    $i++;
                }
            ?>
            ]
        }]
    });
});
//################################################################################################
$(function () {
    $('#graphsMonth').highcharts({
        //ชื่อกราฟ
        title: {
            text: 'กราฟพลังงานรายเดือน',
            x: -20 //center
        },
        subtitle: {
            text: '<?php echo "แสดงค่าพลังงานศะสมที่ได้จากการผลิตพลังงานจากเซลล์แสงอาทิตย์ในแต่ละชั่วของเดือน" ?>',
            x: -20
        },
        //แนวนอน
        xAxis: {
            /*categories: ['เม.ย. 58', 'พ.ค. 58', 'มิ.ย. 58',
                'ก.ค. 58', 'ส.ค. 58', 'ก.ย. 58', 'ต.ค. 58', 'พ.ย. 58', 'ธ.ค. 58',
                'ม.ค. 59', 'ก.พ. 59', 'มี.ค. 59', 'เม.ย. 59', 'พ.ค. 59', 'มิ.ย. 59',
                'ก.ค. 59', 'ส.ค. 59', 'ก.ย. 59', 'ต.ค. 59', 'พ.ย. 59', 'ธ.ค. 59',
                'ม.ค. 60', 'ก.พ. 60', 'มี.ค. 60', 'เม.ย. 60', 'พ.ค. 60', 'มิ.ย. 60',
                'ก.ค. 60', 'ส.ค. 60']*/
            categories: [
                <?php 
                $i = 0;
                foreach ($PhasSumMonth as $key => $value) {
                    if($i > 0){
                        echo ",";
                    }
                    $Day = intval($key);
                    $Day++;
                    echo "'วันที่".$Day."'";
                    $i++;
                }
                /*for ($i = 1; $i <= count($PhasTime2)-1; $i++) {
                    if($i>0){
                        echo ",";
                    }
                    echo "'".$PhasTime2[$i]."'";
                }*/
                 ?>
            ]
        },
        yAxis: {
            title: {
                text: 'จำนวนพลังงานที่ได้ (Kw/h)'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: 'บาท'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
        //     name: 'Tokyo',
        //     data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]
        // }, {
            name: 'เฟส1',
            data: [
            <?php 
                $i = 0;
                foreach ($PhasSumMonth as $key => $value) {
                    $A = $key;
                    foreach ($PhasSumMonth[$A] as $key2 => $value2) {
                        if ($key2 == "sumPW1") {
                            if($i>0){
                                echo ',';
                            }
                             $i++;
                            echo number_format($value2, 2, '.', '');
                        }
                    }
                }
            ?>
            ]
        }, 
         
        {
            name: 'เฟส 2',
            data: [ 
            <?php 
                $i = 0;
                foreach ($PhasSumMonth as $key => $value) {
                    $A = $key;
                    foreach ($PhasSumMonth[$A] as $key2 => $value2) {
                        if ($key2 == "sumPW2") {
                            if($i>0){
                                echo ',';
                            }
                             $i++;
                            echo number_format($value2, 2, '.', '');
                        }
                    }
                }
            ?>
            ]
        },
        {
            name: 'เฟส 3',
            data: [ 
            <?php 
                $i = 0;
                foreach ($PhasSumMonth as $key => $value) {
                    $A = $key;
                    foreach ($PhasSumMonth[$A] as $key2 => $value2) {
                        if ($key2 == "sumPW3") {
                            if($i>0){
                                echo ',';
                            }
                             $i++;
                            echo number_format($value2, 2, '.', '');
                        }
                    }
                }
            ?>
            ]
        }]
    });
});
//################################################################################################
$(function () {
    $('#graphsHour').highcharts({
        //ชื่อกราฟ
        title: {
            text: 'กราฟพลังงานรายชั่วโมง',
            x: -20 //center
        },
        subtitle: {
            text: '<?php echo "แสดงค่าพลังงานศะสมที่ได้จากการผลิตพลังงานจากเซลล์แสงอาทิตย์ในแต่ละนาทีระหว่าง ".$dateTime." ถึง ".$dateTime; ?>',
            x: -20
        },
        //แนวนอน
        xAxis: {
            /*categories: ['เม.ย. 58', 'พ.ค. 58', 'มิ.ย. 58',
                'ก.ค. 58', 'ส.ค. 58', 'ก.ย. 58', 'ต.ค. 58', 'พ.ย. 58', 'ธ.ค. 58',
                'ม.ค. 59', 'ก.พ. 59', 'มี.ค. 59', 'เม.ย. 59', 'พ.ค. 59', 'มิ.ย. 59',
                'ก.ค. 59', 'ส.ค. 59', 'ก.ย. 59', 'ต.ค. 59', 'พ.ย. 59', 'ธ.ค. 59',
                'ม.ค. 60', 'ก.พ. 60', 'มี.ค. 60', 'เม.ย. 60', 'พ.ค. 60', 'มิ.ย. 60',
                'ก.ค. 60', 'ส.ค. 60']*/
            categories: [
                <?php 
                for ($i = 0; $i <= count($PhasTime2)-1; $i++) {
                    if($i>0){
                        echo ",";
                    }
                    echo "'".$PhasTime2[$i]."'";
                }
                 ?>
            ]
        },
        yAxis: {
            title: {
                text: 'จำนวนพลังงานที่ได้ (Kw/h)'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: 'บาท'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
        //     name: 'Tokyo',
        //     data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]
        // }, {
            name: 'เฟส1',
            data: [
                <?php 
                $i = 0;
                foreach ($PhasSum["Phas1"] as $key => $valu) {
                    if($i>0){
                        echo ',';
                    }
                    echo number_format($valu, 2, '.', '');
                    $i++;
                }
            ?>
            ]
        }, 
         
        {
            name: 'เฟส 2',
            data: [ 
            <?php 
                $i = 0;
                foreach ($PhasSum["Phas2"] as $key => $valu) {
                    if($i>0){
                        echo ',';
                    }
                    echo number_format($valu, 2, '.', '');
                    $i++;
                }
            ?>
            ]
        },
        {
            name: 'เฟส 3',
            data: [ 
            <?php 
                $i = 0;
                foreach ($PhasSum["Phas3"] as $key => $valu) {
                    if($i>0){
                        echo ',';
                    }
                    echo number_format($valu, 2, '.', '');
                    $i++;
                }
            ?>
            ]
        }]
    });
});
//############################################################
$(function () {
    $('#graphsDay').highcharts({
        //ชื่อกราฟ
        title: {
            text: 'กราฟพลังงานรายวัน',
            x: -20
        },
        subtitle: {
            text: '<?php echo "แสดงค่าพลังงานศะสมที่ได้จากการผลิตพลังงานจากเซลล์แสงอาทิตย์ในแต่ละชั่วของวันที่ "; ?>',
            x: -20
        },
        xAxis: {
            categories: [
                <?php 
                for ($i = 1; $i <= 24; $i++) {
                    if($i>1){
                        echo ",";
                    }
                    echo "'".sprintf("%02d",$i).":00 น.'";
                }
                 ?>
            ]
        },
        yAxis: {
            title: {
                text: 'จำนวนพลังงานที่ได้ (Kw/h)'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: 'บาท'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            name: 'เฟส1',
            data: [
                <?php 
                for ($i=0; $i < 24; $i++) { 
                    if($i>0){
                        echo ',';
                    }
                foreach ($PhasSumDay[$i] as $key => $valu) {
                    if ($key == 'sumPW1') {
                        echo number_format($valu, 2, '.', '');
                    }
                }
                }
            ?>
            ]
        }, 
         
        {
            name: 'เฟส 2',
            data: [ 
            <?php 
                for ($i=0; $i < 24; $i++) { 
                    if($i>0){
                        echo ',';
                    }
                    foreach ($PhasSumDay[$i] as $key => $valu) {
                        if ($key == 'sumPW2') {
                            echo number_format($valu, 2, '.', '');
                        }
                    }
                }
            ?>
            ]
        },
        {
            name: 'เฟส 3',
            data: [ 
            <?php 
                for ($i=0; $i < 24; $i++) { 
                    if($i>0){
                        echo ',';
                    }
                    foreach ($PhasSumDay[$i] as $key => $valu) {
                        if ($key == 'sumPW3') {
                            echo number_format($valu, 2, '.', '');
                        }
                    }
                }
            ?>
            ]
        }]
    });
});
</script>
