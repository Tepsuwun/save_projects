<style>
@import 'https://code.highcharts.com/css/highcharts.css';

#container {
	height: 400px;
	max-width: 100%;
	margin: 0 auto;
}

/* Link the series colors to axis colors */
.highcharts-color-0 {
	fill: #7cb5ec;
	stroke: #7cb5ec;
}
.highcharts-axis.highcharts-color-0 .highcharts-axis-line {
	stroke: #7cb5ec;
}
.highcharts-axis.highcharts-color-0 text {
	fill: #7cb5ec;
}
.highcharts-color-1 {
	fill: #90ed7d;
	stroke: #90ed7d;
}
.highcharts-axis.highcharts-color-1 .highcharts-axis-line {
	stroke: #90ed7d;
}
.highcharts-axis.highcharts-color-1 text {
	fill: #90ed7d;
}
.highcharts-color-2 {
    fill: #CC0000;
    stroke: #CC0000;
}
.highcharts-axis.highcharts-color-2 .highcharts-axis-line {
    stroke: #CC0000;
}
.highcharts-axis.highcharts-color-2 text {
    fill: #CC0000;
}


.highcharts-yaxis .highcharts-axis-line {
	stroke-width: 2px;
}


</style>

<script src="https://code.highcharts.com/js/highcharts.js"></script>
<script src="https://code.highcharts.com/js/modules/exporting.js"></script>

<div id="container"></div>

<script text="test/javascript">

Highcharts.chart('container', {

    chart: {
        type: 'column'
    },

    title: {
        text: 'Styling axes and columns'
    },

    yAxis: [{
        className: 'highcharts-color-0',
        title: {
            text: 'Pw'
        }
    }, {
        className: 'highcharts-color-1',
        opposite: true,
        title: {
            text: 'V'
        }
    }, {
        className: 'highcharts-color-2',
        opposite: true,
        title: {
            text: 'A'
        }
    }],

    plotOptions: {
        column: {
            borderRadius: 5
        }
    },

    series: [{
        data: [100, 120, 130, 145 ,150 ,155 ,170 ,160 ,165 , 175, 189, 200, 210, 180, 160, 155, 185, 130, 150, 155, 154, 147, 159, 157, 123, 90, 110, 215, 160, 115, 206]
    }, {
        data: [234, 236, 234, 238, 235, 236, 238, 233, 238, 234, 235, 238, 237, 238, 235, 238, 232, 230, 235, 231, 238, 235, 237, 239, 234, 232, 230, 234, 235, 237, 238],
        yAxis: 1
    }, {
        data: [0.5, 0.4, 0.45, 0.58, 0.6, 0.65, 0.53, 0.50, 0.48, 0.57, 0.72, 0.42, 0.53, 0.45, 0.56, 0.68, 0.65, 0.57, 0.56, 0.57, 0.60, 0.65, 0.56, 0.57, 0.57, 0.56, 0.67, 0.53, 0.54, 0.49, 0.45],
        yAxis: 2
    }]

});
</script>