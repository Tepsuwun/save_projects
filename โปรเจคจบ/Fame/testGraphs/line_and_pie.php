<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/series-label.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>

<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

<script text="test/javascript">
Highcharts.chart('container', {
    title: {
        text: 'Combination chart'
    },
    xAxis: {
        categories: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
    },
    labels: {
        items: [{
            html: 'Total fruit consumption',
            style: {
                left: '50px',
                top: '18px',
                color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
            }
        }]
    },
    series: [{
        type: 'column',
        name: 'Jane',
        data: [3, 2, 1, 3, 4, 5, 7, 8, 4, 2, 3, 6]
    }, {
        type: 'column',
        name: 'John',
        data: [4, 6, 2, 2, 4, 7, 2, 7, 9, 4, 2, 5]
    }, {
        type: 'column',
        name: 'Joe',
        data: [5, 6, 7, 8, 8, 7, 9, 4, 6, 7, 8, 5]
    }, {
        type: 'spline',
        name: 'Average',
        data: [12/3, 14/3, 10/3, 13/3, 16/3, 19/3, 18/3, 19/3, 19/3, 13/3, 13/3, 16/3],
        //data: [9, 8, 9, 19, 10],
        marker: {
            lineWidth: 2,
            lineColor: Highcharts.getOptions().colors[3],
            fillColor: '#923456'
        }
    }, {
        type: 'spline',
        name: 'Total',
        //data: [3, 2.67, 3, 6.33, 3.33],
        data: [12, 14, 10, 13, 16, 19, 18, 19, 19, 13, 13, 16],
        marker: {
            lineWidth: 2,
            lineColor: Highcharts.getOptions().colors[4],
            fillColor: 'white'
        }
    }, {
        type: 'pie',
        name: 'Total consumption',
        data: [{
            name: 'Jane',
            y: 13,
            color: Highcharts.getOptions().colors[0] // Jane's color
        }, {
            name: 'John',
            y: 23,
            color: Highcharts.getOptions().colors[1] // John's color
        }, {
            name: 'Joe',
            y: 19,
            color: Highcharts.getOptions().colors[2] // Joe's color
        }],
        center: [100, 80],
        size: 100,
        showInLegend: false,
        dataLabels: {
            enabled: false
        }
    }]
});
</script>