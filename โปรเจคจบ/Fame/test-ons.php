﻿<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>

<!-- ตั้งค่า -->
<script type="text/javascript">
$(function () {
    $('#mailgegraph').highcharts({
        //ชื่อกราฟ
        title: {
            text: 'กราฟพลังงานรายMain',
            x: -20 //center
        },
        subtitle: {
            text: '<?php echo "แสดงค่าพลังงานศะสมที่ได้จากการผลิตพลังงานจากเซลล์แสงอาทิตย์ในแต่ละนาทีระหว่าง ";?>',
            x: -20
        },
        //แนวนอน
        xAxis: {
            /*categories: ['เม.ย. 58', 'พ.ค. 58', 'มิ.ย. 58',
                'ก.ค. 58', 'ส.ค. 58', 'ก.ย. 58', 'ต.ค. 58', 'พ.ย. 58', 'ธ.ค. 58',
                'ม.ค. 59', 'ก.พ. 59', 'มี.ค. 59', 'เม.ย. 59', 'พ.ค. 59', 'มิ.ย. 59',
                'ก.ค. 59', 'ส.ค. 59', 'ก.ย. 59', 'ต.ค. 59', 'พ.ย. 59', 'ธ.ค. 59',
                'ม.ค. 60', 'ก.พ. 60', 'มี.ค. 60', 'เม.ย. 60', 'พ.ค. 60', 'มิ.ย. 60',
                'ก.ค. 60', 'ส.ค. 60']*/
            categories: [
                <?php 
                /*for ($i = 0; $i <= 30; $i++) {
                    if($i>0){
                        echo ",";
                    }
                    echo "'".$i."'";
                }*/
                 ?> 
            ]
        },
        yAxis: {
            title: {
                text: 'จำนวนพลังงานที่ได้ (Kw/h)'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: 'บาท'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
             /*name: 'Tokyo',
             data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]*/
        // }, {
            name: 'เฟส1',
            data: [
                <?php 
                $i = 0;
                for ($i=0; $i < 20; $i++) { 
                    # code...
                    if($i>0){
                        echo ',';
                    }
                    echo number_format($i, 2, '.', '');
                    $i++;
                }
            ?>
            ]
        }]
    });
});
</script>
