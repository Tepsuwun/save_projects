<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<?php
$EE_Pw1 = array();
$time = array();
$V = array();
$A = array();
$W = array();
$num == 0;
 $sql = "SELECT * FROM `test` ORDER BY time";
  $query = mysqli_query($conn,$sql);
  while ($result = mysqli_fetch_array($query,MYSQLI_ASSOC)) {
      array_push($V, $result["V"]);
      array_push($A, $result["A"]);
      array_push($W, $result["W"]);
      array_push($EE_Pw1, $result["Wh"]);
      array_push($time, $result["time"]);
  }
$EE_Pw2 = array();
$time2 = array();
$V2 = array();
$A2 = array();
$W2 = array();
$num == 0;
 $sql = "SELECT * FROM `test` ORDER BY time";
  $query = mysqli_query($conn,$sql);
  while ($result = mysqli_fetch_array($query,MYSQLI_ASSOC)) {
      array_push($V2, $result["V"]);
      array_push($A2, $result["A"]);
      array_push($W2, $result["W"]);
      array_push($EE_Pw2, $result["Wh"]);
      array_push($time2, $result["time"]);
  }
$EE_Pw3 = array();
$time3 = array();
$V3 = array();
$A3 = array();
$W3 = array();
$num == 0;
 $sql = "SELECT * FROM `test` ORDER BY time";
  $query = mysqli_query($conn,$sql);
  while ($result = mysqli_fetch_array($query,MYSQLI_ASSOC)) {
      array_push($V3, $result["V"]);
      array_push($A3, $result["A"]);
      array_push($W3, $result["W"]);
      array_push($EE_Pw3, $result["Wh"]);
      array_push($time3, $result["time"]);
  }
/*echo $num."<br>";
for ($i=0;$i<10;$i++){
      echo $i." => ".$V[$i]." | ".$A[$i]." | ".$W[$i]." | ".$EE_Pw1[$i]." | ".$time[$i]."<br>";
}*/
?>

<script type="text/javascript">
$(function () {
    $('#mailgegraph').highcharts({
        title: {
            text: 'kW/h',
            x: -20 //center
        },
        subtitle: {
            text: 'Real time',
            x: -20
        },
        xAxis: {
            categories: [<?php 
                       for ($i=0;$i<10;$i++){
                          if ($i>0){
                             echo ", ";
                          }
                          echo "'".$time[$i]."'";
                       }
                    ?>]
        },
        yAxis: {
            title: {
                text: 'จำนวนพลังงานที่ได้ (Kw/h)'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: 'บาท'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
             name: 'Tokyo',
             data: [<?php 
                       for ($i=0;$i<10;$i++){
                          if ($i>0){
                             echo ", ";
                          }
                          echo "$EE_Pw1[$i]";
                       }
                    ?>]

         }]
    });
});
$(function () {
    $('#graphV').highcharts({
        title: {
            text: 'V',
            x: -20 //center
        },
        subtitle: {
            text: 'Real time',
            x: -20
        },
        xAxis: {
            categories: [<?php 
                       for ($i=0;$i<10;$i++){
                          if ($i>0){
                             echo ", ";
                          }
                          echo "'".$time[$i]."'";
                       }
                    ?>]
        },
        yAxis: {
            title: {
                text: 'จำนวนพลังงานที่ได้ (V)'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: 'บาท'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
             name: 'Tokyo',
             data: [<?php 
                       for ($i=0;$i<10;$i++){
                          if ($i>0){
                             echo ", ";
                          }
                          echo "$V[$i]";
                       }
                    ?>]

         }]
    });
});
$(function () {
    $('#graphA').highcharts({
        title: {
            text: 'A',
            x: -20 //center
        },
        subtitle: {
            text: 'Real time',
            x: -20
        },
        xAxis: {
            categories: [<?php 
                       for ($i=0;$i<10;$i++){
                          if ($i>0){
                             echo ", ";
                          }
                          echo "'".$time[$i]."'";
                       }
                    ?>]
        },
        yAxis: {
            title: {
                text: 'จำนวนพลังงานที่ได้ (A)'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: 'บาท'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
             name: 'Tokyo',
             data: [<?php 
                       for ($i=0;$i<10;$i++){
                          if ($i>0){
                             echo ", ";
                          }
                          echo "$A[$i]";
                       }
                    ?>]

         }]
    });
});
$(function () {
    $('#graphW').highcharts({
        title: {
            text: 'kW',
            x: -20 //center
        },
        subtitle: {
            text: 'Real time',
            x: -20
        },
        xAxis: {
            categories: [<?php 
                       for ($i=0;$i<10;$i++){
                          if ($i>0){
                             echo ", ";
                          }
                          echo "'".$time[$i]."'";
                       }
                    ?>]
        },
        yAxis: {
            title: {
                text: 'จำนวนพลังงานที่ได้ (kW)'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: 'บาท'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
             name: 'Tokyo',
             data: [<?php 
                       for ($i=0;$i<10;$i++){
                          if ($i>0){
                             echo ", ";
                          }
                          echo "$W[$i]";
                       }
                    ?>]

         }]
    });
});

</script>