<!meta http-equiv='refresh' content='0;url=testGraphs.php?'>
<?php 
session_start();
include 'menu/menu.php';
include 'checklogin.php';
include 'conn.php';
?>

<style type="text/css">
.form-style-9{
    max-width: 450px;
    background: #FAFAFA;
    padding: 30px;
    margin: 50px auto;
    box-shadow: 1px 1px 25px rgba(0, 0, 0, 0.35);
    border-radius: 10px;
    border: 6px solid #305A72;
}
.form-style-9 ul{
    padding:0;
    margin:0;
    list-style:none;
}
.form-style-9 ul li{
    display: block;
    margin-bottom: 10px;
    min-height: 35px;
}
.form-style-9 ul li  .field-style{
    box-sizing: border-box; 
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box; 
    padding: 8px;
    outline: none;
    border: 1px solid #B0CFE0;
    -webkit-transition: all 0.30s ease-in-out;
    -moz-transition: all 0.30s ease-in-out;
    -ms-transition: all 0.30s ease-in-out;
    -o-transition: all 0.30s ease-in-out;

}.form-style-9 ul li  .field-style:focus{
    box-shadow: 0 0 5px #B0CFE0;
    border:1px solid #B0CFE0;
}
.form-style-9 ul li .field-split{
    width: 49%;
}
.form-style-9 ul li .field-full{
    width: 100%;
}
.form-style-9 ul li input.align-left{
    float:left;
}
.form-style-9 ul li input.align-right{
    float:right;
}
.form-style-9 ul li textarea{
    width: 100%;
    height: 100px;
}
.form-style-9 ul li input[type="button"], 
.form-style-9 ul li input[type="submit"] {
    -moz-box-shadow: inset 0px 1px 0px 0px #3985B1;
    -webkit-box-shadow: inset 0px 1px 0px 0px #3985B1;
    box-shadow: inset 0px 1px 0px 0px #3985B1;
    background-color: #216288;
    border: 1px solid #17445E;
    display: inline-block;
    cursor: pointer;
    color: #FFFFFF;
    padding: 8px 18px;
    text-decoration: none;
    font: 12px Arial, Helvetica, sans-serif;
}
.form-style-9 ul li input[type="button"]:hover, 
.form-style-9 ul li input[type="submit"]:hover {
    background: linear-gradient(to bottom, #2D77A2 5%, #337DA8 100%);
    background-color: #28739E;
}
</style>

<form class="form-style-9" action="Adddata/update.php">
  <h2>ค่าใช้จ่าย (แก้ไข)</h2>
<ul>
  <?php 
    $sql = "SELECT * FROM `device` ORDER BY DE_Date";
    $query = mysqli_query($conn,$sql);
    while ($result = mysqli_fetch_array($query,MYSQLI_ASSOC)) { 
  ?>
    <li>
      <input type="text" name="note<?php echo($result["ID_device"]) ?>" class="field-style field-split align-left" placeholder="หมายเหตุ" value="<?php echo($result["DE_note"]) ?>" />
      <input type="number" name="pay<?php echo($result["ID_device"]) ?>" class="field-style field-split align-right" placeholder="จำนวนค่าใช้จ่าย (บาท)" value="<?php echo($result["DE_pay"]) ?>" min="0.00" step="0.01"/>
      <input type="checkbox" name="Delete<?php echo($result["ID_device"]) ?>" value="Y">  ลบ
    </li>
  <?php } ?>

<!--li>
<input type="text" name="field3" class="field-style field-full align-none" placeholder="Subject" />
</li>
<li>
<textarea name="field5" class="field-style" placeholder="Message"></textarea>
</li-->
<li>
<input type="submit" value="ยืนยัน" />
</li>
</ul>
</form>

<form class="form-style-9" action="Adddata/insert.php">
  <h2>ค่าใช้จ่าย (เพิ่ม)</h2>
<ul>
    <li>
      <input type="text" name="note" class="field-style field-split align-left" placeholder="หมายเหตุ"  />
      <input type="number" name="pay" class="field-style field-split align-right" placeholder="จำนวนค่าใช้จ่าย (บาท)" min="0.00" step="0.01"/>
    </li>
<li>
<input type="submit" value="เพิ่ม" />
</li>
</ul>
</form>

<form class="form-style-9" action="Adddata/insertPrice.php">
  <h2>ค่าไฟฟ้า(บาท) ต่อ 1000Pw</h2>
<ul>
    <li>
      <?php  
        $sql = "SELECT * FROM `electricity price`";
        $query = mysqli_query($conn,$sql);
        while ($result = mysqli_fetch_array($query,MYSQLI_ASSOC)) {
          $EP_money = $result["EP_money"];
        }
      ?>
      <input type="number" name="pay" class="field-style field-full align-none" placeholder="ราคาขายไฟฟ้า" min="0.00" step="0.01"/ value="<?php echo($EP_money) ?>">
    </li>
<li>
<input type="submit" value="เพิ่ม" />
</li>
</ul>
</form>