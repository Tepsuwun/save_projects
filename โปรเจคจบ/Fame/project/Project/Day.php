<?php
	include "conn.php";

	function str_plus($str,$plus){
		$str = intval($str);
		$str+=$plus;
		$str = str_pad($str, 2, "0", STR_PAD_LEFT);
		return $str;
	}
	function str_4plus($str,$plus){
		$str = intval($str);
		$str+=$plus;
		$str = str_pad($str, 4, "0", STR_PAD_LEFT);
		return $str;
	}
	$date = '0000-00-00 00:00:00';
	if (isset($_POST["date"])) {
		$date = $_POST["date"];
		$date = "$date 00:00:00";
	}else{
		$sql = "SELECT MAX(EE_Electime) FROM `electrical energy min` WHERE ID_Mi = '$Mi'";
  		$query = mysqli_query($conn,$sql);
		while ($result = mysqli_fetch_array($query,MYSQLI_ASSOC)) {
			$date = $result["MAX(EE_Electime)"];
		}
		$date = substr($date, 0, 10);
		$date = "$date 00:00:00";
	}
	//echo "====$date<br>";
  $subdate = substr($date, 0, 10);
  //echo "$subdate<br>";
  $tomorrow = date('Y-m-d', strtotime($subdate . '+1 day'));
  //echo "$tomorrow"; //วันพรุ้งนี้
	$Y = substr($date, 0, 4);
	$M = substr($date, 5, 2);
	$D = substr($date, 8, 2);
	$h = substr($date, 11, 2);
	$m = substr($date, 14, 2);
	$s = substr($date, 17, 2);
	
	$Y_plus = str_4plus($Y,1);
	$M_plus = str_plus($M,1);
	$D_plus = str_plus($D,1);
	$h_plus = str_plus($h,1);
	$m_plus = str_plus($m,1);
	$s_plus = str_plus($s,1);
	$date_plus = "$Y-$M-$D_plus $h:$m:$s";
	$sql = "SELECT * FROM `electrical energy min` WHERE `EE_Electime` >= '".$date."' AND `EE_Electime` <= '".$date_plus."' AND `ID_Mi` LIKE '$Mi'";
  	$query = mysqli_query($conn,$sql);
  	$EE_Pw1=array();
  	$EE_V1=array();
  	$EE_A1=array();
  	$EE_Electime=array();
  	$EE_Phase1=array();
  	$EE_Pw2=array();
  	$EE_V2=array();
  	$EE_A2=array();
  	$EE_Phase2=array();
  	$EE_Pw3=array();
  	$EE_V3=array();
  	$EE_A3=array();
  	$EE_Phase3=array();
  	while ($result = mysqli_fetch_array($query,MYSQLI_ASSOC)) {
  		if ($result["EE_Phase"] == '1') {
  			array_push($EE_Phase1,	$result["EE_Phase"]);
  			array_push($EE_Pw1,		$result["EE_Pw"]);
  			array_push($EE_V1,		$result["EE_V"]);
  			array_push($EE_A1,		$result["EE_A"]);
  			array_push($EE_Electime,	$result["EE_Electime"]);
  		}else if ($result["EE_Phase"] == '2') {
  			array_push($EE_Pw2,		$result["EE_Pw"]);
  			array_push($EE_V2,		$result["EE_V"]);
  			array_push($EE_A2,		$result["EE_A"]);
  			array_push($EE_Phase2,	$result["EE_Phase"]);
  		}else if ($result["EE_Phase"] == '3') {
  			array_push($EE_Pw3,		$result["EE_Pw"]);
  			array_push($EE_V3,		$result["EE_V"]);
  			array_push($EE_A3,		$result["EE_A"]);
  			array_push($EE_Phase3,	$result["EE_Phase"]);
  		}
  	}
?>
<?php 
$arr1=array();
$arr2=array();
$arr3=array();
if (isset($EE_Electime[0])==true) {
  $value2 = substr($EE_Electime[0], 11, 2);
}
$sum1 = 0;
$sum2 = 0;
$sum3 = 0;
$time_arr = array();
$ch = '';
$value1 = '';
$AA = '';
foreach ($EE_Electime as $key => $value) {
  //echo "$key => $value || เฟส1= $EE_Pw1[$key] เฟส2= $EE_Pw2[$key] เฟส3= $EE_Pw3[$key]<br>";
  $AA = $value1;
  $value1 = substr($value, 11, 2);
  //echo "$value1 , $value2<br>";
  if ($value1 != $value2) {
    //echo "(+เพิ่มใน $value1)<br>";
    //echo "- $EE_Pw1[$key] $EE_Pw2[$key] $EE_Pw3[$key]<br>"; 
    //echo "$AA- $sum1 $sum2 $sum3<br>"; 
      array_push($arr1,$sum1);
      array_push($arr2,$sum2);
      array_push($arr3,$sum3);
      array_push($time_arr,$AA);
      $ch = 'N';
    
    
    $sum1=0; 
    $sum2=0; 
    $sum3=0; 
    $value2 = $value1;
    $sum1 += $EE_Pw1[$key]; 
    $sum2 += $EE_Pw2[$key]; 
    $sum3 += $EE_Pw3[$key];
  }
  
  //==
  $testloob_End = 'N';
  if ($ch == 'N') {
    $ch = 'Y';
    $testloob_End = 'Y';
  }else{
    $sum1+=$EE_Pw1[$key]; 
    $sum2+=$EE_Pw2[$key]; 
    $sum3+=$EE_Pw3[$key]; 
    //echo "$EE_Pw1[$key] $EE_Pw2[$key] $EE_Pw3[$key]<br>"; 
  }
}
if ($testloob_End == 'N') {
  array_push($arr1,$sum1);
  array_push($arr2,$sum2);
  array_push($arr3,$sum3);
  array_push($time_arr,$AA);
}

/*foreach ($EE_Electime as $key => $value) {
	//echo "<br>" .$value;
	$value1 = substr($value, 11, 2);
	//echo ": ($value1)($value2) :";
	//==
	if ($value2 != $value1) {
		$ch = substr($value, 14, 2);
		//echo "($ch) ||<br>";
    echo "$value2 $value1 ||<br>";
		//if ($ch == '00') {
			$sum1+=$EE_Pw1[$key]; 
			$sum2+=$EE_Pw2[$key]; 
			$sum3+=$EE_Pw3[$key]; 
      //echo "3<br>";
			array_push($arr1,$sum1);
			array_push($arr2,$sum2);
			array_push($arr3,$sum3);
			array_push($time_arr,$value1);
      echo "--$value1--<br>**************<br><br>";

			$ch = 'N';
		//}
		$sum1=0; 
		$sum2=0; 
		$sum3=0; 
    $value2 = $value1;
    echo "$value2 $value1 ||<br>";

	}
	//==
	if ($ch == 'N') {
		$ch = 'Y';
	}else{
		$sum1+=$EE_Pw1[$key]; 
		$sum2+=$EE_Pw2[$key]; 
		$sum3+=$EE_Pw3[$key]; 
	}
}*/

?>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
<script type="text/javascript">
	
Highcharts.chart('container', {
    chart: {
        type: 'area'
    },
    title: {
        text: <?php echo "'กราฟพลังงานรายวัน'" ?>
    },
    subtitle: {
        text: <?php echo "'	วันที่ $subdate'" ?>
    },
    xAxis: {
        //categories: ['1750', '1800', '1850', '1900', '1950', '1999', '2050'],
        <?php 
        	echo "categories: [";
        	$CH = 0; 
        	foreach ($time_arr as $key => $value) {
        		if ($CH == 1) {
        		echo ',';
        		}
        		echo "'".$value.".00น.'";
        		$CH = 1;
        	}
        	echo "],";
        ?>
        tickmarkPlacement: 'on',
        title: {
            enabled: false
        }
    },
    yAxis: {
        title: {
            text: 'Pw/h'
        },
        labels: {
            formatter: function () {
                return this.value ;
            }
        }
    },
    tooltip: {
        split: true,
        valueSuffix: ' Pw/h'
    },
    plotOptions: {
        area: {
            stacking: 'normal',
            lineColor: '#666666',
            lineWidth: 1,
            marker: {
                lineWidth: 1,
                lineColor: '#666666'
            }
        }
    },
    series: [{
        name: 'เฟส1',
        //data: [502, 635, 809, 947, 1402, 3634, 10000,10000]
        <?php 
        	echo "data: [";
        	$CH = 0; 
        	foreach ($arr1 as $key => $value) {
        		if ($CH == 1) {
        		echo ',';
        		}
        		echo $arr1[$key];
        		$CH = 1;
        	}
        	echo "]";
        ?>
    }, {
        name: 'เฟส2',
        //data: [106, 107, 111, 133, 221, 767, 7500,5000]
        <?php 
        	echo "data: [";
        	$CH = 0; 
        	foreach ($arr2 as $key => $value) {
        		if ($CH == 1) {
        		echo ',';
        		}
        		echo $arr2[$key];
        		$CH = 1;
        	}
        	echo "]";
        ?>
    }, {
        name: 'เฟส3',
        //data: [163, 203, 276, 408, 547, 729, 5000]
        <?php 
        	echo "data: [";
        	$CH = 0; 
        	foreach ($arr3 as $key => $value) {
        		if ($CH == 1) {
        		echo ',';
        		}
        		echo $arr3[$key];
        		$CH = 1;
        	}
        	echo "]";
        ?>
    }]
});
</script>
<?php  $sql = "SELECT * FROM `electricity price`";
  $query = mysqli_query($conn,$sql);
  while ($result = mysqli_fetch_array($query,MYSQLI_ASSOC)) {
    
    $EP_money = $result["EP_money"];
  }
  ?>

<div align="center">
  รายละเอียดกราฟ
  <table border="1" width="400">
    <tr>
      <td>พลังงานรวมใน 1 วัน</td>
      <td><?php 
      $sum = 0;
      $num = 0;
      foreach ($arr1 as $key => $value) {
        $num++;
        $sum += $arr1[$key]+$arr2[$key]+$arr3[$key];
      }
      $B2 =  number_format($sum/1000*$EP_money, 2, '.', ',');
      $B = $sum/1000*$EP_money;
      echo "$sum Pw/h (".$B2." บาท)"; ?></td>
    </tr>
    <tr>
      <td>พลังงานเฉลี่ย ต่อชั่วโมง</td>
      <td><?php 
      $B =  number_format($B/$num, 2, '.', ',');
      $sum =  number_format($sum/$num, 2, '.', ',');
      echo $sum." Pw/h (".$B." บาท)"; 
      ?></td>
    </tr>
  </table>
</div>

<hr><br>
