
<?php
	include "conn.php";
	$sql = "SELECT MAX(`EE_Electime`) FROM `electrical energy min` WHERE `EE_Electime` != '0000-00-00' AND `ID_Mi` LIKE '$Mi'";
  	$query = mysqli_query($conn,$sql);
  	while ($result = mysqli_fetch_array($query,MYSQLI_ASSOC)) {

      $MAX = $result["MAX(`EE_Electime`)"];
    }
  $sql = "SELECT MIN(`EE_Electime`) FROM `electrical energy min` WHERE `EE_Electime` != '0000-00-00' AND `ID_Mi` LIKE '$Mi'";
    $query = mysqli_query($conn,$sql);
    while ($result = mysqli_fetch_array($query,MYSQLI_ASSOC)) {

      $MIN = $result["MIN(`EE_Electime`)"];
    }
  if (isset($MAX)==true) {
    $MAX = substr($MAX, 0, 4);
    $MIN = substr($MIN, 0, 4);
    //echo "MAX = ".$MAX." MIN = ".$MIN;
    $MAX = intval( $MAX );
    $MIN = intval( $MIN );
    //echo "MAX = ".$MAX." MIN = ".$MIN;
    $MIN2 = $MIN+1;
    $EE_Pw1=array();
    $EE_Pw2=array();
    $EE_Pw3=array();
    $time_arr=array();
    $sum1 = 0;
    $sum2 = 0;
    $sum3 = 0;
    $sumall = 0;
    $num = 0;
    do {
      $sql = "SELECT * FROM `electrical energy min` WHERE `EE_Electime` > '".$MIN."-01-01' AND `EE_Electime` <= '".$MIN2."-01-01' AND `ID_Mi` LIKE '$Mi'";
      $query = mysqli_query($conn,$sql);
      //echo "$sql";
      while ($result = mysqli_fetch_array($query,MYSQLI_ASSOC)) {
      if ($result["EE_Phase"] == '1') {
        $sum1+=$result["EE_Pw"];
        //echo "$sum1";
      }else if ($result["EE_Phase"] == '2') {
        $sum2+=$result["EE_Pw"];
        //echo " $sum2";
      }else if ($result["EE_Phase"] == '3') {
        $sum3+=$result["EE_Pw"];
        //echo " $sum3";
      }
      //echo "<br>";
      $num++;
      $sumall+=$result["EE_Pw"];
      }
      //echo "##########";
      array_push($time_arr,$MIN);
      array_push($EE_Pw1,$sum1);
      array_push($EE_Pw2,$sum2);
      array_push($EE_Pw3,$sum3);
      $MIN++;
    } while ($MIN < $MAX);
  }
?>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>

<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
<script type="text/javascript">
  
Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'กราฟพลังงานทั้งหมด'
    },
    xAxis: {
        //categories: ['Apples', 'Oranges', 'Pears', 'Grapes', 'Bananas']
        <?php 
          echo "categories: [";
          $CH = 0; 
          foreach ($time_arr as $key => $value) {
            if ($CH == 1) {
            echo ',';
            }
            echo $value;
            $CH = 1;
          }
          echo "]";
        ?>
    },
    yAxis: {
        min: 0,
        title: {
            text: 'พลังงานไฟฟ้า (Pw)'
        }
    },
    tooltip: {
        pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
        shared: true
    },
    plotOptions: {
        column: {
            stacking: 'percent'
        }
    },
    series: [{
        name: 'เฟส1',
        //data: [5, 3, 4, 7, 2]
        <?php 
          echo "data: [";
          $CH = 0; 
          foreach ($EE_Pw1 as $key => $value) {
            if ($CH == 1) {
            echo ',';
            }
            echo $value;
            $CH = 1;
          }
          echo "]";
        ?>
    }, {
        name: 'เฟส2',
        //data: [2, 2, 3, 2, 1]
        <?php 
          echo "data: [";
          $CH = 0; 
          foreach ($EE_Pw2 as $key => $value) {
            if ($CH == 1) {
            echo ',';
            }
            echo $value;
            $CH = 1;
          }
          echo "]";
        ?>
    }, {
        name: 'เฟส3',
        //data: [3, 4, 4, 2, 5]
        <?php 
          echo "data: [";
          $CH = 0; 
          foreach ($EE_Pw3 as $key => $value) {
            if ($CH == 1) {
            echo ',';
            }
            echo $value;
            $CH = 1;
          }
          echo "]";
        ?>
    }]
});

</script>
<table>
  <tr>
    <td></td>
    <td></td>
  </tr>
</table>

<div align="center">
<?php 
  $sql = "SELECT * FROM `electricity price`";
  $query = mysqli_query($conn,$sql);
  while ($result = mysqli_fetch_array($query,MYSQLI_ASSOC)) {
    
    $EP_money = $result["EP_money"];
  }
  $B_all = $sumall*$EP_money/1000;
  echo "<table border=\"1\" width=\"600\">";
  $numDay = $num/4/24;
  $numDay = 0;
  echo "$num $numDay <br>";

  $EE_ElectimeLOG = '';
  $sql = "SELECT * FROM `electrical energy min` WHERE `EE_Electime` > '1000-01-01' AND `ID_Mi` LIKE '$Mi'";
      $query = mysqli_query($conn,$sql);
      //echo "$sql";
      while ($result = mysqli_fetch_array($query,MYSQLI_ASSOC)) {
        $EE_Electime = $result["EE_Electime"]."<br>";
        $EE_Electime = substr($EE_Electime, 0, 10);
        //echo "$EE_Electime <br>";
        if ($EE_ElectimeLOG != $EE_Electime) {
          $numDay++;
          $EE_ElectimeLOG = $EE_Electime;
        }
      }

  $sql = "SELECT * FROM `device`";
  $query = mysqli_query($conn,$sql);
  $summ_pay=0;
  while ($result = mysqli_fetch_array($query,MYSQLI_ASSOC)) {
    echo "<tr><td>";
    echo $result["DE_Date"]."</td><td> ".$result["DE_note"]." </td><td>".$result["DE_pay"]." บาท<br></td></tr>";
    $summ_pay += $result["DE_pay"];
  }
  echo "</tr><td></td><td>รวม =</td><td>$summ_pay บาท</td>"; 
  echo "<tr><td>";
  echo "จำนวนวันทั้งหมดที่ติดตั้ง </td><td>".number_format($numDay, 0, '.', ' ')." วัน </td>";
  echo "<td>  ".number_format($B_all, 2, '.', ',')." บาท ($sumall Wp) <br></td><tr>"; 
  $Day_End = $numDay/$B_all*30000;
  $Day_End2 = number_format($Day_End, 2, '.', ',');
  $summ_Pw = $summ_pay*1000/$EP_money;
  $Day_pay = $summ_pay / ($B_all/$numDay);
  //echo "$Day_pay";
  $Day_pay2 = number_format($Day_pay, 2, '.', ',');
  echo "<tr><td> คาดว่าจะใช้ระยะเวลาคืนทุน </td><td>$Day_pay2 วัน</td><td>$summ_pay บาท ($summ_Pw Pw)</td></tr>";
  $Day_End = $Day_pay-$numDay;
  $Day_End2 = number_format($Day_End, 2, '.', ',');
  $Day_End3 = $Day_End/30;
  $Day_End4 = $Day_End%30;
  $Day_End3 = floor($Day_End3);
  $Day_End5 = $Day_End3/12;
  $Day_End6 = $Day_End3%12;
  $Day_End5 = floor($Day_End5);
  //echo "$Day_End5 , $Day_End6";



  echo "<tr><td>คาดว่าจะถึงจุดคุ้มทุนในอีก</td><td>$Day_End2 วัน</td><td>$Day_End5 ปี $Day_End6 เดือน $Day_End4 วัน</td></tr>";
  echo "</table>";
?>
</div>

<hr><br>

