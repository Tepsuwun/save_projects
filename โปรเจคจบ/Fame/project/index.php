<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<title>Daily UI - Day 1 Sign In</title>

	<!-- Google Fonts -->
	<link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700|Lato:400,100,300,700,900' rel='stylesheet' type='text/css'>

	<link rel="stylesheet" href="css/animate.css">
	<!-- Custom Stylesheet -->
	<link rel="stylesheet" href="css/style2.css">

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
</head>

<body>
 <!--  <header style="border:1px;width:1366px;height:100px;background-color:#665851">
   <font color="#FFFFFF" style="font-size:30px;">&#3619;&#3632;&#3610;&#3610;&#3605;&#3619;&#3623;&#3592;&#3626;&#3629;&#3610;&#3649;&#3621;&#3632;&#3619;&#3634;&#3618;&#3591;&#3634;&#3609;&#3585;&#3634;&#3619;&#3612;&#3621;&#3636;&#3605;&#3614;&#3621;&#3633;&#3591;&#3591;&#3634;&#3609;&#3652;&#3615;&#3615;&#3657;&#3634; </font>
   </header> -->
	<div class="container">
		<div class="top">
                    <h1 id="title" class="hidden"><span id="logo">&#3619;&#3632;&#3610;&#3610;&#3605;&#3619;&#3623;&#3592;&#3626;&#3629;&#3610;&#3649;&#3621;&#3632;&#3619;&#3634;&#3618;&#3591;&#3634;&#3609;&#3585;&#3634;&#3619;&#3612;&#3621;&#3636;&#3605;&#3614;&#3621;&#3633;&#3591;&#3591;&#3634;&#3609;&#3652;&#3615;&#3615;&#3657;&#3634;&#3592;&#3634;&#3585;&#3649;&#3626;&#3591;&#3629;&#3634;&#3607;&#3636;&#3605;&#3618;&#3660; &#3649;&#3610;&#3610; 3 &#3648;&#3615;&#3626; </span></h1>
		</div>
		<div class="login-box animated fadeInUp">
			<div class="box-header">
				<h2>Log In</h2>
			</div>
			<form action="login.php" method="post">
			<label for="username">Username</label>
			<br/>
			<input type="text" id="id" name="id">
			<br/>
			<label for="password">Password</label>
			<br/>
			<input type="password" id="pass" name="pass">
			<br/>
			<button type="submit">Sign In</button>
			<br/>
			</form>
			<!--##############################################################################################################-->
			<?php include 'conn.php'; ?>
			<?php 
  				/*$sql ="SELECT * FROM `user`";
  				$query = mysqli_query($conn,$sql);
  				echo "<br>";
  				while ($result = mysqli_fetch_array($query,MYSQLI_ASSOC)) {
    				     echo $result["ID_User"];
                                     echo $result["US_name"];
                                     echo $result["US_pass"];
                                     echo $result["US_Status"];
  				}
  				echo "</table>";*/
			?>
			<p>id : tara pass : tara</p>
			<!--a href="#"><p class="small">Forgot your password?</p></a-->
		</div>
	</div>
</body>

<script>
	$(document).ready(function () {
    	$('#logo').addClass('animated fadeInDown');
    	$("input:text:visible:first").focus();
	});
	$('#username').focus(function() {
		$('label[for="username"]').addClass('selected');
	});
	$('#username').blur(function() {
		$('label[for="username"]').removeClass('selected');
	});
	$('#password').focus(function() {
		$('label[for="password"]').addClass('selected');
	});
	$('#password').blur(function() {
		$('label[for="password"]').removeClass('selected');
	});
</script>

</html>