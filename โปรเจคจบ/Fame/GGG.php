
<meta http-equiv="refresh" content="10"/>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
<div class="row">
    <div class="col-md-6" > 
<div id="V" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
</div>
<div class="col-md-6" > 
<div id="A" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
</div>
<div class="col-md-6" > 
<div id="W" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
</div>
<div class="col-md-6" > 
<div id="Wp" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
</div>
<?php include 'conn.php'; ?>
<?php
$EE_Pw1 = array();
$time = array();
$V = array();
$A = array();
$W = array();
$num = 0;
 $sql = "SELECT * FROM `test` ORDER BY time";
  $query = mysqli_query($conn,$sql);
  while ($result = mysqli_fetch_array($query,MYSQLI_ASSOC)) {
      array_push($V, $result["V"]);
      array_push($A, $result["A"]);
      array_push($W, $result["W"]);
      array_push($EE_Pw1, $result["Wh"]);
      array_push($time, $result["time"]);
  }
$EE_Pw2 = array();
$time2 = array();
$V2 = array();
$A2 = array();
$W2 = array();
$num = 0;
 $sql = "SELECT * FROM `test2` ORDER BY time";
  $query = mysqli_query($conn,$sql);
  while ($result = mysqli_fetch_array($query,MYSQLI_ASSOC)) {
      array_push($V2, $result["V"]);
      array_push($A2, $result["A"]);
      array_push($W2, $result["W"]);
      array_push($EE_Pw2, $result["Wh"]);
      array_push($time2, $result["time"]);
  }
$EE_Pw3 = array();
$time3 = array();
$V3 = array();
$A3 = array();
$W3 = array();
$num = 0;
 $sql = "SELECT * FROM `text3` ORDER BY time";
  $query = mysqli_query($conn,$sql);
  while ($result = mysqli_fetch_array($query,MYSQLI_ASSOC)) {
      array_push($V3, $result["V"]);
      array_push($A3, $result["A"]);
      array_push($W3, $result["W"]);
      array_push($EE_Pw3, $result["Wh"]);
      array_push($time3, $result["time"]);
  }
/*echo $num."<br>";
for ($i=0;$i<8;$i++){
      echo $i." => ".$V[$i]." | ".$A[$i]." | ".$W[$i]." | ".$EE_Pw1[$i]." | ".$time[$i]."<br>";
}
echo $num."<br>";
for ($i=0;$i<8;$i++){
      echo $i." => ".$V2[$i]." | ".$A2[$i]." | ".$W2[$i]." | ".$EE_Pw2[$i]." | ".$time2[$i]."<br>";
}
echo $num."<br>";
for ($i=0;$i<8;$i++){
      echo $i." => ".$V3[$i]." | ".$A3[$i]." | ".$W3[$i]." | ".$EE_Pw3[$i]." | ".$time3[$i]."<br>";
}*/

?>


<script type="text/javascript">
  
Highcharts.chart('V', {
    chart: {
        type: 'line'
    },
    title: {
        text: 'V'
    },
    subtitle: {
        text: 'ปัจจุบัน'
    },
    xAxis: {
        categories: [<?php 
                       for ($i=0;$i<8;$i++){
                          if ($i>0){
                             echo ", ";
                          }
                          echo "'$time[$i]'";
                       }
                    ?>]

    },
    yAxis: {
        title: {
            text: 'V'
        }
    },
    plotOptions: {
        line: {
            dataLabels: {
                enabled: true
            },
            enableMouseTracking: false
        }
    },
    series: [{
        name: 'เฟส 1',
        data: [<?php 
                       for ($i=0;$i<8;$i++){
                          if ($i>0){
                             echo ", ";
                          }
                          echo "$V[$i]";
                       }
                    ?>]
    }, {
        name: 'เฟส 2',
        data: [<?php 
                       for ($i=0;$i<8;$i++){
                          if ($i>0){
                             echo ", ";
                          }
                          echo "$V2[$i]";
                       }
                    ?>]
    }, {
        name: 'เฟส 3',
        data: [<?php 
                       for ($i=0;$i<8;$i++){
                          if ($i>0){
                             echo ", ";
                          }
                          echo "$V3[$i]";
                       }
                    ?>]
    }]
});

  
Highcharts.chart('Wp', {
    chart: {
        type: 'line'
    },
    title: {
        text: 'Wp'
    },
    subtitle: {
        text: 'ปัจจุบัน'
    },
    xAxis: {
        categories: [<?php 
                       for ($i=0;$i<8;$i++){
                          if ($i>0){
                             echo ", ";
                          }
                          echo "'$time[$i]'";
                       }
                    ?>]
    },
    yAxis: {
        title: {
            text: 'Wp'
        }
    },
    plotOptions: {
        line: {
            dataLabels: {
                enabled: true
            },
            enableMouseTracking: false
        }
    },
    series: [{
        name: 'เฟส 1',
        data: [<?php 
                       for ($i=0;$i<8;$i++){
                          if ($i>0){
                             echo ", ";
                          }
                          echo "$EE_Pw1[$i]";
                       }
                    ?>]
    }, {
        name: 'เฟส 2',
        data: [<?php 
                       for ($i=0;$i<8;$i++){
                          if ($i>0){
                             echo ", ";
                          }
                          echo "$EE_Pw2[$i]";
                       }
                    ?>]
    }, {
        name: 'เฟส 3',
        data: [<?php 
                       for ($i=0;$i<8;$i++){
                          if ($i>0){
                             echo ", ";
                          }
                          echo "$EE_Pw3[$i]";
                       }
                    ?>]
    }]
});

Highcharts.chart('A', {
    chart: {
        type: 'line'
    },
    title: {
        text: 'A'
    },
    subtitle: {
        text: 'ปัจจุบัน'
    },
    xAxis: {
        categories: [<?php 
                       for ($i=0;$i<8;$i++){
                          if ($i>0){
                             echo ", ";
                          }
                          echo "'$time[$i]'";
                       }
                    ?>]
    },
    yAxis: {
        title: {
            text: 'A'
        }
    },
    plotOptions: {
        line: {
            dataLabels: {
                enabled: true
            },
            enableMouseTracking: false
        }
    },
    series: [{
        name: 'เฟส 1',
        data: [<?php 
                       for ($i=0;$i<8;$i++){
                          if ($i>0){
                             echo ", ";
                          }
                          echo "$A[$i]";
                       }
                    ?>]
    }, {
        name: 'เฟส 2',
        data: [<?php 
                       for ($i=0;$i<8;$i++){
                          if ($i>0){
                             echo ", ";
                          }
                          echo "$A2[$i]";
                       }
                    ?>]
    }, {
        name: 'เฟส 3',
        data: [<?php 
                       for ($i=0;$i<8;$i++){
                          if ($i>0){
                             echo ", ";
                          }
                          echo "$A3[$i]";
                       }
                    ?>]
    }]
});

Highcharts.chart('W', {
    chart: {
        type: 'line'
    },
    title: {
        text: 'W'
    },
    subtitle: {
        text: 'ปัจจุบัน'
    },
    xAxis: {
        categories: [<?php 
                       for ($i=0;$i<8;$i++){
                          if ($i>0){
                             echo ", ";
                          }
                          echo "'$time[$i]'";
                       }
                    ?>]
    },
    yAxis: {
        title: {
            text: 'W'
        }
    },
    plotOptions: {
        line: {
            dataLabels: {
                enabled: true
            },
            enableMouseTracking: false
        }
    },
    series: [{
        name: 'เฟส 1',
        data: [<?php 
                       for ($i=0;$i<8;$i++){
                          if ($i>0){
                             echo ", ";
                          }
                          echo "$W[$i]";
                       }
                    ?>]
    }, {
        name: 'เฟส 2',
        data: [<?php 
                       for ($i=0;$i<8;$i++){
                          if ($i>0){
                             echo ", ";
                          }
                          echo "$W2[$i]";
                       }
                    ?>]
    }, {
        name: 'เฟส 3',
        data: [<?php 
                       for ($i=0;$i<8;$i++){
                          if ($i>0){
                             echo ", ";
                          }
                          echo "$W3[$i]";
                       }
                    ?>]
    }]
});
</script>
