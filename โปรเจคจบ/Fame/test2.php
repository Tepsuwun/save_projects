﻿<script src="https://code.highcharts.com/stock/highstock.js"></script>
<script src="https://code.highcharts.com/stock/modules/exporting.js"></script>
<div id="container" style="height: 400px; min-width: 310px"></div>
<?php 
include 'conn.php';
                    $V = 0;
                    $A = 0;
                    $W = 0;
                    $Wh = 10;
                /*$sql = "SELECT * FROM `test` WHERE `ID` = '1'";
                $query = mysqli_query($conn,$sql);
                while ($result = mysqli_fetch_array($query,MYSQLI_ASSOC)) {
                    $V = $result["V"];
                    $A = $result["A"];
                    $W = $result["W"];
                    $Wh = $result["Wh"];
                }*/
?>


<script type="text/javascript">
Highcharts.setOptions({
    global: {
        useUTC: false
    }
});

// Create the chart
<?php  $i = 10; ?>
var i=  0   ;
<?php $i++; ?>
Highcharts.stockChart('container', {
    chart: {
        events: {
            load: function () {

                // set up the updating of the chart each second
                var series = this.series[0];
                <?php $i++; ?>
                setInterval(function () {
                    var x = (new Date()).getTime(), // current time
                    <?php 
                        /*$sql = "SELECT * FROM `test` WHERE `ID` = '1'";
                        $query = mysqli_query($conn,$sql);
                        while ($result = mysqli_fetch_array($query,MYSQLI_ASSOC)) {
                            $Wh = $result["Wh"];
                        }*/
                    ?>
                    //y = <?php //echo $Wh; ?> 
                    //i = Math.round(Math.random() * 100);
                    <?php $i++; ?>
                    i = <?php echo (rand(10,100)); ?> ;
                    //y = Math.random() * i;
                    y = <?php echo (rand(10,100)); ?>;
                        //y = Math.round(Math.random() * 100);
                    series.addPoint([x, <?php $i+=20; echo $i; ?>], true, false);
                    series.addPoint([x, <?php $i+=20; echo $i; ?>], true, false);
                }, 1000);
            }
        }
    },

    rangeSelector: {
        buttons: [{
            count: 1,
            type: 'minute',
            text: '1M'
        }, {
            count: 5,
            type: 'minute',
            text: '5M'
        }, {
            type: 'all',
            text: 'All'
        }],
        inputEnabled: false,
        selected: 0
    },

    title: {
        text: 'Live random data'
    },

    exporting: {
        enabled: false
    },

    series: [{
        name: 'Random data',
        data: (function () {
            // generate an array of random data
            var data = [],
                time = (new Date()).getTime(),
                i;

            for (i = -60; i <= 0; i += 1) {
                data.push([
                    time + i * 1000,
                    
                    Math.round(Math.random() * 100)
                ]);
            }
            <?php $i++; ?>
            return data;
        }())
    }]
});
<?php $i++; ?>
</script>